import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [OnboardingComponent],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule
  ]
})
export class OnboardingModule { }
