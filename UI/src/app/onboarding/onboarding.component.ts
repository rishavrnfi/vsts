import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { environment } from '../../environments/environment';
import { EkycService } from '../services/ekyc.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css'],
  providers:[EkycService]
})
export class OnboardingComponent implements OnInit {

  agent_token:string='';

  constructor(
    private formBuilder: FormBuilder,
    private ekycService:EkycService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
			this.agent_token = params['agent_token'];
			sessionStorage.setItem("agent_token", this.agent_token);
		});
  }

  onboarding(){
    const formData = new FormData();
    formData.append('agent_token',this.agent_token);
    this.ekycService.onboarding(formData).subscribe(
      data => {
        if(data['status']){
          Swal.fire({
            icon: 'success',
            title: 'Csp Onboarding',
            text: data['message']
          });
          if(data['url'] !=''){
            window.location.replace(data['url']);
          }
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: data['message']
          });
        }
      },
      err => {        
         console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
    
  }

}
