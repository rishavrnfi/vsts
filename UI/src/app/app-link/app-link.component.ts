import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../services/user.service';
import { EkycService } from '../services/ekyc.service';
import { Router } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AdminService } from '../services/admin.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-app-link',
  templateUrl: './app-link.component.html',
  styleUrls: ['./app-link.component.css']
})
export class AppLinkComponent implements OnInit {

  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  AddFormData: FormGroup;
  loader:Boolean = false;
  result = [];
  length = 100;
  pageSize = 100;
  pageEvent: PageEvent;
  page:number=1;
  appLinks: any;
  linkData: any;
  dialogRef: MatDialogRef<unknown, any>;

  constructor(
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,   
    private adminService:AdminService,
    // private route:ActivatedRoute,
    private ngxloader:NgxUiLoaderService,
    public jwtauth: AuthjwtService,
    public dialog: MatDialog,
  ) { 
    this.user = this.jwtauth.isAuthenticatedjwttoken();
  }

  ngOnInit(): void {

    this.AddFormData = new FormGroup({
      appType:new FormControl("",Validators.compose([
        Validators.required
      ])),
      appName:new FormControl("",Validators.compose([
        Validators.required
      ])),
      appLink:new FormControl("",Validators.compose([
        Validators.required
      ])),
      default:new FormControl("",Validators.compose([
        // Validators.required
      ])),
    });

    this.getAppLink()

  }

  pageevent(event){
    this.AddFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.getAppLink();
  }
  
  addAppLink(){

    this.loader = true;
    const formData = new FormData();
  
    formData.append('appType', this.AddFormData.value.appType);
    formData.append('appName', this.AddFormData.value.appName);
    formData.append('appLink', this.AddFormData.value.appLink);
    formData.append('default', this.AddFormData.value.default);
    formData.append('page',this.page.toString());
  
    this.adminService.addAppLink(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.getAppLink()
          
        }else{
          this.loader=false;
        }
      },
      err => {        
        console.log(err); 
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }     
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getAppLink(){
    this.ngxloader.start();
    this.adminService.getAppLink().subscribe(
      data => {
        if(data['status']) {
          this.ngxloader.stop();
          if(data['result'] != null && data['result'] != ''){   
            // this.appLinks  = data['result'];

            this.appLinks = data['result'].data;
            this.length      = data['result'].total;
            console.log(this.appLinks)
          }
          
        }else{
          this.ngxloader.stop();
        }
      },
      err => {        
        console.log(err); 
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }     
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openDialog(id){
    this.getAppLinkkData(id)
  }


  getAppLinkkData(id){
    this.adminService.getAppLinkkData(id).subscribe(
      data => {
        if(data['code'] == 200) {
            this.linkData = data['result'];
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{ 
        this.dialogRef = this.dialog.open(DialogDataDialog, {
          width:'50vw',
          data: {
            linkData:this.linkData
          }
        });

        this.dialogRef.afterClosed().subscribe(result => {
          this.getAppLink();
          console.log('afterClosed event fired');
        });
        console.log('request completed!');
      }
    );
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}

@Component({
  selector: 'dialog-data-app-link-dialog',
  templateUrl: 'dialog-data-app-link-dialog.html',
  styleUrls: ['./dialog-data-app-link-dialog.css'],
})
export class DialogDataDialog {

  UpdateRemarkFormGroup:FormGroup;
  remark_type:string='';
  remark:string='';
  remarkFormId:string='';
  status:any='';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isChecked:Boolean=false;
  linkDataId: any;
  linkDataType: any;
  linkDataAppname: any;
  linkDataLink: any;
  updateAddFormData: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogDataDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: EkycService,
    private userService:UserService,
    private router: Router,
    private adminService:AdminService,
    private _snackBar: MatSnackBar,
    ) {}

    ngOnInit(): void {

      this.updateAddFormData = new FormGroup({
        appType:new FormControl("",Validators.compose([
          Validators.required
        ])),
        appName:new FormControl("",Validators.compose([
          Validators.required
        ])),
        appLink:new FormControl("",Validators.compose([
          Validators.required
        ])),
        default:new FormControl("",Validators.compose([
          // Validators.required
        ])),
      });

      if(this.data.linkData.id!=''){
        this.linkDataId = this.data.linkData.id;
        this.linkDataType = this.data.linkData.apptype;
        this.linkDataAppname = this.data.linkData.appname;
        this.linkDataLink = this.data.linkData.link;

        this.updateAddFormData.get('appType').setValue(this.linkDataType);
        this.updateAddFormData.get('appName').setValue(this.linkDataAppname);
        this.updateAddFormData.get('appLink').setValue(this.linkDataLink);
        this.updateAddFormData.get('appLink').setValue(this.linkDataLink);
        this.updateAddFormData.get('default').setValue(this.data.linkData.isdefault);
        // if(this.data.remark.status){
        //   this.isChecked = true;
        // }
      }
      
    }

    updateAppLink(){
      const formData = new FormData();
      formData.append('id', this.linkDataId);
      formData.append('appname', this.updateAddFormData.value.appName);
      formData.append('apptype', this.updateAddFormData.value.appType);
      formData.append('link', this.updateAddFormData.value.appLink);
      formData.append('default', this.updateAddFormData.value.default);
      this.adminService.updateAppLink(formData).subscribe(
        data => {
          if(data['code'] == 200) {
            this.openSnackBar(data['message'],'Done!!');
            this.dialogRef.close();
            
          }else{
            this.openSnackBar(data['message'],'Error!!');
          }
          
        },
        err => {        
          console.log(err);
          if(err['status'] == 401){
            window.location.replace(err['error']['returnURL']);
          }
        },() =>{
          console.log('pan request completed!');
        }
      );

    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['mat-snack-bar-container'],
      });
    }
   
}

