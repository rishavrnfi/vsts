import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppLinkRoutingModule } from './app-link-routing.module';
import { AppLinkComponent, DialogDataDialog } from './app-link.component';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  declarations: [AppLinkComponent, DialogDataDialog],
  imports: [
    CommonModule,
    AppLinkRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSlideToggleModule,
    NgxUiLoaderModule
  ]
})
export class AppLinkModule { }
