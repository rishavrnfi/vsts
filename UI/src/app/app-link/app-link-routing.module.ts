import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppLinkComponent } from './app-link.component';

const routes: Routes = [{ path: '', component: AppLinkComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppLinkRoutingModule { }
