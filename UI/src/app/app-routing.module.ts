import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AuthGuard } from './auth/auth.guard';
import { AppComponent } from './app.component';
import { AadharOtpComponent } from './aadhar-otp/aadhar-otp.component';
import { VideoKycComponent } from './video-kyc/video-kyc.component';
import { VideoResponseComponent } from './video-response/video-response.component';
import { AadharResponseComponent } from './aadhar-response/aadhar-response.component';


const routes: Routes = [
  //{ path:'',component:AppComponent },
  { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'staff-dashboard', loadChildren: () => import('./staff-dashboard/staff-dashboard.module').then(m => m.StaffDashboardModule) },
  { path: 'agent/dashboard', loadChildren: () => import('./agent/dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'agent', loadChildren: () => import('./agent/dashboard/dashboard.module').then(m => m.DashboardModule) },
  // { path: 'choose-aadhar', component:AadharOtpComponent },
  { path: 'video-kyc', component:VideoKycComponent },
  { path: 'video-response', component:VideoResponseComponent },
  { path: 'aadhar-response', component:AadharResponseComponent },
  { path: 'aadhar-non-otp', loadChildren: () => import('./aadhar-non-otp/aadhar-non-otp.module').then(m => m.AadharNonOtpModule) },
  { path: 'aadhar-otp', loadChildren: () => import('./choose-aadhar/choose-aadhar.module').then(m => m.ChooseAadharModule) },
  { path: 'admin-remarks', loadChildren: () => import('./admin-remarks/admin-remarks.module').then(m => m.AdminRemarksModule) },
  { path: 'kyc-forms', loadChildren: () => import('./kyc-forms/kyc-forms.module').then(m => m.KycFormsModule) },
  { path: 'admin-dashboard', loadChildren: () => import('./admin-dashboard/admin-dashboard.module').then(m => m.AdminDashboardModule) },
  { path: 'admin-reports', loadChildren: () => import('./admin-reports/admin-reports.module').then(m => m.AdminReportsModule) },
  { path: 'partner-kyc', loadChildren: () => import('./partner-kyc/partner-kyc.module').then(m => m.PartnerKycModule) },
  { path: 'biometric', loadChildren: () => import('./biometric/biometric.module').then(m => m.BiometricModule) },
  { path: 'admin-settings', loadChildren: () => import('./admin-settings/admin-settings.module').then(m => m.AdminSettingsModule) },
  { path: 'biometric-kyc', loadChildren: () => import('./biometric-kyc/biometric-kyc.module').then(m => m.BiometricKycModule) },
  { path: 'partner-aadhar-otp', loadChildren: () => import('./partner-aadhar-otp/partner-aadhar-otp.module').then(m => m.PartnerAadharOtpModule) },
  { path: 'video-partner', loadChildren: () => import('./video-partner/video-partner.module').then(m => m.VideoPartnerModule) },
  { path: 'onboarding', loadChildren: () => import('./onboarding/onboarding.module').then(m => m.OnboardingModule) },
  { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule) },
  { path: 'csp-onboarding', loadChildren: () => import('./cps-onboarding/cps-onboarding.module').then(m => m.CpsOnboardingModule) },
  { path: 'biometricv1', loadChildren: () => import('./biometricv1/biometricv1.module').then(m => m.Biometricv1Module) },
  { path: 'dashboardv1', loadChildren: () => import('./dashboardv1/dashboardv1.module').then(m => m.Dashboardv1Module) },
  { path: 'staff', loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule) },
  { path: 'view-data', loadChildren: () => import('./view-data/view-data.module').then(m => m.ViewDataModule) },
  { path: 'agent-error', loadChildren: () => import('./agent-error/agent-error.module').then(m => m.AgentErrorModule) },
  { path: 'video-verification', loadChildren: () => import('./video-verification/video-verification.module').then(m => m.VideoVerificationModule) },
  { path: 'signzyLog', loadChildren: () => import('./signzy-log/signzy-log.module').then(m => m.SignzyLogModule) },
  { path: 'lockedForms', loadChildren: () => import('./locked-forms/locked-forms.module').then(m => m.LockedFormsModule) },
  { path: 'appLink', loadChildren: () => import('./app-link/app-link.module').then(m => m.AppLinkModule) },
  { path: 'deleteForms', loadChildren: () => import('./delete-forms/delete-forms.module').then(m => m.DeleteFormsModule) },
  //{ path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }