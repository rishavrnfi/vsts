import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { AuthjwtService } from '../services/authjwt.service';
import { UserService } from '../services/user.service';
import { EkycService } from '../services/ekyc.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';
import { Lightbox } from 'ngx-lightbox';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FileValidator } from 'ngx-material-file-input';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import {AadhaarService } from '../services/aadhar-validate.service'
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './staff-dashboard.component.html',
  styleUrls: ['./staff-dashboard.component.css'],
  providers: [
    EkycService
  ],
})
export class StaffDashboardComponent implements OnInit {

  SearchFormData: FormGroup;
  showlist:Boolean=false;
  kycList: any = [];
  loader:Boolean=false;

  length   = 100;
  pageSize = 100;
  pageEvent: PageEvent;
  page:number=1;
  kycData:any;
  dialogRef:any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  user:any;
  statusDropdownDisable: boolean = false;
  status: any;

  minFromDate = new Date(1999, 0, 1);
  maxFromDate = new Date(2025, 0, 1);

  minToDate   = new Date(1999, 0, 1);
  maxToDate   = new Date(2025, 0, 1);

  fromdate: String = '';
  todate: String = '';
  kycStatus: String = '';

  kycModeList: KycMode[] = [
    {value: 'Aadhar OTP', viewValue: 'Aadhar OTP(Free)'},
    {value: 'Biometric', viewValue: 'Biometric(Free)'},
    {value: 'Non Aadhar', viewValue: 'Non Aadhar(Rs 100)'},
  ];
  kycDataMsg: any;
  permission: any;

  constructor(
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,   
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private ngxloader:NgxUiLoaderService
    ) { 
      this.user = this.jwtauth.isAuthenticatedjwttoken();
    }

  ngOnInit(): void {
    // this.userService.validatePage();
    this.getStaffData();
    this.SearchFormData = this.formBuilder.group({
      uservalue: [''],
      status:[''],
      page:[''],
      fromdate:[''],
      todate:[''],
      frommodifieddate:[''],
      modifiedtodate:[''],
      kycMode:[''],
      sortby:['created_at'],
      sortdata:['desc']

    });

    console.log(this.user)
    this.route.queryParams.subscribe(params => {
      if(typeof params['status'] !== 'undefined' ){
        this.SearchFormData.value.status = params['status'];
        this.kycStatus = params['status'];
      }
    });

    // this.SearchFormData = new FormGroup({
    //   fromdate: new FormControl("",Validators.compose([
    //   ])),
    //   todate: new FormControl("",Validators.compose([
    //   ])),
    //   frommodifieddate: new FormControl("",Validators.compose([
    //   ])),
    //   modifiedtodate: new FormControl("",Validators.compose([
    //   ])),
    //   page:new FormControl("",Validators.compose([
    //   ])),
    //   kycStatus:new FormControl("",Validators.compose([
    //   ])),
    //   kycMode:new FormControl("",Validators.compose([
    //   ])),
    //   uservalue:new FormControl("",Validators.compose([
    //   ]))
    // });
    
    
  }

  getStaffData(){   
    this.ekycService.getUserByToken().subscribe(
      data => {
        if(data['code'] == 200) {
          if(data['result']['role_id'] == 2 && data['result']['assign_status'] != 0){   
            this.status = (data['result']['assign_status']).split(',');
            this.statusDropdownDisable = true;
          }
          this.permission = data['result']['permission']
        }else{
          // do somethingd
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  SearchKycData() {
    this.ngxloader.start();
    // this.SearchFormData.value.token = this.userService.getToken();
    
    const formData = new FormData();
    formData.append('fromdate', this.SearchFormData.value.fromdate);
    formData.append('todate', this.SearchFormData.value.todate);
    formData.append('status', this.SearchFormData.value.status);
    formData.append('kycMode', this.SearchFormData.value.kycMode);
    formData.append('page', this.SearchFormData.value.page);
    formData.append('frommodifieddate', this.SearchFormData.value.frommodifieddate);
    formData.append('modifiedtodate', this.SearchFormData.value.modifiedtodate);
    formData.append('uservalue', this.SearchFormData.value.uservalue);
    formData.append('token', this.userService.getToken());
    formData.append('sortby', this.SearchFormData.value.sortby);
    formData.append('sortdata', this.SearchFormData.value.sortdata);

    // this.SearchFormData.value.fromdate = this.SearchFormData.value.fromdate;
    // this.SearchFormData.value.todate = this.SearchFormData.value.todate;
    // this.SearchFormData.value.frommodifieddate = this.SearchFormData.value.frommodifieddate;
    // this.SearchFormData.value.modifiedtodate = this.SearchFormData.value.modifiedtodate;
    // console.log(this.SearchFormData.value)
    this.ekycService.SearchKycData(formData).subscribe(
      response => {
        this.ngxloader.stop();
        this.showlist = true;
        this.kycList = response['result'].data;
        this.length  = response['result'].total;
      },
      err => {
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      }, () => {
        console.log('request completed!');
      }
    );
  }

  pageevent(event){
    this.SearchFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.SearchKycData();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openDialog(kycId) {
    this.getKycData(kycId);
  }

  getKycData(kycFormId){
    this.ngxloader.start();
    this.ekycService.getKycData(kycFormId).subscribe(
      data => {
        this.ngxloader.stop();
        if(data['code'] == 200) {
            this.kycData = data['result'];
            this.kycDataMsg = data['message'];
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
         this.ngxloader.stop();
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        if(this.kycData.islocked == "Yes"){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: this.kycDataMsg
          })
          
        }else {
          this.dialogRef = this.dialog.open(DialogDataKycDialog, {
            disableClose:true,
            maxWidth: '100vw',
            width: '90%',
            data: {
              kyc:this.kycData
            }
          });

          this.dialogRef.afterClosed().subscribe(result => {
            console.log('afterClosed event fired');
            this.unlockForm(this.kycData.id)
          });
        }
        
        console.log('request completed!');
      }
    );
  }

  unlockForm(kycFormId){
    this.ngxloader.start();
    const formData = new FormData();
    formData.append('kycFormId', kycFormId);
    formData.append('isLocked', "No");
    
    this.ekycService.unlockForm(formData).subscribe(
      data => {
        this.ngxloader.stop();
        if(data['code'] == 200) {
          //this.loader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          //this.loader=false;
          // this.works = data['result'];
        }
      },
      err => {        
        this.ngxloader.stop();
        console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.minToDate = event.value;
  }

}


@Component({
  selector: 'dialog-data-kyc-dialog',
  templateUrl: 'dialog-data-kyc-dialog.html',
  styleUrls: ['./dialog-data-kyc-dialog.css'],
})
export class DialogDataKycDialog {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  aadharForm: FormGroup;
  completeAdhar:any;
  msg:any;
  submitted:boolean=false;
  aadharsecond:any;
  aadharfirst:any;
  adharNumber:any;
  MobileFormGroup: FormGroup;
  PanFormGroup: FormGroup;
  BasicDetailsFormGroup: FormGroup;
  ResidentialAddressFormGroup: FormGroup;
  BusinessinformationFormGroup: FormGroup;
  BankVerificationFormGroup: FormGroup;
  ResidentialProofFormGroup: FormGroup;
  KycModeFormGroup: FormGroup;

  apiUrlVar:string='';
  token:string='';

  loader:Boolean=false;
  mobileError = '';
  kycFormId:string='';
  mobileNumber:string='';
 
  /* Pan details  */
  pan:string='';
  email:string='';
  applicant:string;
  panRemarks:any;
  customPanRemark:any;
  panLoader:Boolean =false;
  panFile:string='';
  panThumbFile:string='';
  selectedPanFile:File = null;
  selectedPPFile:File = null;
  panRemarksArray:Array<Remark> = [];
  /* End of pan details  */

  /* Basic details  */
  minDate: Date;
  maxDate: Date;
  name:string='';
  dob: any;
  parentName: any;
  gender: any;
  qualification: any;
  basicDetailsRemarks:any;
  isBankVerification = true;
  basicLoader: boolean=false;
  qualificationList:any =[];
  genders: Gender[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
    {value: 'Other', viewValue: 'Other'}
  ];

  BasicDetailsRemarkArray:Array<Remark>=[];

  /* End of Basic details  */

  /* Resendial Address */
  address: any;
  landmark: any;
  pincode: any;
  district: any;
  city: any;
  state: any;
  addressRemarks:any;
  resiLoader: boolean=false;
  ResidentialDetailsRemarkArray:Array<Remark>=[];
  /* End of Resendial Address */

  /* Business Information */
  firmName: any;
  workExp: any;
  firm_address: any;
  firm_landmark: any;
  firm_pincode: any;
  firm_district: any;
  firm_city: any;
  firm_state: any;
  works: any=[];
  businessRemarks:any;
  BusinessLoader: boolean=false;
  isResAddr = false;
  BusinesssRemarkArray:Array<Remark>=[];
   /* End of Business Information */

  /*Bank Details */
  accountType: any;
  bankName: any;
  ifsc: any;
  account: any;
  bankVerificationRemarks:any;
  accountTypes: any=[];
  bankList: any = [];
  BankLoader: boolean=false;
  bankFile:string='';
  selectedBankFile:File = null;
  BankRemarkArray:Array<Remark>=[];
  /* End of Bank Details*/ 

  VideoRemarkArray:Array<Remark>=[];

  /* Address proof */

  proof_type: string='';
  addressProofRemarks:any;
  ProofLoader: boolean=false;
  AddressProofRemarkArray:Array<Remark>=[];

  proofPhotoFile:string='';
  proofThumbPhotoFile:string='';
  proofPhotoBackFile:string='';
  proofThumbPhotoBackFile:string='';

  addressProofList: AddressProof[] = [
    {value: 'Voter ID', viewValue: 'Voter ID'},
    {value: 'Driving Licence', viewValue: 'Driving Licence'},
    {value: 'Passport', viewValue: 'Passport'},
    {value: 'Aadhar', viewValue: 'Aadhar'},
    
  ];
  /* End Address proof */
 
  /* KYC mode */
  kyc_mode: string;
  kycModeRemarks:any;
  kyCRemarksArray:any;
  kycModeLoader: boolean=false;
  kycModeList: KycMode[] = [
    {value: 'Aadhar OTP', viewValue: 'Aadhar OTP(Free)'},
    {value: 'Biometric', viewValue: 'Biometric(Free)'},
    {value: 'Non Aadhar', viewValue: 'Non Aadhar(Rs 100)'},
  ];
  /* end of KYC mode */

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  events: Array<string> = [];
  panExtractionResponseData: Array<string> = [];
  panFetchResponseData: any = {};
  addressProofResponseData: Array<string> = [];
  BankVerificationResponseData: Array<string> = [];
  VideoVerificationResponseData: any;

  panalbum: Array<Album> = [];

  remarksDataArray:any;
  panRemarksDataArray:any;
  bankRemarksDataArray:any
  aadharRemarksDataArray:any
  addressRemarksDataArray:any
  videoRemarksDataArray:any;

  VideoVerificationFormGroup: FormGroup;
  ForgeryResponseData: any;
  ImageQualityResponseData: any;
  NameMatchResponseData: any;
  category: any;
  physicallyHandicapped: any;
  alternateOccupationType: any;
  provider: any;
  entityType: any;
  alternateNumber: any;
  BiometricResponseData: any;
  kycStatus: any;
  aadhar_otp_response:any;

  categoryList = [
    {value: 'General', viewValue: 'General'},
    {value: 'OBC', viewValue: 'OBC'},
    {value: 'SC', viewValue: 'SC'},
    {value: 'ST', viewValue: 'ST'}
  ];

  physicallyhandicappedList = [
    {value: 'Handicaped', viewValue: 'Handicaped'},
    {value: 'Not Handicapped', viewValue: 'Not Handicapped'}
  ];
  
  alternateoccupationtypeList = [
    {value: 'Public Sector', viewValue: 'Public Sector'},
    {value: 'Self Employed', viewValue: 'Self Employed'},
    {value: 'Private', viewValue: 'Private'},
    {value: 'Others', viewValue: 'Others'},
    {value: 'None', viewValue: 'None'}
  ];

  providerList = [
    {value: 'Airtel', viewValue: 'Airtel'},
    {value: 'IDEA', viewValue: 'IDEA'},
    {value: 'VODA', viewValue: 'VODA'},
    {value: 'JIO', viewValue: 'JIO'}
  ];

  entitytypeList = [
    {value: 'Individual', viewValue: 'Individual'},
    {value: 'Sole Proprietorship', viewValue: 'Sole Proprietorship'},
    {value: 'Partnership Firm', viewValue: 'Partnership Firm'}
  ];
  secondaryProff: any;
  readonly maxSize = 104857600;
  bussinessRemarksDataArray: any;
  secAddRemarksDataArray: any;
  secAddressProofRemarks: any;
  secAddressProofRemarkArray:Array<Remark>=[]; 
  filteredOptions: Observable<string[]>;
  customRemark: any;
  showResponseFornonotp:boolean=true;
  responseAddhar = {type:"Fiat", model:"500", color:"white"};
 
  constructor(
    public dialogRef: MatDialogRef<DialogDataKycDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: EkycService,
    private userService:UserService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private _lightbox: Lightbox,
    private aadharservice:AadhaarService,
    private fb: FormBuilder
    ) {
      this.aadharForm = this.fb.group({
        secondnum1: ['', [Validators.required]],
        firstnum1: ['', [Validators.required]],
        thirdnum1: ['', [Validators.required]],
      });
      var response = '';
      this.aadharservice.getvalue().subscribe((res: any) => {
        if (res.firstnumber) {
          this.aadharfirst = res.firstnumber;
        }
        if (res.secondnumber) {
          this.aadharsecond = response.concat(res.secondnumber);
        }
      });
    }
   
  ngOnInit(): void {

    this.getRemarks('Pan Rejection');
    this.getRemarks('Aadhaar Rejection');
    this.getRemarks('Bank Rejection');
    this.getRemarks('Address Rejection');
    this.getRemarks('Video Kyc Rejection');
    this.getRemarks('Bussiness Rejection');
    this.getRemarks('Secondary Address Rejection');

    this.apiUrlVar = environment.apiBaseUrl;
    this.token = this.userService.getToken();

    if(this.data.kyc.id){
      this.kycFormId = this.data.kyc.id;
    }

    if(this.data.kyc.mobileNumber){
      this.mobileNumber = this.data.kyc.mobileNumber;
    }

    if(this.data.kyc.kyc_remarks!=null){   //for each form
      this.kyCRemarksArray = this.data.kyc.kyc_remarks;
      this.kyCRemarksArray.forEach(function (value) {
        switch(value.type){
          case 4:
            const remark4 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              custom_remark:value.customRemarks,
              created_at:value.created_at
            };
            this.panRemarksArray.push(remark4);
          break;

          case 5:
            const remark5 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.AddressProofRemarkArray.push(remark5);
          break;

          case 6:
            const remark6 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.BasicDetailsRemarkArray.push(remark6);
          break;

          case 7:
            const remark7 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks,
            };
            this.ResidentialDetailsRemarkArray.push(remark7);
          break;

          case 8:
            const remark8 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.BusinesssRemarkArray.push(remark8);
          break;

          case 9:
            const remark9 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.BankRemarkArray.push(remark9);
          break;

          case 10:
            const remark10 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.VideoRemarkArray.push(remark10);
            console.log(this.VideoRemarkArray);
          break;
          
          case 13:
            const remark13 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at,
              custom_remark:value.customRemarks
            };
            this.secAddressProofRemarkArray.push(remark13);
          break;
        }
    }.bind(this));
    }
    if(this.data.kyc.aadhar_otp_response!=null){
      this.aadhar_otp_response = this.data.kyc.aadhar_otp_response.response.result;
    }
    /*Pan Details */
    if(this.data.kyc.pan_details!=null){
      this.pan = this.data.kyc.pan_details.panNumber;
      this.applicant = this.data.kyc.pan_details.applicantName;
      this.panFile   = this.data.kyc.pan_details.panPhoto;
      this.panThumbFile   = this.data.kyc.pan_details.panThumbPhoto;
      this.mobileNumber = this.data.kyc.mobileNumber;
      for (let i = 1; i <= 1; i++) {
        const src = this.apiUrlVar+'/getPhoto?filename='+this.panFile+'&token='+this.userService.getToken();
        const caption = 'Pan Details';
        const thumb = this.apiUrlVar+'/getPhoto?filename='+this.panThumbFile+'&token='+this.userService.getToken();
        const album = {
            src: src,
            caption: caption,
            thumb: thumb
        };
        this.panalbum.push(album);
      } 

      
  }
    /*End of Pan Details */

    /*Basic Details */

    if(this.data.kyc.basic_details !=null){
      this.dob = this.data.kyc.basic_details.dob;
      this.name = this.data.kyc.basic_details.name;
      this.email = this.data.kyc.basic_details.email;
      //this.distributor = this.data.kyc.distributorId;
      this.parentName = this.data.kyc.basic_details.fatherName;
      this.gender = this.data.kyc.basic_details.gender;
      this.qualification = this.data.kyc.basic_details.qualification;
      this.category = this.data.kyc.basic_details.category;
      this.physicallyHandicapped = this.data.kyc.basic_details.physicallyHandicapped;
      this.alternateOccupationType = this.data.kyc.basic_details.alternateOccupationType;
      this.provider = this.data.kyc.basic_details.provider;
      this.entityType = this.data.kyc.basic_details.entityType;
      this.alternateNumber = this.data.kyc.basic_details.alternateNumber;
    }
    /*End of Basic Details*/

    /*Residentail Address */
    if(this.data.kyc.residential_details!=null){
      this.address  = this.data.kyc.residential_details.address;
      this.landmark = this.data.kyc.residential_details.landmark;
      this.pincode  = this.data.kyc.residential_details.pincode;
      this.district = this.data.kyc.residential_details.district;
      this.city     = this.data.kyc.residential_details.city;
      this.state    = this.data.kyc.residential_details.state;
    }

    /*End of Residentail Address*/

    /*Business Information */
  if(this.data.kyc.business_details!=null){
    this.firmName       = this.data.kyc.business_details.firmName;
    this.workExp        = this.data.kyc.business_details.workExperiance;
    this.firm_address   = this.data.kyc.business_details.businessAddress;
    this.firm_landmark  = this.data.kyc.business_details.businessLandmark;
    this.firm_pincode   = this.data.kyc.business_details.businessPincode;
    this.firm_district  = this.data.kyc.business_details.businessDistrict;
    this.firm_city      = this.data.kyc.business_details.businessCity;
    this.firm_state     = this.data.kyc.business_details.businessState;
  }

    /*End of Business Information*/

    /*Bank Details */

    if(this.data.kyc.bank_details!=null){
      this.accountType = this.data.kyc.bank_details.bankAccountType;
      this.bankName    = this.data.kyc.bank_details.bankName;
      this.ifsc        = this.data.kyc.bank_details.ifscCode;
      this.account     = this.data.kyc.bank_details.accountNumber;
      this.bankFile    = this.data.kyc.bank_details.passbookPhoto;
    }

    /*End of Bank Details*/

    /* Address Proof */

    if(this.data.kyc.residential_proof!=null){
      this.proof_type   = this.data.kyc.residential_proof.kyc_type;
      this.proofPhotoFile=this.data.kyc.residential_proof.proofPhoto;
      this.proofThumbPhotoFile=this.data.kyc.residential_proof.proofThumbPhoto;
      this.proofPhotoBackFile=this.data.kyc.residential_proof.proofPhotoBack;
      this.proofThumbPhotoBackFile = this.data.kyc.residential_proof.proofThumbPhotoBack;
    }

    /*End of Address Proof*/

     /* Final Confirmation */
    if(this.data.kyc.kyc_mode){
      this.kyc_mode   = this.data.kyc.kyc_mode;
      if(this.kyc_mode=='Non Aadhar'){
        this.addressProofList=[
          {value: 'Voter ID', viewValue: 'Voter ID'},
          {value: 'Driving Licence', viewValue: 'Driving Licence'},
          {value: 'Passport', viewValue: 'Passport'},
          
        ];
      }else if(this.kyc_mode=='Aadhar OTP'){
      }
    }

    if(this.data.kyc.kycStatus !=null && this.data.kyc.kycStatus !=''){
      this.kycStatus = this.data.kyc.kycStatus;
      console.log(this.kycStatus);
    }

    
    /*End of Final Confirmation*/

  this.PanFormGroup = new FormGroup({
    panRemarks:new FormControl("",Validators.compose([]))
      
  });
  // this.BasicDetailsFormGroup = new FormGroup({
  //   basicDetailsRemarks: new FormControl("",Validators.compose([])),
      
  // });
  
  this.MobileFormGroup = new FormGroup({
    mobile: new FormControl("",Validators.compose([
      Validators.required,
      Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"),
      Validators.minLength(10),
      Validators.maxLength(10)
    ])),
  });

  this.BasicDetailsFormGroup = new FormGroup({
    email: new FormControl("",Validators.compose([
      Validators.required
    ])),
    // name: new FormControl("",Validators.compose([
    //   Validators.required
    // ])),
    // mobile: new FormControl("",Validators.compose([
    //   Validators.required
    // ])),
    parentName: new FormControl("",Validators.compose([
      Validators.required
    ])),
    gender: new FormControl("",Validators.compose([
      Validators.required
    ])),
    dob: new FormControl("",Validators.compose([
      Validators.required
    ])),
    qualification: new FormControl("",Validators.compose([
      Validators.required
    ])),
    entityType: new FormControl("",Validators.compose([
      Validators.required
    ])),
    alternateNumber: new FormControl("",Validators.compose([
      Validators.required
    ])),
    physicallyhandicapped: new FormControl("",Validators.compose([
      Validators.required
    ])),
    alternateOccupationType: new FormControl("",Validators.compose([
      Validators.required
    ])),
    provider: new FormControl("",Validators.compose([
      Validators.required
    ])),
    name: new FormControl("",Validators.compose([
      Validators.required
    ])),
    category: new FormControl("",Validators.compose([
      Validators.required
    ]))
  });

  

  // this.ResidentialAddressFormGroup = new FormGroup({
  //   addressRemarks: new FormControl("",Validators.compose([])),
  // });

  this.ResidentialAddressFormGroup = new FormGroup({
    address: new FormControl("",Validators.compose([
      Validators.required
    ])),
    landmark: new FormControl("",Validators.compose([
      Validators.required
    ])),
    pincode: new FormControl("",Validators.compose([
      Validators.required,Validators.minLength(6),Validators.maxLength(6),Validators.pattern("[0-9]*")
    ])),
    district: new FormControl("",Validators.compose([
      Validators.required
    ])),
    city: new FormControl("",Validators.compose([
      Validators.required
    ])),
    state: new FormControl("",Validators.compose([
      Validators.required
    ])),
  });

  // this.BusinessinformationFormGroup = new FormGroup({
  //   businessRemarks: new FormControl("",Validators.compose([])),

  // });

  this.BusinessinformationFormGroup = new FormGroup({
    workExp: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firmName: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firm_address: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firm_landmark: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firm_pincode: new FormControl("",Validators.compose([
      Validators.required,Validators.minLength(6),Validators.maxLength(6),Validators.pattern("[0-9]*")
    ])),
    firm_district: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firm_city: new FormControl("",Validators.compose([
      Validators.required
    ])),
    firm_state: new FormControl("",Validators.compose([
      Validators.required
    ])),
  });

  // this.BankVerificationFormGroup = new FormGroup({
  //   bankVerificationRemarks : new FormControl("",Validators.compose([])),
  // });

  this.BankVerificationFormGroup = new FormGroup({
    accountType:new FormControl("",Validators.compose([
      Validators.required
    ])),
    bankName:new FormControl("",Validators.compose([
      Validators.required
    ])),
    bank_doc:new FormControl("",Validators.compose([
      Validators.required,FileValidator.maxContentSize(this.maxSize)
    ])),
    ifsc:new FormControl("",Validators.compose([
      Validators.required,Validators.minLength(11),Validators.maxLength(11),Validators.pattern("[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$")
    ])),
    account:new FormControl("",Validators.compose([
      Validators.required,Validators.minLength(8),Validators.maxLength(20)
    ])),
    caccount : new FormControl("",Validators.compose([
      Validators.required
    ])),
  });
  
  this.ResidentialProofFormGroup = new FormGroup({
    addressProofRemarks: new FormControl("",Validators.compose([])), 
  });

  this.KycModeFormGroup = new FormGroup({
    kycModeRemarks: new FormControl("",Validators.compose([])),
    kycMode: new FormControl("",Validators.compose([])),
  });
  
  this.VideoVerificationFormGroup = new FormGroup({
    VideoVerificationRemarks: new FormControl("",Validators.compose([])),
  });

  this.panFetchResponse();
  this.panExtractionResponse();
  this.videoVerificationResponse();
  this.forgeryResponse();
  this.getImageQualityResponse();
  this.getNameMatchResponse();
  this.getWorkExperienceList();
  this.getQualifications();
  this.getSecondaryProof();
  this.getAccTypeList();
  this.getBankList();
 
  if(this.proof_type!=''){
    this.addressProofResponse();
  }
  this.getBankVerificationResponse();
  if(this.kyc_mode =='Biometric'){
    this.getBiometricResponse();
  }

  this.filteredOptions = this.BankVerificationFormGroup.get("bankName").valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  
  }

  isSameAddressControl(val: any) {
    //console.log(val.currentTarget.checked);
    //console.log(typeof val.currentTarget.checked);
    if (val.currentTarget.checked) {
      this.BusinessinformationFormGroup.patchValue({
        "firm_address"  :  this.ResidentialAddressFormGroup.get('address').value,
        "firm_landmark" : this.ResidentialAddressFormGroup.get('landmark').value,
        "firm_pincode"  :  this.ResidentialAddressFormGroup.get('pincode').value,
        "firm_district" : this.ResidentialAddressFormGroup.get('district').value,
        "firm_city"     :     this.ResidentialAddressFormGroup.get('city').value,
        "firm_state"    :    this.ResidentialAddressFormGroup.get('state').value,
      })
    } else {
      this.BusinessinformationFormGroup.reset('');
    }
  }
getQualifications(){
  this.ekycService.getQualifications().subscribe(
    data => {
      if(data['status']) {
        this.qualificationList = data['result'];
      }else{
        // do something
      }
    },
    err => {        
       console.log(err);
      
    },() =>{
      console.log('request completed!');
    }
  );
}

getBankList(){
  this.ekycService.getBankList().subscribe(
    data => {
      if(data['status']) {
        this.bankList = data['result'];
      }else{
        //do something
      }
    },
    err => {        
       console.log(err);
      
    },() =>{
      console.log('request completed!');
    }
  );
}

private _filter(value: string): string[] {
  const filterValue = value.toLowerCase();
  return this.bankList.filter(option => (option.name).toLowerCase().includes(filterValue));
}

getAccTypeList(){
  this.ekycService.getAccTypeList().subscribe(
    data => {
      if(data['status']) {
        this.accountTypes = data['result'];
      }else{
        //do something
      }
    },
    err => {        
       console.log(err);
      
    },() =>{
      console.log('request completed!');
    }
  );
}

numberOnly(event): boolean {
  const charCode = event.which ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}

onSubmit() {
  this.submitted = true;
  if (this.aadharForm.invalid) {
    return;
  }
  this.completeAdhar = this.aadharfirst.concat(
    this.aadharsecond,
    this.aadharForm.controls.thirdnum1.value
  );

  if (this.aadharservice.isValidUidaiNumber(this.completeAdhar)) {
    console.log(this.completeAdhar);
  } else {
    console.log(this.completeAdhar);
    this.msg = 'Aadhar Number is Invalid';
  }
}


panopen(index: number): void {
  // open lightbox
  this._lightbox.open(this.panalbum, index, {fitImageInViewPort:true,wrapAround: true,
    positionFromTop:50,showZoom:true,
    showRotate:true,centerVertically:true});
}

close(): void {
  // close lightbox programmatically
  this._lightbox.close();
}

updatePanDetails() {
  this.panLoader = true;
  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  formData.append('panRemarks', this.PanFormGroup.value.panRemarks);

  this.ekycService.updatePanDetails(formData).subscribe(
    data => {
      if(data['code'] == 200) {
        this.panLoader=false;
        this.openSnackBar(data['message'],'Done!!');
        
      }else{
        this.panLoader=false;
        this.openSnackBar(data['message'],'Error!!');
      }
      
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
    },() =>{
      console.log('pan request completed!');
    }
  );
}

updateKycMode() {

  this.kycModeLoader = true;
  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  formData.append('kycModeRemarks', this.KycModeFormGroup.value.kycModeRemarks);
  formData.append('kycMode', this.KycModeFormGroup.value.kycMode);

  this.ekycService.updateKycMode(formData).subscribe(
    data => {
      if(data['code'] == 200) {
        this.kycModeLoader = false;
        this.openSnackBar(data['message'],'Done!!');
      }else{
        this.kycModeLoader = false;
        this.openSnackBar(data['message'],'Error!!');
      }
      
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
    },() =>{
      console.log('request completed!');
    }
  );
}

updateBasicDetails() {
  this.basicLoader = true;
  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  // formData.append('mobileNumber', this.mobileNumber);
  formData.append('email', this.email);
  formData.append('dob', this.BasicDetailsFormGroup.value.dob);
  formData.append('qualification', this.BasicDetailsFormGroup.value.qualification);
  formData.append('gender', this.BasicDetailsFormGroup.value.gender);
  formData.append('parentName', this.BasicDetailsFormGroup.value.parentName);
  
  formData.append('category', this.BasicDetailsFormGroup.value.category);
  formData.append('name', this.BasicDetailsFormGroup.value.name);
  formData.append('physicallyHandicapped', this.BasicDetailsFormGroup.value.physicallyhandicapped);
  formData.append('alternateOccupationType', this.BasicDetailsFormGroup.value.alternateOccupationType);
  formData.append('provider', this.BasicDetailsFormGroup.value.provider);
  formData.append('entityType', this.BasicDetailsFormGroup.value.entityType);
  formData.append('alternateNumber', this.BasicDetailsFormGroup.value.alternateNumber);
  // formData.append('basicDetailsRemarks', this.BasicDetailsFormGroup.value.basicDetailsRemarks);
  
  this.ekycService.updateBasicDetails(formData).subscribe(
    data => {
      if(data['status']) {
        this.basicLoader=false;
        this.openSnackBar(data['message'],'Done!!');
      }else{
        this.basicLoader=false;
        this.openSnackBar(data['message'],'Error!!');
        
      }
      
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
    },() =>{
      console.log('request completed!');
    }
  );
}

updateResidentialDetails() {
  this.resiLoader = true;

  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  formData.append('address', this.ResidentialAddressFormGroup.value.address);
  formData.append('landmark', this.ResidentialAddressFormGroup.value.landmark);
  formData.append('pincode', this.ResidentialAddressFormGroup.value.pincode);
  formData.append('district', this.ResidentialAddressFormGroup.value.district);
  formData.append('city', this.ResidentialAddressFormGroup.value.city);
  formData.append('state', this.ResidentialAddressFormGroup.value.state);

  this.ekycService.updateResidentialDetails(formData).subscribe(
    data => {
      if(data['code'] == 200) {
        this.resiLoader=false;
        this.openSnackBar(data['message'],'Done!!');
      }else{
        this.resiLoader=false;
        this.openSnackBar(data['message'],'Error!!');
      }	
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
    },() =>{
      console.log('request completed!');
    }
  );
}

updateBusinessDetails() {

  this.BusinessLoader = true;
  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  formData.append('firmName', this.BusinessinformationFormGroup.value.firmName);
  formData.append('firm_address', this.BusinessinformationFormGroup.value.firm_address);
  formData.append('firm_landmark', this.BusinessinformationFormGroup.value.firm_landmark);
  formData.append('firm_pincode', this.BusinessinformationFormGroup.value.firm_pincode);
  formData.append('firm_district', this.BusinessinformationFormGroup.value.firm_district);
  formData.append('firm_city', this.BusinessinformationFormGroup.value.firm_city);
  formData.append('firm_state', this.BusinessinformationFormGroup.value.firm_state);
  formData.append('workExp', this.BusinessinformationFormGroup.value.workExp);

  this.ekycService.updateBusinessDetails(formData).subscribe(
    data => {
      if(data['code'] == 200) {
        this.BusinessLoader=false;
        this.openSnackBar(data['message'],'Done!!');
      }else{
        this.BusinessLoader=false;
        this.openSnackBar(data['message'],'Error!!'); 
      }
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
      
    },() =>{
      console.log('request completed!');
    }
  );
}

  updateBankDetails() {
    this.BankLoader = true;
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    // formData.append('bankVerificationRemarks',this.BankVerificationFormGroup.value.bankVerificationRemarks);

    if(this.selectedBankFile!=null){
      formData.append('bank_doc', this.selectedBankFile,this.selectedBankFile.name);
    }

    formData.append('kycFormId', this.kycFormId);
    formData.append('accountType', this.BankVerificationFormGroup.value.accountType);
    formData.append('bankName', this.BankVerificationFormGroup.value.bankName);
    formData.append('ifsc', this.BankVerificationFormGroup.value.ifsc);
    formData.append('account', this.BankVerificationFormGroup.value.account);
    formData.append('caccount', this.BankVerificationFormGroup.value.caccount);

    this.ekycService.updateBankDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.openSnackBar(data['message'],'Done!!');
          this.BankLoader=false;
        }else{
          if(typeof data['result']['verificationFailed'] !== 'undefined' && data['result']['verificationFailed']){
            this.isBankVerification = false;
            this.openSnackBar('Bank Verification Failed, Upload Bank Doc','Done!!');
          }
          else {
            this.openSnackBar(data['message'],'Done!!');
          }
          this.BankLoader=false;
        }
      	
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  updateResidentialProof() {
    this.ProofLoader = true;
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('addressProofRemarks', this.ResidentialProofFormGroup.value.addressProofRemarks);

    this.ekycService.updateResidentialProof(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ProofLoader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          this.ProofLoader=false;
          
          this.openSnackBar(data['message'],'Error!!');
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  onConfirm(){
    
    if(!this.isResAddr){
      this.firm_address  = this.address;
      this.firm_landmark = this.landmark;
      this.firm_pincode  = this.pincode;
      this.firm_district = this.district;
      this.firm_city     = this.city;
      this.firm_state    = this.state;
    }
  }

  onBankSelect(event){

    let list         = this.bankList;
    let selectedList = list.filter(function (list) { return list.name == event.value });

    if(selectedList[0]['verificationAvailable']){
      this.isBankVerification = true;
      this.BankVerificationFormGroup.get("bank_doc").clearValidators();
      this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();
      
    }
    else {
      this.isBankVerification = false;
      this.BankVerificationFormGroup.get("bank_doc").setValidators([Validators.required]);
      this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();

    }
    this.ifsc = selectedList[0]['ifsc'];
  }

  onFilePanSelected(event){
    this.selectedPanFile = <File>event.target.files[0];
  }
  
  onFilePPSelected(event){
    this.selectedPPFile = <File>event.target.files[0];
  }

  onFileBankSelected(event){
    this.selectedBankFile = <File>event.target.files[0];
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }

  unlockForm(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.unlockForm(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          //this.loader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          //this.loader=false;
          
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
        
      },() =>{
        console.log('request completed!');
    
      }
    );
  }

  rejectForm(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.rejectForm(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.dialogRef.close();
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
            
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
 
  }

  approvePanDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve pan details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approvePanDetails(formData).subscribe(
          data => {
            if(data['status']) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          },() =>{
            console.log('request completed!');
          }
        );
      }
    });
  }

  rejectPanDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject this pan details.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.panRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.panRemarks = remarksField
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('panRemarks', this.panRemarks);

          this.ekycService.rejectPanDetails(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
                this.addCustomRemarks(data['result']['id'])
              }else{
                //this.loader=false;
              }
            },
            err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
              
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  approveAddressProof(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve address proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveAddressProof(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }

  rejectAddressProof(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject address proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {

        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.addressRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.addressProofRemarks = remarksField;
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('addressProofRemarks', this.addressProofRemarks);
          this.ekycService.rejectAddressProof(formData).subscribe(
            data => {
              if(data['status']) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
                this.addCustomRemarks(data['result']['id'])
              }else{
                //this.loader=false;
                this.openSnackBar(data['message'],'Error!!');
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }
              
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  approveBasicDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve basic details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveBasicDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }

  rejectBasicDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject basic details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.remarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.basicDetailsRemarks = remarksField;
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('basicDetailsRemarks',this.basicDetailsRemarks);
          this.ekycService.rejectBasicDetails(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }
            },() =>{
              console.log('request completed!');
            }
          );
        }
      }
    });
  }

  approveResidentialDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve basic details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveResidentialDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }

          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }

  rejectResidentialDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject basic details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.remarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.addressRemarks = remarksField
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('addressRemarks',this.addressRemarks);
          this.ekycService.rejectResidentialDetails(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }

            },() =>{
              console.log('request completed!');

            }
          );
        }
        
      }
    });
  }

  approveBusinessDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve business details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveBusinessDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }

  rejectBusinessDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject business details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.bussinessRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.businessRemarks = remarksField;
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('businessRemarks',this.businessRemarks);
          this.ekycService.rejectBusinessDetails(formData).subscribe(
            data => {
              if(data['status']) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
                this.addCustomRemarks(data['result']['id'])
              }else{
                //this.loader=false;
                this.openSnackBar(data['message'],'Error!!');
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }

            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  approveBankDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve business details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveBankDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }
  
  rejectBankDetails(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject bank details.",
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.bankRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if (remarksField) {
          const formData = new FormData();
          this.bankVerificationRemarks = remarksField;
          formData.append('kycFormId', this.kycFormId);
          formData.append('bankVerificationRemarks',this.bankVerificationRemarks);
          this.ekycService.rejectBankDetails(formData).subscribe(
            data => {
              if(data['status']) {
                this.loader=false;
                this.addCustomRemarks(data['result']['id'])
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
                this.openSnackBar(data['message'],'Error!!');
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }
              
            },() =>{
              console.log('request completed!');
            }
          );
        }
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  panFetchResponse(){

    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.panFetchResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.panFetchResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  panExtractionResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.panExtractionResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.panExtractionResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addressProofResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('address_proof_type',this.proof_type);
    this.ekycService.addressProofResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.addressProofResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getBankVerificationResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getBankVerificationResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.BankVerificationResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  videoVerificationResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.getVideoVerificationResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          if(data['result'] != null){ 
            this.VideoVerificationResponseData = data['result'];
            console.log(this.VideoVerificationResponseData['finalMatchImage2']);
          }
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  approveVideoVerification(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve video details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveVideoDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.addCustomRemarks(data['result']['id'])
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          },() =>{
            console.log('request completed!');
          }
        );
      }
    });
  }

  rejectVideoVerification(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject this pan details.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.videoRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.panRemarks = remarksField
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('videoRemarks', this.panRemarks);
          this.ekycService.rejectVideoDetails(formData).subscribe(
            data => {
              if(data['status']) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
                this.addCustomRemarks(data['result']['id']);
              }else{
                //this.loader=false;
                this.openSnackBar(data['message'],'Error!!');
              }
            },
            err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
              
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  forgeryResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getForgeryResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ForgeryResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getImageQualityResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getImageQualityResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ImageQualityResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getNameMatchResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getNameMatchResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.NameMatchResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
 
  getRemarks(type){
    this.ekycService.getRemarksByType(type).subscribe(
      data => {
        if(data['status']) {
          switch(data['type']){
            case 'Pan Rejection':
              this.panRemarksDataArray = data['result'];
              break;
            case 'Bank Rejection':
              this.bankRemarksDataArray = data['result'];
              break;
            case 'Aadhaar Rejection':
              this.aadharRemarksDataArray = data['result'];
              break;
            case 'Address Rejection':
              this.addressRemarksDataArray = data['result'];
              break;
            case 'Video Kyc Rejection':
                this.videoRemarksDataArray = data['result'];
                break;
            case 'Bussiness Rejection':
                this.bussinessRemarksDataArray = data['result'];
                break;
            case 'Secondary Address Rejection':
                this.secAddRemarksDataArray = data['result'];
                break;
            default: 
              this.remarksDataArray = data['result'];
            break; 
          
          }
        }else{
          // do something
        }
      },
      err => {        
          console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  
  getBiometricResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getBiometricResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.BiometricResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getSecondaryProof(){

    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.getSecondaryProof(formData).subscribe(
      data => {
        if(data['status']) {
          this.secondaryProff = data['result'];
          console.log(this.secondaryProff);
        }else{
          //do something
        }
      },
      err => {        
         console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getWorkExperienceList(){
    this.ekycService.getWorkExperienceList().subscribe(
      data => {
        if(data['status']) {
          this.works = data['result'];
          //this.loader=false;
        }else{
          //this.loader=false;
        }
      },
      err => {        
         console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addSecAddressProof(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('type', this.kyc_mode);

    this.ekycService.addSecAddressProof(formData).subscribe(
      data => {
        if(data['code']) {
          this.openSnackBar(data['message'],'Done!!');
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  approveSecondaryAddressProof() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve address proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveSecondaryAddressProof(formData).subscribe(
          data => {
            if (data['code'] == 200) {
              this.loader = false;
              this.openSnackBar(data['message'], 'Done!!');
            } else {
              //this.loader=false;
            }
          },
          err => {
            console.log(err);
            if (err['status'] == 401) {
              window.location.replace(err['error']['returnURL']);
            }

          }, () => {
            console.log('request completed!');

          }
        );
      }
    });
  }
  rejectSecondaryAddressProof(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject address secondary proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {

        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.secAddRemarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.secAddressProofRemarks = remarksField;
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('secAddressProofRemarks', this.secAddressProofRemarks);
          this.ekycService.rejectSecAddressProof(formData).subscribe(
            data => {
              if(data['status']) {
                this.loader=false;
                this.addCustomRemarks(data['result']['id'])
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
                this.openSnackBar(data['message'],'Error!!');
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }
              
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  updateMobile(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('mobileNumber', this.mobileNumber);
    
    this.ekycService.updateMobile(formData).subscribe(
      data => {
        if(data['status']) {
          // this.basicLoader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          // this.basicLoader=false;
          this.openSnackBar(data['message'],'Error!!');
          
        }
        
      },
      err => {        
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
    }

    getRequeryData(){
    
      const formData = new FormData();
      formData.append('kycFormId', this.kycFormId);
      formData.append('mobileNumber', this.mobileNumber);
      this.loader = true;
      this.ekycService.rblRequeryFromPortal(formData).subscribe(
        data => {
          if(data['status']) {
            this.loader = false;
            this.openSnackBar(data['message'],'Done!!');
            this.BiometricResponseData = data['result'];
          }else{
            this.loader = false;
            this.openSnackBar(data['message'],'Error!!');
            
          }
          
        },
        err => {        
          console.log(err);
          if(err['status'] == 401){
            window.location.replace(err['error']['returnURL']);
          }
        },() =>{
          console.log('request completed!');
        }
      );
    }

  addCustomRemarks(remarkId) {
    Swal.fire({
      title: 'Add custom remark',
      input: 'text',
      inputPlaceholder: 'Add any custom remark if required',
      showCancelButton: true,
      cancelButtonColor: '#d33',
    }).then((result) => {
      console.log(result)
      if (result.value) {
        this.customRemark = result.value;
        const formData = new FormData();
        formData.append('customRemark', this.customRemark);
        formData.append('id', remarkId);
        this.ekycService.addCustomRemark(formData).subscribe(
          data => {
            if (data['status']) {
              this.openSnackBar(data['message'], 'Done!!');
            } else {
              this.openSnackBar(data['message'], 'Error!!');
            }
          },
          err => {
            console.log(err);
          }, () => {
            console.log('request completed!');
          }
        );
      }
      this.customRemark = '';
    })
  }

}

interface Gender {
  value: string;
  viewValue: string;
}

interface AddressProof {
  value: string;
  viewValue: string;
}

interface KycMode {
  value: string;
  viewValue: string;
}

interface Album {
  src: string;
  caption: string;
  thumb:string
}

interface Remarks {
  value: string;
  viewValue: string;
}

interface Remark {
  type: string;
  remark: string;
}