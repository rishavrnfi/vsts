import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators,FormGroupDirective, NgForm } from '@angular/forms';
import { EkycService } from '../services/ekyc.service';
import { FileValidator } from 'ngx-material-file-input';
import Swal from 'sweetalert2';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute, CanActivate } from '@angular/router'; 
import { environment } from '../../environments/environment';
import { MatRadioChange } from '@angular/material/radio';
declare var $: any;

@Component({
  selector: 'app-dashboardv1',
  templateUrl: './dashboardv1.component.html',
  styleUrls: ['./dashboardv1.component.css'],
  providers:[EkycService]
})
export class Dashboardv1Component implements  OnInit {

  BiometricFormGroup:FormGroup;
  client_code:string='';
  api_key:string='';
  redirect_url:string='';
  mobile:string='';
  apiUrlVar:string='';
  biometricloader:Boolean=false;
  
  constructor(
    private ekycService:EkycService,
    private userService:UserService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
    }

  ngOnInit(): void {
    this.apiUrlVar = environment.apiBaseUrl;
    this.route.queryParams.subscribe(params => {
        this.mobile = params['mobile'];
        sessionStorage.setItem("mobile", this.mobile);
       
    });

    this.BiometricFormGroup = new FormGroup({
      mobile: new FormControl("",Validators.compose([
        Validators.required
      ]))
    });
  }
  
  // logMeOut(){
  //   sessionStorage.removeItem('session_token');
  //   this.router.navigateByUrl('login');
  // }

  bioMetricSubmit() {
    this.biometricloader = true;
    const formData = new FormData();
    formData.append('mobile', this.mobile);

    this.ekycService.bioMetricSubmitv1(formData).subscribe(
      data => {
        if(data['status']) {
          this.biometricloader=false;
          window.location.assign(data['result']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: data['message']
          });
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }
}
