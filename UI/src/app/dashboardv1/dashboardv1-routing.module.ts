import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Dashboardv1Component } from './dashboardv1.component';

const routes: Routes = [{ path: '', component: Dashboardv1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Dashboardv1RoutingModule { }
