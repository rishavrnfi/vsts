import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Dashboardv1RoutingModule } from './dashboardv1-routing.module';
import { Dashboardv1Component } from './dashboardv1.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';


@NgModule({
  declarations: [Dashboardv1Component],
  imports: [
    CommonModule,
    Dashboardv1RoutingModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class Dashboardv1Module { }
