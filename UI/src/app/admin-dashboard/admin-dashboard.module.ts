import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxUiLoaderModule } from 'ngx-ui-loader';
import { AdminDashboardRoutingModule } from './admin-dashboard-routing.module';
import { AdminDashboardComponent } from './admin-dashboard.component';


@NgModule({
  declarations: [AdminDashboardComponent],
  imports: [
    CommonModule,
    AdminDashboardRoutingModule,
    NgxUiLoaderModule
  ]
})
export class AdminDashboardModule { }
