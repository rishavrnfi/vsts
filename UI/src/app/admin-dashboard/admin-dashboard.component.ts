import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  dashboardStats : any = [];
  loader: Boolean = false;
  user:any;
  constructor(
    private userService:UserService,
    private adminService:AdminService,
    private router: Router,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService
    ) {
      this.user = this.jwtauth.isAuthenticatedjwttoken();
     }

  ngOnInit(): void {
    this.loginvalidate();
    // this.loader=true;
    this.ngxloader.start();
    const formData = new FormData();
    this.adminService.getAdminDashboard(formData).subscribe(
			data => {
        this.ngxloader.stop();
				this.dashboardStats = data['result'];			
			}, err =>{
        console.log(err);
			}, () =>{
				console.log('Request Completed!');
			}
    )
    console.log(this.user.type)
  }
  
  async loginvalidate(){
    await this.userService.validatePage();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}
