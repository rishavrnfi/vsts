import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators,FormGroupDirective, NgForm } from '@angular/forms';
import { EkycService } from '../../services/ekyc.service';
import { FileValidator } from 'ngx-material-file-input';
import Swal from 'sweetalert2';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, CanActivate } from '@angular/router'; 
import { environment } from '../../../environments/environment';
import { MatRadioChange } from '@angular/material/radio';
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[EkycService]
})
export class DashboardComponent implements  OnInit {

  MobileFormGroup:FormGroup;
  PanFormGroup: FormGroup;
  BasicDetailsFormGroup: FormGroup;
  ResidentialAddressFormGroup: FormGroup;
  BusinessinformationFormGroup: FormGroup;
  BankVerificationFormGroup: FormGroup;
  ConfirmFormGroup: FormGroup;
  ResidentialProofFormGroup: FormGroup;
  KycModeFormGroup: FormGroup;
  AadharOtpFormGroup:FormGroup;
  BiometricFormGroup:FormGroup;

  client_code:string='';
  api_key:string='';
  redirect_url:string='';
  request_id:string='';
  stan:string='';
  hash:string='';
  mobile:string='';
  otp_required:String='';
  aadharotploader:Boolean=false;
  biometricloader:Boolean=false;
  aadharotpform:Boolean=false;
  biometricform:Boolean=false;
  biometricKyc:Boolean=false;
  addressproofform:Boolean=false;

  AadharOtpCredentials:any=[];
  initWebVIKYCURL='';
  videoId = '';
  videoVerified:Boolean=false;


  //Veri5Digital
  requestIdResponse:string='';
  userIdResponse:string='';
  uuidResponse:string='';
  hashResponse:string='';
  statusResponse:string='';
  aadhar_otp:string='';

  // end of veri5digital

  apiUrlVar:string='';
  kycData: any;
  passport:Boolean=false;
  driving:Boolean=false;
  voter:Boolean=false;

  /* KYC mode */
  kyc_mode: string;
  /* end of KYC mode */

  /* Pan details  */
  pan:string='';
  applicantName:string='';
  panRemarks:any;
  panLoader:Boolean =false;
  panFile:string='';
  selectedPanFile:File = null;

  /* End of pan details  */

  /* Basic details  */
    minDate= new Date(2000, 0, 1);
    maxDate= new Date(2020, 0, 1);
    dob: any;
    email:string='';
    name:string='';
    parentName: any;
    gender: any;
    qualification: any;
    basicDetailsRemarks:any;
    isBankVerification = true;
    basicLoader: boolean=false;
    qualificationList:any =[];

  genders: Gender[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
    {value: 'Other', viewValue: 'Other'}
  ];

  categoryList: Category[] = [
    {value: 'General', viewValue: 'General'},
    {value: 'OBC', viewValue: 'OBC'},
    {value: 'SC', viewValue: 'SC'},
    {value: 'ST', viewValue: 'ST'}
  ];

  physicallyhandicappedList: physically[] = [
    {value: 'Handicaped', viewValue: 'Handicaped'},
    {value: 'Not Handicapped', viewValue: 'Not Handicapped'}
  ];
  
  alternateoccupationtypeList: alternateOcc[] = [
    {value: 'Public Sector', viewValue: 'Public Sector'},
    {value: 'Self Employed', viewValue: 'Self Employed'},
    {value: 'Private', viewValue: 'Private'},
    {value: 'Others', viewValue: 'Others'},
    {value: 'None', viewValue: 'None'}
  ];

  providerList: Provider[] = [
    {value: 'Airtel', viewValue: 'Airtel'},
    {value: 'IDEA', viewValue: 'IDEA'},
    {value: 'VODA', viewValue: 'VODA'},
    {value: 'JIO', viewValue: 'JIO'}
  ];

  entitytypeList: Entity[] = [
    {value: 'Individual', viewValue: 'Individual'},
    {value: 'Sole Proprietorship', viewValue: 'Sole Proprietorship'},
    {value: 'Partnership Firm', viewValue: 'Partnership Firm'}
  ];

  /* End of Basic details  */

  /* Resendial Address */
  address: any;
  landmark: any;
  pincode: any;
  district: any;
  city: any;
  state: any;
  addressRemarks:any;
  resiLoader: boolean=false;
  category: any;
  physicallyhandicapped: any;
  provider: any;
  entitytype: any;
  alternatenumber: any;
  alternateoccupationtype: any;
  /* End of Resendial Address */

  /* Business Information */
  firmName: any;
  workExp: any;
  firm_address: any;
  firm_landmark: any;
  firm_pincode: any;
  firm_district: any;
  firm_city: any;
  firm_state: any;
  works: any=[];
  businessRemarks:any;
  BusinessLoader: boolean=false;
  isResAddr = false;
 /* End of Business Information */

 /*Bank Details */
  accountType: any;
  bankName: any;
  ifsc: any;
  account: any;
  bankVerificationRemarks:any;
  accountTypes: any=[];
  bankList: any = [];
  pincodes: any = [];
  BankLoader: boolean=false;
  bankFile:string='';
  selectedBankFile:File = null;
/* End of Bank Details*/ 

/* Address proof */

  proof_type: string;
  voter_epicNumber:string;
  voter_state:string;
  dl_number:string;
  dl_dob:string;
  passport_file_number:string;
  passport_dob:string;

  aadhar_doc: any;
  addressProofRemarks:any;
  ProofLoader: boolean=false;
  aadharFile:string='';
  selectedAadharFile:File = null;
  addressProofList: AddressProof[] = [
    {value: 'Passport', viewValue: 'Passport'},
    {value: 'Voter ID', viewValue: 'Voter ID'},
    {value: 'Driving Licence', viewValue: 'Driving Licence'}
  ];
/* End Address proof */

  isEditable = true;
  loader:Boolean=false;
  mobileNumber:string='';

  readonly maxSize = 104857600;
  stage=0;
  checkTerms:any;
  comment:any;

/*steper code */
  steperBlock:Boolean=false;
  tab4:Boolean=false;
  tab5:Boolean=false;
  tab6:Boolean=false;
  tab7:Boolean=false;
  tab8:Boolean=false;
  tab9:Boolean=false;
  tab10:Boolean=false;
  tab11:Boolean=false;
  currentTab = 0;

/* End of Steper code*/
  agent_token:string='';
  stages:any;
  kyCRemarksArray: any;
  showVerificationProgres: Boolean=false;
  states: { value: string; display: string; }[];
  bio_video: any;
  
  constructor(
    private ekycService:EkycService,
    private userService:UserService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
      const currentYear = new Date().getFullYear();
      this.minDate = new Date(currentYear - 50, 0, 1);
      this.maxDate = new Date(currentYear + 1, 11, 31);
    }

  ngOnInit(): void {
    this.apiUrlVar = environment.apiBaseUrl;
    this.route.queryParams.subscribe(params => {
        this.agent_token = params['agent_token'];
        sessionStorage.setItem("agent_token", this.agent_token);
        this.requestIdResponse = params['requestId'];
        this.userIdResponse    = params['userId'];
        this.uuidResponse      = params['uuid'];
        this.hashResponse      = params['hash'];
        this.statusResponse    = params['status'];
        this.videoId           = params['id'];
        this.aadhar_otp        = params['aadhar_otp'];
        this.bio_video         = params['bio_video']; 
    });

    if(typeof this.statusResponse !=='undefined' && this.statusResponse=='FAIL'){
      this.getAgentCodeByRequestId(this.requestIdResponse);
    }

    if(typeof this.requestIdResponse !=='undefined' && typeof this.userIdResponse !=='undefined'
     && typeof this.uuidResponse !=='undefined' && typeof this.hashResponse !=='undefined' && typeof this.statusResponse !=='undefined')
    {
        this.steperBlock=true;
        this.aadharotpform=true;
        this.ekycService.saveAadharOtpResponse({
          requestId:this.requestIdResponse,
          userId:this.userIdResponse,
          uuid:this.uuidResponse,
          hash:this.hashResponse,
          status:this.statusResponse
        }).subscribe(
          data => {
            if(data['status']) {
              this.loader=false;
              this.agent_token = data['agent_token'];
              sessionStorage.setItem('agent_token',this.agent_token);
              this.fillFormData(data['result']);
              Swal.fire({
                icon: 'success',
                title: 'Aadhar OTP Verfication',
                html: data['message']
              });
              this.stages = data['stages'];
              this.stage = data['stages'][0];
              this.showSteps(this.stage);
            }else{
              this.loader=false;
              this.agent_token = data['agent_token'];
              sessionStorage.setItem('agent_token',this.agent_token);
              this.fillFormData(data['result']);
              this.stages = data['stages'];
              this.stage = data['stages'][0];
              this.showSteps(this.stage);
              if(Array.isArray(data['message'])){
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  html: data['message'].join(" <br/> ")
                });
              }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  html: data['message']
                });
              }
            }
          },
          err => {        
             console.log(err);      
          },() =>{
            console.log('request completed!');
            this.router.navigate(['agent'], { 
              queryParams: 
              { 
                agent_token: this.agent_token
              }
            });
          }
        );
    }

    if(typeof this.videoId !=='undefined' && this.videoId !=''){
      this.steperBlock=true;
      this.ekycService.getVideoVerification({mobile: this.videoId}).subscribe(
        data => {
          if(data['status']) {
            this.loader=false;
            this.agent_token = data['result']['agent_token'];
            sessionStorage.setItem('agent_token',this.agent_token);
            this.fillFormData(data['result']);
            Swal.fire({
              icon: 'success',
              title: 'Video Kyc',
              html: data['message']
            });
            this.stages = data['stages'];
            this.stage = data['stages'][0];
            this.showSteps(this.stage);
          }else{
            this.loader=false;
            this.agent_token = data['result']['agent_token'];
            sessionStorage.setItem('agent_token',this.agent_token);
            this.fillFormData(data['result']);
            this.stage = 10;
            
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message']
            });
            
            if(data['result']['verificationProgres']){
              this.showVerificationProgres = true;
            }

            this.stages = data['stages'];
            this.stage = data['stages'][0];
            this.showSteps(this.stage);
          }
        },
        err => {        
            console.log(err);
        },() =>{
          console.log('request completed!');
          this.router.navigate(['agent'], { 
            queryParams: 
            { 
              agent_token: this.agent_token
            }
          });
        }
      );  
    }

    this.getBankList();
    this.getQualifications();
    this.getWorkExperienceList();
    this.getAccTypeList();
    this.getPincodes();
    if(typeof this.requestIdResponse !=='undefined'){
        
    }else{
      this.getAadharOtpCredentials(sessionStorage.getItem('agent_token'));
    }
    if(typeof this.aadhar_otp !=='undefined'){
      this.getAadharOtpCredentials(this.agent_token);
      this.steperBlock=true;
      this.aadharotpform=true;    
    }

    this.PanFormGroup = new FormGroup({
      pan_doc:new FormControl("",Validators.compose([
        Validators.required
      ]))
    });

    this.MobileFormGroup = new FormGroup({
      mobileNumber:new FormControl("",Validators.compose([
        Validators.required,
        Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"),
        Validators.minLength(10),
        Validators.maxLength(10)
      ])),
    });

    this.BasicDetailsFormGroup = new FormGroup({
        dob: new FormControl("",Validators.compose([
          Validators.required
        ])),
        name: new FormControl("",Validators.compose([
          Validators.required
        ])),
        email: new FormControl("",Validators.compose([
          Validators.required,Validators.email
        ])),
        parentName: new FormControl("",Validators.compose([
          Validators.required,Validators.minLength(3),Validators.maxLength(100)
        ])),
        gender: new FormControl("",Validators.compose([
          Validators.required
        ])),
        qualification: new FormControl("",Validators.compose([
          Validators.required
        ])),
        category: new FormControl("",Validators.compose([
          Validators.required
        ])),
        physicallyhandicapped: new FormControl("",Validators.compose([
          Validators.required
        ])),
        provider: new FormControl("",Validators.compose([
          Validators.required
        ])),
        entitytype: new FormControl("",Validators.compose([
          Validators.required
        ])),
        alternatenumber: new FormControl("",Validators.compose([
          Validators.required
        ])),
        alternateoccupationtype: new FormControl("",Validators.compose([
          Validators.required
        ])),
    });

    this.ResidentialAddressFormGroup = new FormGroup({
      address: new FormControl("",Validators.compose([
        Validators.required
      ])),
      landmark: new FormControl("",Validators.compose([
        Validators.required
      ])),
      pincode: new FormControl("",Validators.compose([
        Validators.required,Validators.minLength(6),Validators.maxLength(6),Validators.pattern("[0-9]*")
      ])),
      district: new FormControl("",Validators.compose([
        Validators.required
      ])),
      city: new FormControl("",Validators.compose([
        Validators.required
      ])),
      state: new FormControl("",Validators.compose([
        Validators.required
      ])),
    });

    this.BusinessinformationFormGroup = new FormGroup({
      workExp: new FormControl("",Validators.compose([
        Validators.required
      ])),
      firmName: new FormControl("",Validators.compose([
        Validators.required
      ])),
      resAddrconfirm: new FormControl("",Validators.compose([
        
      ])),
      firm_address: new FormControl("",Validators.compose([
        Validators.required
      ])),
      firm_landmark: new FormControl("",Validators.compose([
        Validators.required
      ])),
      firm_pincode: new FormControl("",Validators.compose([
        Validators.required,Validators.minLength(6),Validators.maxLength(6),Validators.pattern("[0-9]*")
      ])),
      firm_district: new FormControl("",Validators.compose([
        Validators.required
      ])),
      firm_city: new FormControl("",Validators.compose([
        Validators.required
      ])),
      firm_state: new FormControl("",Validators.compose([
        Validators.required
      ])),
    });

    this.BankVerificationFormGroup = new FormGroup({
      accountType:new FormControl("",Validators.compose([
        Validators.required
      ])),
      bankName:new FormControl("",Validators.compose([
        Validators.required
      ])),
      bank_doc:new FormControl("",Validators.compose([
        Validators.required,FileValidator.maxContentSize(this.maxSize)
      ])),
      ifsc:new FormControl("",Validators.compose([
        Validators.required,Validators.minLength(11),Validators.maxLength(11),Validators.pattern("[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$")
      ])),
      account:new FormControl("",Validators.compose([
        Validators.required,Validators.minLength(8),Validators.maxLength(20)
      ])),
      caccount : new FormControl("",Validators.compose([
        Validators.required
      ])),
    });

    this.ConfirmFormGroup = new FormGroup({
      checkTerms:new FormControl("",Validators.compose([
        Validators.required
      ])),
      comment:new FormControl("",Validators.compose([
      ])),
    });

    this.ResidentialProofFormGroup = new FormGroup({
      proof_type: new FormControl("",Validators.compose([
        Validators.required
      ])),
      
      voter_epicNumber: new FormControl("",Validators.compose([
      ])),
      voter_state: new FormControl("",Validators.compose([
      ])),
      dl_number: new FormControl("",Validators.compose([
      ])),
      dl_dob: new FormControl("",Validators.compose([
      ])),
      passport_file_number: new FormControl("",Validators.compose([
      ])),
      passport_dob: new FormControl("",Validators.compose([
      ]))
    });

    this.AadharOtpFormGroup = new FormGroup({
      client_code: new FormControl("",Validators.compose([
        Validators.required
      ])),
      api_key: new FormControl("",Validators.compose([
        Validators.required
      ])),
      redirect_url: new FormControl("",Validators.compose([
        Validators.required
      ])),
      request_id: new FormControl("",Validators.compose([
        Validators.required
      ])),
      stan: new FormControl("",Validators.compose([
        Validators.required
      ])),
      hash: new FormControl("",Validators.compose([
        Validators.required
      ])),
      mobile: new FormControl("",Validators.compose([
        Validators.required
      ])),
      otp_required: new FormControl("",Validators.compose([
        Validators.required
      ])), 
    });

    this.BiometricFormGroup = new FormGroup({
      mobile: new FormControl("",Validators.compose([
        Validators.required
      ]))
    });

    if(typeof this.bio_video !=='undefined' && this.bio_video=='success'){
      this.steperBlock=true;
      this.showSteps(10);
    }
  }

  changeAddreesProof(value){
    
    if(value=='Passport'){
      
      this.passport = true;
      this.driving  = false;
      this.voter    = false;

      this.ResidentialProofFormGroup.get("passport_file_number").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("passport_file_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("passport_dob").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("passport_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("dl_number").clearValidators();
      this.ResidentialProofFormGroup.get("dl_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("dl_dob").clearValidators();
      this.ResidentialProofFormGroup.get("dl_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("voter_state").clearValidators();
      this.ResidentialProofFormGroup.get("voter_state").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("voter_epicNumber").clearValidators();
      this.ResidentialProofFormGroup.get("voter_epicNumber").updateValueAndValidity();

    }
    if(value=='Voter ID'){
      this.passport = false;
      this.driving  = false;
      this.voter    = true;

      this.ResidentialProofFormGroup.get("passport_file_number").clearValidators();
      this.ResidentialProofFormGroup.get("passport_file_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("passport_dob").clearValidators();
      this.ResidentialProofFormGroup.get("passport_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("dl_number").clearValidators();
      this.ResidentialProofFormGroup.get("dl_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("dl_dob").clearValidators();
      this.ResidentialProofFormGroup.get("dl_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("voter_state").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("voter_state").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("voter_epicNumber").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("voter_epicNumber").updateValueAndValidity();

    }
    if(value=='Driving Licence'){
      this.passport = false;
      this.driving  = true;
      this.voter    = false;

      this.ResidentialProofFormGroup.get("passport_file_number").clearValidators();
      this.ResidentialProofFormGroup.get("passport_file_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("passport_dob").clearValidators();
      this.ResidentialProofFormGroup.get("passport_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("dl_number").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("dl_number").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("dl_dob").setValidators([Validators.required]);
      this.ResidentialProofFormGroup.get("dl_dob").updateValueAndValidity();

      this.ResidentialProofFormGroup.get("voter_state").clearValidators();
      this.ResidentialProofFormGroup.get("voter_state").updateValueAndValidity();
      this.ResidentialProofFormGroup.get("voter_epicNumber").clearValidators();
      this.ResidentialProofFormGroup.get("voter_epicNumber").updateValueAndValidity();
    }
   
  }

  resPin(){
    let selectedPinCode =this.pincode;
    let selectedPinArr = this.pincodes.filter(function(pin){
     return selectedPinCode == pin.pincode
    });
    if( typeof selectedPinArr[0] !=='undefined' && selectedPinArr[0]['state']){
      this.city = selectedPinArr[0]['state'];
    }else{
      this.city = '';
    }
  }

  firmPin(){
    let selectedPinCode =this.firm_pincode;
    let selectedPinArr = this.pincodes.filter(function(pin){
     return selectedPinCode == pin.pincode
    });
    if( typeof selectedPinArr[0] !=='undefined' && selectedPinArr[0]['state']){
      this.firm_city = selectedPinArr[0]['state'];
    }else{
      this.firm_city = '';
    }
  }

  agentMobileVerify() {
    this.loader = true;
    const formData = new FormData();
    formData.append('mobile', this.mobileNumber);
    this.ekycService.agentMobileVerify(formData).subscribe(
      data => {
        if(data['status']) {
          this.kycData = data['result']; 
          if(this.kycData.formCompleted){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Form is already submitted.'
            }).then((result) => {
              if (result.value) {
                this.loader=false;
                this.MobileFormGroup.reset();
            }});
          }
          else{
            this.fillFormData(this.kycData);
            this.steperBlock=true;
            this.loader=false;
            this.stages = data['stages'];
            this.stage = data['stages'][0];
            this.showSteps(this.stage);
          }        
          
        }else{
          if(data['code']==203){
            Swal.fire({
              icon: 'error',
              title: 'Onboarding',
              text: data['message']
            }).then((result) => {
              if (result.value) {
                this.loader=false;
                window.location.replace(data['returnURL']);
            }});
          }
          else{
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: data['message']
            }).then((result) => {
              if (result.value) {
                this.loader=false;
                this.MobileFormGroup.reset();
            }});
          }
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  uploadPanPhoto() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    if(this.selectedPanFile!=null){
      formData.append('pan_doc', this.selectedPanFile,this.selectedPanFile.name);
    }
    formData.append('mobile', this.mobileNumber);
  
    this.ekycService.uploadPanPhoto(formData).subscribe(
      data => {
        if(data['status']) {
          if(data['result']['basicDetails']!=null){
            this.dob  = data['result']['basicDetails']['dob'];
            this.name = data['result']['basicDetails']['name'];
          }
          this.loader=false;
          this.nextStep();
        }else{
          this.loader=false;
          if(Array.isArray(data['message'])){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message'].join(" <br/> ")
            });
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message']
            });
          }
        }
      },
      err => {        
         console.log(err);      
      },() =>{
        console.log('request completed!');
      }
    );
  }

  submitForm(){
    this.loader = true;
    const formData = new FormData();
    console.log(this.checkTerms);
    formData.append('isConfirm', this.checkTerms);
    formData.append('comment', this.comment);
    formData.append('mobile', this.mobileNumber);
    this.ekycService.confirmForm(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          Swal.fire({
            icon: 'success',
            title: 'Confirmed',
            text: "Form Submitted Successfully" ,
            showConfirmButton: false,
            timer: 1500
          });
          this.mobileNumber='';
          this.MobileFormGroup.reset();
          this.steperBlock = false;
        }else{
          this.loader=false;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          })
        }
      	
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addAddressProof() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);

    if(this.passport_file_number!=''){
      formData.append('passport_file_number', this.passport_file_number);
    }
    if(this.passport_dob!=''){
      formData.append('passport_dob', this.passport_dob);
    }
    if(this.dl_number!=''){
      formData.append('dl_number', this.dl_number);
    }
    if(this.dl_dob!=''){
      formData.append('dl_dob', this.dl_dob);
    }
    if(this.voter_epicNumber!=''){
      formData.append('voter_epicNumber', this.voter_epicNumber);
    }
    if(this.voter_state!=''){
      formData.append('voter_state', this.voter_state);
    }
   
    formData.append('proof_type', this.ResidentialProofFormGroup.value.proof_type);
  
    this.ekycService.addAddressProof(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          if(data['result']['resDetails']!=null){
            this.state = data['result']['resDetails']['state'];
            this.pincode = data['result']['resDetails']['pincode'];
            this.district = data['result']['resDetails']['district'];
            this.city = data['result']['resDetails']['city'];
            this.address = data['result']['resDetails']['address'];
          }
          if(data['result']['basicDetails']!=null){
            this.dob = data['result']['basicDetails']['dob'];
            this.parentName = data['result']['basicDetails']['fatherName'];
            this.gender = data['result']['basicDetails']['gender'];
          }
          this.nextStep();
        }else{
          this.loader=false;
          if(Array.isArray(data['message'])){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message'].join(" <br/> ")
            });
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message']
            });
          }
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }
  
  addBasicDetails() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    formData.append('dob', this.BasicDetailsFormGroup.value.dob);
    formData.append('email', this.BasicDetailsFormGroup.value.email);
    formData.append('qualification', this.BasicDetailsFormGroup.value.qualification);
    formData.append('gender', this.BasicDetailsFormGroup.value.gender);
    formData.append('parentName', this.BasicDetailsFormGroup.value.parentName);
    
    formData.append('category', this.BasicDetailsFormGroup.value.category);
    formData.append('physicallyHandicapped', this.BasicDetailsFormGroup.value.physicallyhandicapped);
    formData.append('alternateOccupationType', this.BasicDetailsFormGroup.value.alternateoccupationtype);
    formData.append('provider', this.BasicDetailsFormGroup.value.provider);
    formData.append('entityType', this.BasicDetailsFormGroup.value.entitytype);
    formData.append('alternateNumber', this.BasicDetailsFormGroup.value.alternatenumber);

    this.ekycService.addBasicDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.nextStep();  
        }else{
          this.loader=false;
          this.PanFormGroup.reset();
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addResidentialDetails() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    formData.append('address', this.ResidentialAddressFormGroup.value.address);
    formData.append('landmark', this.ResidentialAddressFormGroup.value.landmark);
    formData.append('pincode', this.ResidentialAddressFormGroup.value.pincode);
    formData.append('district', this.ResidentialAddressFormGroup.value.district);
    formData.append('city', this.ResidentialAddressFormGroup.value.city);
    formData.append('state', this.ResidentialAddressFormGroup.value.state);

    this.ekycService.addResidentialDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.nextStep();
        }else{
          this.loader=false;
        }
      },
      err => {     
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addBusinessDetails() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    formData.append('firmName', this.BusinessinformationFormGroup.value.firmName);
    formData.append('firm_address', this.BusinessinformationFormGroup.value.firm_address);
    formData.append('firm_landmark', this.BusinessinformationFormGroup.value.firm_landmark);
    formData.append('firm_pincode', this.BusinessinformationFormGroup.value.firm_pincode);
    formData.append('firm_district', this.BusinessinformationFormGroup.value.firm_district);
    formData.append('firm_city', this.BusinessinformationFormGroup.value.firm_city);
    formData.append('firm_state', this.BusinessinformationFormGroup.value.firm_state);
    formData.append('workExp', this.BusinessinformationFormGroup.value.workExp);

    this.ekycService.addBusinessDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.nextStep();
        }else{
          this.loader=false;
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addBankDetails() {
    this.loader = true;
    const formData = new FormData();
    if(this.selectedBankFile!=null){
      formData.append('bank_doc', this.selectedBankFile,this.selectedBankFile.name);
    }
    formData.append('mobile', this.mobileNumber);
    formData.append('accountType', this.BankVerificationFormGroup.value.accountType);
    formData.append('bankName', this.BankVerificationFormGroup.value.bankName);
    formData.append('ifsc', this.BankVerificationFormGroup.value.ifsc);
    formData.append('account', this.BankVerificationFormGroup.value.account);
    formData.append('caccount', this.BankVerificationFormGroup.value.caccount);
    if(this.BankVerificationFormGroup.value.account != this.BankVerificationFormGroup.value.caccount){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Account number is not equal to Confirm account number'
      });
      this.loader=false;
      return false;
    }
    this.ekycService.addBankDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.nextStep();
        }else{
          this.loader=false;
          if( typeof data['result']['verificationFailed'] !== 'undefined' && data['result']['verificationFailed']){
            this.isBankVerification = false;
            this.BankVerificationFormGroup.get("bank_doc").setValidators([Validators.required]);
            this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();
          }
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: data['message']
          });
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getBankList(){
    this.ekycService.getBankList().subscribe(
      data => {
        if(data['status']) {
          this.bankList = data['result'];
        }else{
          //do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  
  getQualifications(){
    this.ekycService.getQualifications().subscribe(
      data => {
        if(data['status']) {
          this.qualificationList = data['result'];
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  
  getWorkExperienceList(){
    this.ekycService.getWorkExperienceList().subscribe(
      data => {
        if(data['status']) {
          this.works = data['result'];
          //this.loader=false;
        }else{
          //this.loader=false;
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getAccTypeList(){
    this.ekycService.getAccTypeList().subscribe(
      data => {
        if(data['status']) {
          this.accountTypes = data['result'];
        }else{
          //do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getPincodes(){
    this.ekycService.getPincodes().subscribe(
      data => {
        if(data['status']) {
          this.pincodes = data['result'];
        }else{
          //do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  onConfirm(){
    if(!this.isResAddr){
      this.firm_address  = this.address;
      this.firm_landmark = this.landmark;
      this.firm_pincode  = this.pincode;
      this.firm_district = this.district;
      this.firm_city     = this.city;
      this.firm_state    = this.state;
    }
  }
  onBankSelect(event){
    let list = this.bankList;
    let selectedList =list.filter(function (list) { return list.name == event.value });

    if(selectedList[0]['verificationAvailable']){
      this.isBankVerification = true;
      this.BankVerificationFormGroup.get("bank_doc").clearValidators();
      this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();
      
    }
    else {
      this.isBankVerification = false;
      this.BankVerificationFormGroup.get("bank_doc").setValidators([Validators.required]);
      this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();
    }
    this.ifsc = selectedList[0]['ifsc'];
  }

  onFilePanSelected(event){
    this.selectedPanFile = <File>event.target.files[0];
  }
  

  onFileBankSelected(event){
    this.selectedBankFile = <File>event.target.files[0];
  }

  onAadharSelected(event){
    this.selectedAadharFile = <File>event.target.files[0];
  }
 
  logMeOut(){
    sessionStorage.removeItem('session_token');
    this.router.navigateByUrl('login');
  }

  nextStep(){
    let index = (this.stages.indexOf(this.stage) + 1);
    this.stage = this.stages[index];
    this.showSteps(this.stage);
  }

   showSteps(n) {

    switch(n) { 
      case 4: { 
        this.tab4=true;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
        this.tab10=false;
        this.tab11=false;
        break; 
      } 
      case 5: { 
        this.tab4=false;
        this.tab5=true;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
        this.tab10=false;
        this.tab11=false;
        break; 
      } 
      case 6: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=true;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
        this.tab10=false;
        this.tab11=false;
        break; 
      } 
      case 7: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=false;
        this.tab7=true;
        this.tab8=false;
        this.tab9=false;
        this.tab10=false;
        this.tab11=false;
        break; 
      } 
      case 8: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=true;
        this.tab9=false;
        this.tab10=false;
        this.tab11=false;
        break; 
      } 
      case 9: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=true;
        this.tab10=false;
        this.tab11=false;
        break; 
      }
      case 10: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
        this.tab10=true;
        this.tab11=false;
        break; 
      }
      case 11: { 
        this.tab4=false;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
        this.tab10=false;
        this.tab11=true;
        break; 
      } 
      default: { 
        this.tab4=true;
        this.tab5=false;
        this.tab6=false;
        this.tab7=false;
        this.tab8=false;
        this.tab9=false;
         break; 
      } 
   }
    this.fixStepIndicator(n)
  }
  prevStep(){
    if(this.stages.indexOf(this.stage)){
      let index = (this.stages.indexOf(this.stage) - 1);
      this.stage = this.stages[index];
      this.showSteps(this.stage);
    }
    else{
      this.steperBlock=false;
    }
  }

  fixStepIndicator(n) {
    n = n-4;
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
  }


  fillFormData(data){
    this.mobileNumber = data.mobileNumber;
    /*Pan Details */
    if(data.pan_details!=null){
      this.panFile   = data.pan_details.panPhoto;
      this.pan = data.pan_details.panNumber;
      this.applicantName = data.pan_details.applicantName;

      if(this.panFile && data.kycRemarks.stage_4.length == 0){
        this.PanFormGroup.get("pan_doc").clearValidators();
        this.PanFormGroup.get("pan_doc").updateValueAndValidity();
      }

    }
    /*End of Pan Details */

    if(data.kyc_mode){
      this.kyc_mode = data.kyc_mode;
      
      if(this.kyc_mode=='Biometric'){
        if(data.biometricKyc=='0'){
          this.biometricform= true;
          this.biometricKyc = true;
        }
        else if(data.cspOnBoardingStage1=='0'){

          this.router.navigate(['onboarding'], { 
            queryParams: 
            { 
              agent_token: this.agent_token
            }
          });

        }else if(data.cspOnBoardingStage2=='0'){
          this.getRblConsentStatus();
        }
      }
      else if(this.kyc_mode=='Non Aadhar'){

        this.addressproofform=true;
        this.addressProofList=[
          {value: 'Voter ID', viewValue: 'Voter ID'},
          {value: 'Driving Licence', viewValue: 'Driving Licence'},
          {value: 'Passport', viewValue: 'Passport'},
          
        ];
      }else if(this.kyc_mode=='Aadhar OTP'){
        this.aadharotpform=true;
      }      
     }

    /*Basic Details */

    if(data.basic_details !=null){
      this.dob = data.basic_details.dob;
      this.name = data.basic_details.name;
      this.email = data.basic_details.email;
      this.parentName = data.basic_details.fatherName;
      this.gender = data.basic_details.gender;
      this.qualification = data.basic_details.qualification;
      this.category = data.basic_details.category;
      this.physicallyhandicapped = data.basic_details.physicallyHandicapped;
      this.provider = data.basic_details.provider;
      this.entitytype = data.basic_details.entityType;
      this.alternatenumber = data.basic_details.alternateNumber;
      this.alternateoccupationtype = data.basic_details.alternateOccupationType;
    }
    /*End of Basic Details*/

    /*Residentail Address */
    if(data.residential_details!=null){
      this.address  = data.residential_details.address;
      this.landmark = data.residential_details.landmark;
      this.pincode  = data.residential_details.pincode;
      this.district = data.residential_details.district;
      this.city     = data.residential_details.city;
      this.state    = data.residential_details.state;
    }

    /*End of Residentail Address*/

    /*Business Information */
  if(data.business_details!=null){
    this.firmName       = data.business_details.firmName;
    this.workExp        = data.business_details.workExperiance;
    this.firm_address   = data.business_details.businessAddress;
    this.firm_landmark  = data.business_details.businessLandmark;
    this.firm_pincode   = data.business_details.businessPincode;
    this.firm_district  = data.business_details.businessDistrict;
    this.firm_city      = data.business_details.businessCity;
    this.firm_state     = data.business_details.businessState;
  }

    /*End of Business Information*/

    /*Bank Details */

    if(data.bank_details!=null){
      this.accountType = data.bank_details.bankAccountType;
      this.bankName    = data.bank_details.bankName;
      this.ifsc        = data.bank_details.ifscCode;
      this.account     = data.bank_details.accountNumber;
      this.bankFile    = data.bank_details.passbookPhoto;

      if(this.bankFile){
        this.BankVerificationFormGroup.get("bank_doc").clearValidators();
        this.BankVerificationFormGroup.get("bank_doc").updateValueAndValidity();
      }
    }

    /*End of Bank Details*/
    /* Address Proof */

    if(data.residential_proof!=null){
      this.proof_type   = data.residential_proof.kyc_type;
      this.aadharFile   = data.residential_proof.proofPhoto;
      this.changeAddreesProof(this.proof_type);
    }
    /*End of Address Proof*/

    if(data.kycRemarks!=null){   //for each form
      this.kyCRemarksArray = data.kycRemarks;
    }

    if(data.videoVerification){
      this.videoVerified = data.videoVerification;
    }
  }

  async getRblConsentStatus() {
		await this.findRblConsentStatus().then(response=>{
      console.log(response);
      if(response==0){
         window.location.replace('https://uid.rblbank.com/CustomerLogin/PPIAgentEkyc.aspx?ref=CAxJsY1SmRAqS5eOCTSPtQ==');
      }
		}).catch(err=>{
			console.log(err);
		})
  }
  
  findRblConsentStatus(){
		return new Promise((resolve,reject)=>{
      const formData = new FormData();
      formData.append('agent_token', this.agent_token);
      this.ekycService.getRBLConsentStatusData(formData).subscribe(
        data => {
          if(data['status']){
            resolve(1);
          }else{
            reject(0);
          }
        },
        err => {     
           console.log(err);
           reject(err);
        },() =>{
          console.log('request completed!');
        }
      );
		});
	}

  aadharOtpSubmit(e){
    e.target.submit();
  }

  getAadharOtpCredentials(agent_token){

    this.ekycService.getAadharOtpCredentials(agent_token).subscribe(
      data => {
        if(data['status']){
          this.AadharOtpCredentials = data['result'];
          this.client_code     = this.AadharOtpCredentials['client_code'];
          this.api_key         = this.AadharOtpCredentials['api_key'];
          this.redirect_url    = this.AadharOtpCredentials['redirect_url'];
          this.request_id      = this.AadharOtpCredentials['request_id'];
          this.stan            = this.AadharOtpCredentials['stan'];
          this.hash            = this.AadharOtpCredentials['hash'];
          this.mobile          = this.AadharOtpCredentials['mobile'];
          this.otp_required    = this.AadharOtpCredentials['otp_required'];
          this.initWebVIKYCURL = this.AadharOtpCredentials['initWebVIKYC'];

          this.fillFormData(data['data']);
          this.stages = data['stages'];
          this.stage = data['stages'][0];
          this.showSteps(this.stage);
          
        }else{
          //do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getVideoUrl(){
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    this.ekycService.getVideoKycUrl(formData).subscribe(
      data => {
        if(data['status']){
          window.location.replace(data['result']['videoUrl']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Something Went wrong"
          });
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getVideoResponse(){
    
    this.ekycService.checkVideoResponse({mobile: this.mobileNumber}).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          Swal.fire({
            icon: 'success',
            title: 'Video Kyc',
            html: data['message']
          });
          this.stages = data['stages'];
          this.stage = data['stages'][0];
          this.showVerificationProgres = false;
          this.showSteps(this.stage);
        }else{
          this.loader=false;
          
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: data['message']
          });
          if(data['result']['verificationProgres']){
            this.showVerificationProgres = true;
          }
          else {
            this.showVerificationProgres = false;
          }
          
          if(data['stages']){
            this.stages = data['stages'];
            this.stage = data['stages'][0];
            this.showSteps(this.stage);
          } 
        }
      },
      err => {        
          console.log(err);
      },() =>{
        console.log('request completed!');
      }
    ); 
    
  }

  getAgentCodeByRequestId(requestId){
    this.loader = true;
    const formData = new FormData();
    formData.append('refrenceId', requestId);
    this.ekycService.getAgentCodeByRequestId(formData).subscribe(
      data => {
        if(data['status']){
          this.router.navigate(['agent'], { 
            queryParams: 
            { 
              agent_token: data['result'],
              aadhar_otp:'fail'
            }
          });
          this.steperBlock=true;
          this.aadharotpform=true;
          this.getAadharOtpCredentials(data['result']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: "Something Went wrong"
          });
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  bioMetricSubmit() {
    this.biometricloader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);

    this.ekycService.bioMetricSubmit(formData).subscribe(
      data => {
        if(data['status']) {
          this.biometricloader=false;
          window.location.assign(data['result']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: data['message']
          });
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

}

interface Gender {
  value: string;
  viewValue: string;
}

interface Qualification {
  value: string;
  viewValue: string;
}

interface Work {
  value: string;
  viewValue: string;
}

interface AddressProof {
  value: string;
  viewValue: string;
}

interface KycMode {
  value: string;
  viewValue: string;
}

interface Category {
  value: string;
  viewValue: string;
}

interface physically {
  value: string;
  viewValue: string;
}

interface alternateOcc {
  value: string;
  viewValue: string;
}

interface Provider {
  value: string;
  viewValue: string;
}

interface Entity {
  value: string;
  viewValue: string;
}
