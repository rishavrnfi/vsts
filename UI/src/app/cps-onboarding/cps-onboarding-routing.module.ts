import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CpsOnboardingComponent } from './cps-onboarding.component';

const routes: Routes = [{ path: '', component: CpsOnboardingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CpsOnboardingRoutingModule { }
