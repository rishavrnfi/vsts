import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EkycService } from '../services/ekyc.service';

@Component({
  selector: 'app-cps-onboarding',
  templateUrl: './cps-onboarding.component.html',
  styleUrls: ['./cps-onboarding.component.css']
})
export class CpsOnboardingComponent implements OnInit {
  agent_token: any;

  constructor(
    private route: ActivatedRoute,
    private ekycService:EkycService,
  ) { }

  loader:Boolean=true;

  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      this.agent_token = params['agent_token'];
    });

    this.cspOnboarding();

  }

  cspOnboarding(){
    window.location.assign('https://uid.rblbank.com/CustomerLogin/PPIAgentEkyc.aspx?ref=CAxJsY1SmRAqS5eOCTSPtQ==');
  }
}
