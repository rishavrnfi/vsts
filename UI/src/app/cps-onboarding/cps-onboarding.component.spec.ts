import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpsOnboardingComponent } from './cps-onboarding.component';

describe('CpsOnboardingComponent', () => {
  let component: CpsOnboardingComponent;
  let fixture: ComponentFixture<CpsOnboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpsOnboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpsOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
