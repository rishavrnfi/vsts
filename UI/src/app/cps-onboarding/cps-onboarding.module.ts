import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CpsOnboardingRoutingModule } from './cps-onboarding-routing.module';
import { CpsOnboardingComponent } from './cps-onboarding.component';


@NgModule({
  declarations: [CpsOnboardingComponent],
  imports: [
    CommonModule,
    CpsOnboardingRoutingModule
  ]
})
export class CpsOnboardingModule { }
