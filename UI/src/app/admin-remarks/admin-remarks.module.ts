import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRemarksRoutingModule } from './admin-remarks-routing.module';
import { AdminRemarksComponent, DialogDataRemarksDialog } from './admin-remarks.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
  declarations: [AdminRemarksComponent,DialogDataRemarksDialog],
  imports: [
    CommonModule,
    AdminRemarksRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatDialogModule,
    MatCardModule,
    MatSlideToggleModule
  ]
})
export class AdminRemarksModule { }
