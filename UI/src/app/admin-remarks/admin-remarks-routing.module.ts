import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminRemarksComponent } from './admin-remarks.component';

const routes: Routes = [{ path: '', component: AdminRemarksComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRemarksRoutingModule { }
