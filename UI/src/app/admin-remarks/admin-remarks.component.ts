import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../services/user.service';
import { EkycService } from '../services/ekyc.service';
import { Router } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-admin-remarks',
  templateUrl: './admin-remarks.component.html',
  styleUrls: ['./admin-remarks.component.css']
})
export class AdminRemarksComponent implements OnInit {
  user:any;
  loader:Boolean = false;
  showaddremakrs:Boolean = false;
  RemarksFormGroup:FormGroup;
  remark_type:string='';
  remark:string='';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  length   = 100;
  pageSize = 25;
  pageEvent: PageEvent;
  page:number=1;

  showlist:Boolean=false;
  remarksList: any = [];
  remarksData: any;
  dialogRef: any;
  isChecked:Boolean=true;
  GetRemarksFormGroup: FormGroup;

  constructor(
    private userService:UserService,
    private ekycService: EkycService,
    private router: Router,
    public jwtauth: AuthjwtService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    ) {
      this.user = this.jwtauth.isAuthenticatedjwttoken();
    }

  ngOnInit(): void {

    this.RemarksFormGroup = new FormGroup({
      remark:new FormControl("",Validators.compose([
        Validators.required
      ])),
      remark_type:new FormControl("",Validators.compose([
        Validators.required
      ])),
      status:new FormControl("",Validators.compose([
      ])),
    });

    this.GetRemarksFormGroup = new FormGroup({
      remark_type:new FormControl("",Validators.compose([
      ])),
      page:new FormControl("",Validators.compose([
      ]))
    });

    this.allremarks();
  }

  showAddRemarks(){
    if(this.showaddremakrs){
      this.showaddremakrs = false;
    }
    else {
      this.showaddremakrs = true;
    }
  }

  addRemarks(){
    const formData = new FormData();
    formData.append('remark_type', this.RemarksFormGroup.value.remark_type);
    formData.append('remark', this.RemarksFormGroup.value.remark);
    formData.append('status', this.RemarksFormGroup.value.status);
    this.ekycService.addRemarks(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.allremarks();
          this.RemarksFormGroup.reset();
          this.openSnackBar(data['message'],'Done!!');
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
        
      },() =>{
        console.log('request completed!');
      }
    );

  }

  allremarks(){
    this.loader = true;
    const formData = new FormData();
    formData.append('page', this.GetRemarksFormGroup.value.page);
    formData.append('remark_type', this.GetRemarksFormGroup.value.remark_type);
    this.ekycService.allremarks(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.showlist = true;
          this.loader = false;
          this.remarksList = data['result'].data;
          this.length      = data['result'].total;
        }else{
          this.loader = false;
          // do something
        }
      },
      err => {        
       	console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openDialog(remarkId) {
    this.getRemarkData(remarkId);
  }

  getRemarkData(kycFormId){
    this.ekycService.getRemarkData(kycFormId).subscribe(
      data => {
        if(data['code'] == 200) {
            this.remarksData = data['result'];
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{ 
        this.dialogRef = this.dialog.open(DialogDataRemarksDialog, {
          width:'100vw',
          data: {
            remark:this.remarksData
          }
        });

        this.dialogRef.afterClosed().subscribe(result => {
          this.allremarks();
          console.log('afterClosed event fired');
        });
        console.log('request completed!');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  pageevent(event){
    console.log(event.pageIndex)
    this.GetRemarksFormGroup.value.page = event.pageIndex+1;
    // if(event.pageIndex<2){
    //   this.page = 2;
    // }
    // else{
      this.page = event.pageIndex+1;
    // }
    this.allremarks();
  }

  changeStatus(e:MatSlideToggleChange,remarksId){
    const formData = new FormData();
    formData.append('remarkId',remarksId);
    if(e.checked){
      formData.append('status','1');
    }else{
      formData.append('status','0');
    }
    this.ekycService.changeRemarkStatus(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.allremarks();
          this.openSnackBar(data['message'],'Done!!');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );

  }
}

@Component({
  selector: 'dialog-data-remarks-dialog',
  templateUrl: 'dialog-data-remarks-dialog.html',
  styleUrls: ['./dialog-data-remarks-dialog.css'],
})
export class DialogDataRemarksDialog {

  UpdateRemarkFormGroup:FormGroup;
  remark_type:string='';
  remark:string='';
  remarkFormId:string='';
  status:any='';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isChecked:Boolean=false;

  constructor(
    public dialogRef: MatDialogRef<DialogDataRemarksDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: EkycService,
    private userService:UserService,
    private router: Router,
    private _snackBar: MatSnackBar,
    ) {}

    ngOnInit(): void {

      this.UpdateRemarkFormGroup = new FormGroup({
        remark:new FormControl("",Validators.compose([
          Validators.required
        ])),
        remark_type:new FormControl("",Validators.compose([
          Validators.required
        ])),
        status:new FormControl("",Validators.compose([
        ])),
          
      });

      if(this.data.remark.id!=''){
        this.remarkFormId = this.data.remark.id;
        this.remark = this.data.remark.comments;
        this.remark_type = this.data.remark.type;
        if(this.data.remark.status){
          this.isChecked = true;
        }
      }
      
    }

    updateRemark(){
      const formData = new FormData();
      formData.append('remarkFormId', this.remarkFormId);
      formData.append('remark', this.remark);
      formData.append('remark_type', this.remark_type);
      formData.append('status', this.UpdateRemarkFormGroup.value.status);
      this.ekycService.updateRemark(formData).subscribe(
        data => {
          if(data['code'] == 200) {
            this.openSnackBar(data['message'],'Done!!');
            
          }else{
            this.openSnackBar(data['message'],'Error!!');
          }
          
        },
        err => {        
          console.log(err);
          if(err['status'] == 401){
            window.location.replace(err['error']['returnURL']);
          }
        },() =>{
          console.log('pan request completed!');
        }
      );

    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['mat-snack-bar-container'],
      });
    }
   
}
