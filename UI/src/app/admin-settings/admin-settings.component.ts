import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { AdminService } from '../services/admin.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css'],
  // providers:[EkycService]
})
export class AdminSettingsComponent implements OnInit {
  SettingsFormGroup: FormGroup;
  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  nameMatchMinValue: string;
  nameMatchMaxValue: string;
  imgQualityMaxValue: string;
  imgQualityMinValue: string;
  videoMatchValue: string;
  addressMatchValue: string;
  videoVerificationMode: any;
  loader:Boolean=false;
  
  constructor(
    private userService:UserService,
    private adminService:AdminService,
    private router: Router,
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService
    ) {
      this.user = this.jwtauth.isAuthenticatedjwttoken();
      this.apiBaseUrl= environment.apiBaseUrl;
      this.tokenvar = this.userService.getToken();
     }


  ngOnInit(): void {
    this.SettingsFormGroup = new FormGroup({
      nameMatchMaxThreshold:new FormControl("",Validators.compose([
        Validators.required,
        Validators.max(1)
      ])),
      nameMatchMinThreshold:new FormControl("",Validators.compose([
        Validators.required,
        Validators.max(1)
      ])),
      imgQualityMinThreshold:new FormControl("",Validators.compose([
        Validators.required,
        Validators.max(1)

      ])),
      videoKycThreshold:new FormControl("",Validators.compose([
        Validators.required,
        Validators.max(100)
      ])),
      addressMatchThreshold:new FormControl("",Validators.compose([
        Validators.required,
        Validators.max(100)
      ])),
      videoVerification:new FormControl("",Validators.compose([
        // Validators.required,
        // Validators.max(100)
      ]))
    });

    this.getThresholds();
  }
  
  addThresholdSettings(){
    this.loader = true;
    const formData = new FormData();
    formData.append('nameMatchMaxValue', this.nameMatchMaxValue);
    formData.append('nameMatchMinValue', this.nameMatchMinValue);
    formData.append('imgQualityValue', this.imgQualityMinValue);
    formData.append('videoMatchValue', this.videoMatchValue);
    formData.append('addressMatchValue', this.addressMatchValue);
    formData.append('videoVerificationMode', this.videoVerificationMode);
  
    this.adminService.updateThresholdSettings(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          
        }else{
          this.loader=false;
        }
      },
      err => {        
        console.log(err); 
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }     
      },() =>{
        console.log('request completed!');
      }
    );
  }


  getThresholds(){
    this.ngxloader.start();
    this.adminService.getThresholds().subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          this.ngxloader.stop();
          if(data['result'] != null && data['result'] != ''){   
            this.nameMatchMinValue  = data['result']['nameMatchMinValue'];
            this.nameMatchMaxValue  = data['result']['nameMatchMaxValue'];
            this.imgQualityMinValue = data['result']['imgQualityValue'];
            this.videoMatchValue    = data['result']['videoMatchValue'];
            this.addressMatchValue  = data['result']['addressMatchValue'];
            this.videoVerificationMode  = Math.floor(data['result']['videoVerificationMode']);
            console.log(this.videoVerificationMode)
          }
          
        }else{
          this.ngxloader.stop();
        }
      },
      err => {        
        console.log(err); 
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }     
      },() =>{
        console.log('request completed!');
      }
    );
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}
