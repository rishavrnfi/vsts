import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgentErrorRoutingModule } from './agent-error-routing.module';
import { AgentErrorComponent } from './agent-error.component';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  declarations: [AgentErrorComponent],
  imports: [
    NgxUiLoaderModule,
    CommonModule,
    AgentErrorRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class AgentErrorModule { }
