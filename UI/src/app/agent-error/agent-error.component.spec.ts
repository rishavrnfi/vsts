import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentErrorComponent } from './agent-error.component';

describe('AgentErrorComponent', () => {
  let component: AgentErrorComponent;
  let fixture: ComponentFixture<AgentErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
