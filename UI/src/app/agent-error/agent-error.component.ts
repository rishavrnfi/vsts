import { Component, OnInit } from '@angular/core';
import { EkycService } from '../services/ekyc.service';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { PageEvent } from '@angular/material/paginator';
import { NgxUiLoaderService } from 'ngx-ui-loader';


@Component({
  selector: 'app-agent-error',
  templateUrl: './agent-error.component.html',
  styleUrls: ['./agent-error.component.css'],
  providers:[EkycService]
})
export class AgentErrorComponent implements OnInit {
  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  SearchFormData: FormGroup;
  loader:Boolean = false;
  result = [];
  length = 100;
  pageSize = 100;
  pageEvent: PageEvent;
  page:number=1;

  
  constructor(
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,   
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService
  ) { 
    this.user = this.jwtauth.isAuthenticatedjwttoken();
  }   

  ngOnInit(): void {
    this.SearchFormData = new FormGroup({
      uservalue:new FormControl("",Validators.compose([
        // Validators.required,
        // Validators.max(10),
        // Validators.min(10)
      ])),
      page:new FormControl("",Validators.compose([
      ]))
    });
    this.getAgentError();
  }

  getAgentError(){

    this.ngxloader.start();
    const formData = new FormData();
    formData.append('mobileNumber', this.SearchFormData.value.uservalue);
    formData.append('page', this.SearchFormData.value.page);

    this.ekycService.getAgentError(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ngxloader.stop();
          this.result = data['result']['data'];
          this.length = data['result']['total'];
          // this.openSnackBar(data['message'],'Done!!');
        }else{
          this.ngxloader.stop();
          // this.openSnackBar(data['message'],'Error!!');
        }
        
      },
      err => {        
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
  }

  pageevent(event){
    this.SearchFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.getAgentError();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }


}
