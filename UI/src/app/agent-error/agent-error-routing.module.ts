import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentErrorComponent } from './agent-error.component';

const routes: Routes = [{ path: '', component: AgentErrorComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentErrorRoutingModule { }
