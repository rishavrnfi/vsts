import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPartnerComponent } from './video-partner.component';

describe('VideoPartnerComponent', () => {
  let component: VideoPartnerComponent;
  let fixture: ComponentFixture<VideoPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
