import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EkycService } from '../services/ekyc.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-video-partner',
  templateUrl: './video-partner.component.html',
  styleUrls: ['./video-partner.component.css']
})
export class VideoPartnerComponent implements OnInit {

  mobileNumber:string ='';
  videoLoader:boolean = false;
  loader:boolean = false;
  response:string = '';
  webViewMessage:string = '';
  status: any;

  constructor(
    private route: ActivatedRoute,
    private ekycService:EkycService,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.mobileNumber = params['mobile'];
      this.response     = params['response'];
      this.status       = params['status'];
    });
    if(typeof this.response !== 'undefined' && this.response != '' && this.response != null ){
      localStorage.removeItem("Webmsg");
      let outerInstance = this;
      this.loader = true;
      setTimeout(function(){ outerInstance.getVideoVerification(); }, 15000);
    }else if(typeof this.status !== 'undefined' && this.status != '' && this.status != null) {
      this.webViewMessage = localStorage.getItem("Webmsg") !=='undefined' ? localStorage.getItem("Webmsg") : '';
    }
    else {
      localStorage.removeItem("Webmsg");
      this.videoLoader = true;
      this.getVideoUrl();
    }    
  }

  getVideoUrl(){
    const formData = new FormData();
    formData.append('mobile', this.mobileNumber);
    this.ekycService.getPartnerVideoKycUrl(formData).subscribe(
      data => {
        if(data['status']){
          window.location.replace(data['result']['videoUrl']);
        }else{
          //do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getVideoVerification() {
    this.ekycService.getPartnerVidVerificationResponse({mobile: this.mobileNumber}).subscribe(
      data => {  
        this.loader = false;
        
        if(data['status']) {
          localStorage.setItem("Webmsg", data['webViewMessage']); 
          if(typeof data['result'].videoVerification !=='undefined' && data['result'].videoVerification != null && data['result'].videoVerification){ 
            window.location.replace(environment.appBaseUrl+'/video-partner?status=success')
          }else{
            window.location.replace(environment.appBaseUrl+'/video-partner?status=failed')
          }         
        }
        else {
          localStorage.setItem("Webmsg", data['webViewMessage']);
          window.location.replace(environment.appBaseUrl+'/video-partner?status=failed')
        }
      },
      err => {        
        console.log(err);
        localStorage.setItem("Webmsg", "Something Went Wrong, please try again");
        window.location.replace(environment.appBaseUrl+'/video-partner?status=failed');
      },() =>{
        console.log('request completed!');
      }
    );
  }
}
