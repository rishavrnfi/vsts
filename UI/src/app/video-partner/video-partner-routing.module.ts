import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoPartnerComponent } from './video-partner.component';

const routes: Routes = [{ path: '', component: VideoPartnerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoPartnerRoutingModule { }
