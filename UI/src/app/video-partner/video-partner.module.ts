import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoPartnerRoutingModule } from './video-partner-routing.module';
import { VideoPartnerComponent } from './video-partner.component';


@NgModule({
  declarations: [VideoPartnerComponent],
  imports: [
    CommonModule,
    VideoPartnerRoutingModule
  ]
})
export class VideoPartnerModule { }
