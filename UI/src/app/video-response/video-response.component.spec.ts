import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoResponseComponent } from './video-response.component';

describe('VideoResponseComponent', () => {
  let component: VideoResponseComponent;
  let fixture: ComponentFixture<VideoResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
