import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators,FormGroupDirective, NgForm } from '@angular/forms';
import { EkycService } from '../services/ekyc.service';
import { FileValidator } from 'ngx-material-file-input';
import Swal from 'sweetalert2';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute, CanActivate } from '@angular/router'; 
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Select2OptionData } from 'ng-select2';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

export interface Distributor {
  dt_name: string;
  dt_username: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[EkycService]
})
export class DashboardComponent implements  OnInit {
  private _snackBar: MatSnackBar;
  stateCtrl = new FormControl();
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  //filteredDistributors: Observable<Distributor[]>;

  MobileFormGroup: FormGroup;
  PanFormGroup: FormGroup;
  DistributorFormGroup: FormGroup;
  KycModeFormGroup: FormGroup;

  //filteredDistributor: Observable<Distributor[]>;

  apiUrlVar:string='';
  kycData: any;
  /* Pan details  */
  pan:string='';
  panRemarks:any;
  panLoader:Boolean =false;
  /* End of pan details  */

  /* Distributor Mapping Form */
    distributors: Distributor[] = [];
    distributor: any;
  /* End of Distributor Mapping Form

 

  /* KYC mode */
  kyc_mode: string;
  kycModeRemarks:any;
  kyCRemarksArray:any;
  kycModeLoader: boolean=false;
  kycModeList: KycMode[] = [
    {value: 'Aadhar OTP', viewValue: 'Aadhar OTP'}, 
    {value: 'Non Aadhar', viewValue: 'Non Aadhar'},
  ];
  /* end of KYC mode */

  isEditable = true;
 
  loader:Boolean=false;
  mobileError='';

  mobile:string='';

  readonly maxSize = 104857600;
  stage=0;
  stages:any;

/*steper code */
  steperBlock:Boolean=false;
  tab1:Boolean=false;
  tab2:Boolean=false;
  tab3:Boolean=false;
  currentTab = 0;
  // showl:boolean=true;
/* End of Steper code*/
exampleData: Array<Select2OptionData> = []; 
from_bank : any;
  constructor(
    private ekycService:EkycService,
    private userService:UserService,
    private router: Router
    ) {
      
    }

    private _filterDistributors(value: string): Distributor[] {
      const filterValue = value.toLowerCase();
      return this.distributors.filter(dist => dist.dt_name.toLowerCase().includes(filterValue.toLowerCase()));
    }
  ngOnInit(): void {
    this.userService.validatePage();
    this.getDistributorList();

    this.apiUrlVar = environment.apiBaseUrl;
    
    this.MobileFormGroup = new FormGroup({
      mobile:new FormControl("",Validators.compose([
        Validators.required,
        Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"),
        Validators.minLength(10),
        Validators.maxLength(10)
      ])),
    });

    this.PanFormGroup = new FormGroup({
      pan : new FormControl("",Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ]))
    });

    this.DistributorFormGroup = new FormGroup({
      distributor : new FormControl("",Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ]))
    });


    this.KycModeFormGroup = new FormGroup({
      kyc_mode: new FormControl("",Validators.compose([
        Validators.required
      ])),
    });

    // this.filteredDistributors = this.stateCtrl.valueChanges
    // .pipe(
    //   startWith(''),
    //   map(dist => dist ? this._filterDistributors(dist) : this.distributors.slice())
    // );
  }

  addMobileDetail() {
    this.loader = true; 
    // this.showl = false;
    const formData = new FormData();
    formData.append('mobile', this.mobile);
    this.ekycService.mobileVerify(formData).subscribe(
      data => {
        if(data['status']) {
          this.kycData = data['result'];
          this.fillFormData(this.kycData);
          if(data['stages'] == ''){
            Swal.fire({
              icon: 'success',
              title: 'Onboarding',
              text: data['message']
            }).then((result) => {
              if (result.value) {
                this.loader=false;
                this.MobileFormGroup.reset();
            }});
          }else{
              this.steperBlock=true;
              this.loader=false;
              this.mobileError=''; 
              this.stages = data['stages'];
              this.stage = data['stages'][0];
              this.showSteps(this.stage);
          }
          
        }else{
          if(data['code']==203){
            Swal.fire({
              icon: 'error',
              title: 'Onboarding',
              text: data['message']
            }).then((result) => {
              if (result.value) {
                this.loader=false;
                if( typeof data['returnURL'] !== 'undefined'){
                  window.location.replace(data['returnURL']);
                }
                else {
                  this.MobileFormGroup.reset();
                }
            }});
          }
          else{
            this.mobileError = data['message'];
            this.loader=false;
            this.MobileFormGroup.reset();
          }
         
        }
      	
      },
      err => {        
        //this.isError = true;
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addPanDetails() {
    this.loader = true;
    const formData = new FormData();
    formData.append('mobile', this.mobile);
    formData.append('pan', this.PanFormGroup.value.pan);

    this.ekycService.addPanDetails(formData).subscribe(
      data => {
        if(data['status']) {

          Swal.fire({
            icon: 'success',
            title: 'Please confirm the below information of your pan.',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm',
            html: `<b>Name :</b>${data['result']['name']} <br>` +
                  `<b>Father name :</b>${data['result']['fatherName']} <br>`
          }).then((result) => {
            console.log(result)
            if (result.value) {
              // Swal.fire(
              //   'Confirmed!',
              //   'Your Pan verification confirmed.',
              //   'success'
              // )
              this.loader=false;
              // this.showSteps(2);
              this.nextStep();
          }
          else if ( result.dismiss === Swal.DismissReason.cancel ) {
            this.loader=false;
          }});

          // this.loader=false;
          // this.showSteps(2);
        }else{
          this.loader=false;
          if(Array.isArray(data['message'])){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message'].join(" <br/> ")
            });
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              html: data['message']
            });
          }
         
        }
      	
      },
      err => {        
         console.log(err);      
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addDistributorDetails(){
    this.loader = true;
    const formData = new FormData();
    formData.append('mobile', this.mobile);
    formData.append('distributor', this.DistributorFormGroup.value.distributor);

    this.ekycService.addDistributorDetails(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          // this.showSteps(3); 
          this.nextStep(); 
        }else{
          this.loader=false;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: data['message']
          });
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addKycMode() {
    this.loader = true;
    this.KycModeFormGroup.value.mobile = this.mobile;
    this.KycModeFormGroup.value.appname = 'Relipay';
    this.ekycService.addKycMode(this.KycModeFormGroup.value).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          Swal.fire({
            icon: 'success',
            title: 'Retailer will recive your form, to complete the pending verification processs',
            showConfirmButton: false,
            timer: 1500
          });
          this.mobile = '';
          this.MobileFormGroup.reset();
          this.steperBlock = false;
        }else{
          this.loader=false;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          })
        }
      	
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

 
  logMeOut(){
    sessionStorage.removeItem('session_token');
    this.router.navigateByUrl('login');
  }

  nextStep(){
    let index = (this.stages.indexOf(this.stage) + 1);
    this.stage = this.stages[index];
    this.showSteps(this.stage);
  }

  showSteps(n) {
   
    switch(n) { 
   
      case 1: { 
        this.tab1=true;
        this.tab2=false;
        this.tab3=false;
        break; 
      } 
      case 2: { 
        this.tab1=false;
        this.tab2=true;
        this.tab3=false;
        break; 
      } 
      case 3: { 
        this.tab1=false;
        this.tab2=false;
        this.tab3=true;
        break; 
      }
      default: { 
        // this.tab1=true;
        // this.tab2=false;
        // this.tab3=false;
        //  break; 
      } 
   }
    this.fixStepIndicator(n)
  }
  prevStep(n){
    if(this.stages.indexOf(this.stage)){
      let index = (this.stages.indexOf(this.stage) - 1);
      this.stage = this.stages[index];
      this.showSteps(this.stage);
    }
    else {
      this.steperBlock=false;
    }
    
  }

  fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
  }
  fillFormData(data){
    if(data.id){
      //this.kycFormId = data.id;
    }

    if(data.kycRemarks!=null){   //for each form
      this.kyCRemarksArray = data.kycRemarks;
    }
    /*Pan Details */
    if(data.pan_details!=null){
      this.pan = data.pan_details.panNumber;
      this.distributor = data.dist_username;
    }
    /*End of Pan Details */

     /* Final Confirmation */
     if(data.kyc_mode){
      this.kyc_mode   = data.kyc_mode;
     }
      
    /*End of Final Confirmation*/

    if(data.dist_username){
      this.distributor   = data.dist_username;
    }

    if(data.creater_type == "distributor"){
      this.distributor   = data.creater_username;
    }
  }
  
  getDistributorList(){
    const formData = new FormData();
    this.ekycService.getDistributorList(formData).subscribe(
      data => { 
        if(data['status'] == false){  
        }else{
        let  data1 = data['result']; 
        console.log(data1);
        
          let newArray: any = []; 
          for (let i in data1) {
          let objTitle1 = data1[i]['dt_username']; 
          let dt_name =data1[i]['dt_name']; 
          let final = dt_name+' '+objTitle1;
          newArray.push({ id: objTitle1, 'text': final });
        }
         this.exampleData = newArray; 
         console.log(this.exampleData);
         
        }
        
            
      }  
    );

  }

}

interface KycMode {
  value: string;
  viewValue: string;
}



