import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { AdminService } from '../services/admin.service';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { EkycService } from '../services/ekyc.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [
    EkycService
  ],
})
export class UsersComponent implements OnInit {

  SearchUserFormData: FormGroup;
  AddUserFormGroup: FormGroup;
  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  loader:Boolean=false;
  userList = [
    {value: '2', viewValue: 'Staff'},
    {value: '1', viewValue: 'Admin'},
  ];

  length   = 100;
  pageSize = 10;
  pageEvent: PageEvent;
  page:number;

  showlist:Boolean=false;
  userListData: any = [];
  dialogRef: any;
  usersData: any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private userService:UserService,
    private adminService:AdminService,
    private router: Router,
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private formBuilder: FormBuilder,
    private ekycService: EkycService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private ngxloader:NgxUiLoaderService
    ) {
      this.user       = this.jwtauth.isAuthenticatedjwttoken();
      this.apiBaseUrl = environment.apiBaseUrl;
      this.tokenvar   = this.userService.getToken();
     }


  ngOnInit(): void {

    this.AddUserFormGroup = new FormGroup({
      firstName:new FormControl("",Validators.compose([
        Validators.required
      ])),
      lastName:new FormControl("",Validators.compose([
        Validators.required
      ])),
      email:new FormControl("",Validators.compose([
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),
      ])),
      password:new FormControl("",Validators.compose([
        Validators.required
      ])),
      // permission:new FormControl("",Validators.compose([
      //   Validators.required
      // ])),
      userRole:new FormControl("",Validators.compose([
        Validators.required
      ]))
    });

    this.SearchUserFormData = this.formBuilder.group({
      email: [''],
      status:[''],
      page:['']
    });
  }

  pageevent(event){
    this.SearchUserFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.SearchUserData();
  }

  SearchUserData() {
    this.ngxloader.start();
    this.SearchUserFormData.value.token = this.userService.getToken();
    this.ekycService.SearchUserData(this.SearchUserFormData.value).subscribe(
      data => {

        if(data['status']){
         this.ngxloader.stop();
          this.showlist      = true;
          this.userListData  = data['result'].data;
          this.length        = data['result'].total;
        }else{

        } 
      },
      err => {
        console.log(err);
      }, () => {
        console.log('request completed!');
      }
    );
  }

  openAddUserDialog() {
    this.dialogRef = this.dialog.open(DialogDataAddUserDialog, {
      data: {}
    });
  }

  openDialog(userId) {
    this.getUserData(userId);
  }

  getUserData(userId){
    this.ekycService.getUserData(userId).subscribe(
      data => {
        if(data['status']) {
            this.usersData = data['result'];
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
      },() =>{ 
        this.dialogRef = this.dialog.open(DialogDataUserDialog, {
          data: {
            user:this.usersData
          }
        });

        this.dialogRef.afterClosed().subscribe(result => {
          this.SearchUserData();
          console.log('afterClosed event fired');
        });
        console.log('request completed!');
      }
    );
  }

  changeStatus(e:MatSlideToggleChange,userId){
    const formData = new FormData();
    formData.append('userId',userId);
    if(e.checked){
      formData.append('isBlocked','no');
    }else{
      formData.append('isBlocked','yes');
    }
    this.ekycService.changeUserStatus(formData).subscribe(
      data => {
        if(data['status']) {
          this.SearchUserData();
          this.openSnackBar(data['message'],'Done!!');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }


  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}

@Component({
  selector: 'dialog-data-user-dialog',
  templateUrl: 'dialog-data-user-dialog.html',
  styleUrls: ['./dialog-data-user-dialog.css'],
})
export class DialogDataUserDialog {

  UpdateUserFormGroup:FormGroup;
  userRole:string='';
  first_name:string='';
  last_name:string='';
  email:string='';
  userFormId:string='';
  status:any='';
  userRoleList:any;
  password:string='';
  assignStatus:any;
  showStatus:boolean = false;

  assignStatusList = [
    {value: "5", viewValue: 'Pending'},
    {value: "1", viewValue: 'Pre Approved'},
    {value: "2", viewValue: 'Approved'},
    {value: "3", viewValue: 'Rejected'},
  ];



  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isChecked:Boolean=false;
  option: any;
  options: any;

  constructor(
    public dialogRef: MatDialogRef<DialogDataUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: EkycService,
    private userService:UserService,
    private router: Router,
    private _snackBar: MatSnackBar,
    ) {}

    ngOnInit(): void {

      this.UpdateUserFormGroup = new FormGroup({
        first_name:new FormControl("",Validators.compose([
          Validators.required
        ])),
        last_name:new FormControl("",Validators.compose([
          Validators.required
        ])),
        email:new FormControl("",Validators.compose([
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),
        ])),
        password:new FormControl("",Validators.compose([
        ])),
        userRole:new FormControl("",Validators.compose([
          Validators.required
        ])),
        status:new FormControl("",Validators.compose([
          Validators.required
        ])),
        options:new FormControl("",Validators.compose([
          Validators.required
        ])),
        assignStatus:new FormControl("",Validators.compose([
         
        ]))
      });

      if(this.data.id!=''){
        this.userFormId = this.data.user.id;
        this.first_name = this.data.user.first_name;
        this.last_name  = this.data.user.last_name;
        this.email      = this.data.user.email;
        this.userRole   = this.data.user.role_id;
        this.option     = this.data.user.permission;
        
        if(this.data.user.role_id == 2) {
          this.showStatus = true;
          this.assignStatus = (this.data.user.assign_status).split(',');
          console.log(this.assignStatus)
        }
        if(this.data.user.isBlocked=='no'){
          this.isChecked = true;
        }
        this.getUserRole();
      } 
    }
    
    getUserRole(){
      this.ekycService.getUserRole().subscribe(
        data => {
          if(data['status']) {
            this.userRoleList = data['result'];
            console.log(data['result']);
          }else{
            
          }
        },
        err => {        
          console.log(err);
        },() =>{
          console.log('update user request completed.');
        }
      );
    }

    updateUser(){

      const formData = new FormData();
      formData.append('userFormId', this.userFormId);
      formData.append('first_name', this.first_name);
      formData.append('last_name', this.last_name);
      formData.append('email', this.email);
      formData.append('userRole', this.userRole);
      formData.append('password', this.password);
      formData.append('status', this.UpdateUserFormGroup.value.status);
      formData.append('assign_status', this.UpdateUserFormGroup.value.assignStatus);
      formData.append('permission', this.UpdateUserFormGroup.value.options);
      // formData.append('assign_status', this.assignStatus);

      this.ekycService.updateUser(formData).subscribe(
        data => {
          if(data['code'] == 200) {
            this.openSnackBar(data['message'],'Done!!');
            this.dialogRef.close();
          }else{
            this.openSnackBar(data['message'],'Error!!');
          }
        },
        err => {        
          console.log(err);
        },() =>{
          console.log('update user request completed.');
        }
      );

    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['mat-snack-bar-container'],
      });
    }

    selectRole($value){
      if($value == 2){
        this.showStatus = true;
        this.UpdateUserFormGroup.get("assignStatus").setValidators([Validators.required]);
      }
      else {
        this.showStatus = false;
        this.UpdateUserFormGroup.get("assignStatus").clearValidators();
      }
      this.UpdateUserFormGroup.get("assignStatus").updateValueAndValidity();
    }
   
}


@Component({
  selector: 'dialog-data-add-user-dialog',
  templateUrl: 'dialog-data-add-user-dialog.html',
  styleUrls: ['./dialog-data-add-user-dialog.css'],
})
export class DialogDataAddUserDialog {

  AddUserFormGroup:FormGroup;
  userRole:string='';
  first_name:string='';
  last_name:string='';
  email:string='';
  userFormId:string='';
  status:any='';
  userRoleList:any;
  password:string='';
  assignStatus:string='';
  showStatus:boolean = false;

  assignStatusList = [
    {value: 5, viewValue: 'Pending'},
    {value: 1, viewValue: 'Pre Approved'},
    {value: 2, viewValue: 'Approved'},
    {value: 3, viewValue: 'Rejected'},
  ];

  favoriteSeason: string;
  seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isChecked:Boolean=true;
  favoriteCartoon: string;
  cartoons: string[] = ['Tom and Jerry', 'Rick and Morty', 'Ben 10', 'Batman: The Animated Series'];

  constructor(
    public dialogRef: MatDialogRef<DialogDataAddUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: EkycService,
    private userService:UserService,
    private router: Router,
    private _snackBar: MatSnackBar,
    ) {}

    ngOnInit(): void {

      this.AddUserFormGroup = new FormGroup({
        first_name:new FormControl("",Validators.compose([
          Validators.required
        ])),
        last_name:new FormControl("",Validators.compose([
          Validators.required
        ])),
        email:new FormControl("",Validators.compose([
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),
        ])),
        password:new FormControl("",Validators.compose([
          Validators.required
        ])),
        userRole:new FormControl("",Validators.compose([
          Validators.required
        ])),
        status:new FormControl("",Validators.compose([
          Validators.required
        ])),
        options:new FormControl("",Validators.compose([
          Validators.required
        ])),
        assignStatus:new FormControl("",Validators.compose([
          // Validators.required
        ]))
      });

      this.getUserRole();
    }

    getUserRole(){
      this.ekycService.getUserRole().subscribe(
        data => {
          if(data['status']) {
            this.userRoleList = data['result'];
            console.log(data['result']);
          }else{
            
          }
        },
        err => {        
          console.log(err);
        },() =>{
          console.log('update user request completed.');
        }
      );
    }

    addUser(){
      const formData = new FormData();
      formData.append('first_name', this.AddUserFormGroup.value.first_name);
      formData.append('last_name', this.AddUserFormGroup.value.last_name);
      formData.append('email', this.AddUserFormGroup.value.email);
      formData.append('userRole', this.AddUserFormGroup.value.userRole);
      formData.append('password', this.AddUserFormGroup.value.password);
      formData.append('status', this.AddUserFormGroup.value.status);
      formData.append('assign_status', this.AddUserFormGroup.value.assignStatus);
      formData.append('permission', this.AddUserFormGroup.value.options);

      this.ekycService.addUser(formData).subscribe(
        data => {
          if(data['status']) {
            this.openSnackBar(data['message'],'Done!!');
            this.dialogRef.close();
          }else{
            this.openSnackBar(data['message'],'Error!!');
          }
        },
        err => {        
          console.log(err);
        },() =>{
          console.log('update user request completed.');
        }
      );

    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 2000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        panelClass: ['mat-snack-bar-container'],
      });
    }

    selectRole($value){
      if($value == 2){
        this.showStatus = true;
        this.AddUserFormGroup.get("assignStatus").setValidators([Validators.required]);
      }
      else {
        this.showStatus = false;
        this.AddUserFormGroup.get("assignStatus").clearValidators();
      }
      this.AddUserFormGroup.get("assignStatus").updateValueAndValidity();
    }
   
}

