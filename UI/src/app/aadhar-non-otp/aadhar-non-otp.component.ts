import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AadhaarService } from '../services/aadhar-validate.service';

@Component({
  selector: 'app-aadhar-non-otp',
  templateUrl: './aadhar-non-otp.component.html',
  styleUrls: ['./aadhar-non-otp.component.css'],
})
export class AadharNonOtpComponent implements OnInit {
  aadharsecond: any = '';
  aadharfirst: any = '';
  msg: any;
  completeAdhar: any;
  aadharForm: FormGroup;
  submitted: boolean = false;
  aadharotploader: Boolean = false;
  agent_token: any;
  constructor(
    private fb: FormBuilder,
    private aadharservice: AadhaarService,
    private route: ActivatedRoute
  ) {
    this.aadharForm = this.fb.group({
      secondnum1: ['', [Validators.required]],
      firstnum1: ['', [Validators.required]],
      thirdnum1: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.agent_token = params['agent_token'];
      sessionStorage.setItem('agent_token', this.agent_token);
    });

    this.getAadharNONOtpCredentials(sessionStorage.getItem('agent_token'));

    var response = '';
    this.aadharservice.getvalue().subscribe((res: any) => {
      if (res.firstnumber) {
        this.aadharfirst = res.firstnumber;
      }
      if (res.secondnumber) {
        this.aadharsecond = response.concat(res.secondnumber);
      }
    });
  }
  get f() {
    return this.aadharForm.controls;
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  getAadharNONOtpCredentials(token: any) {
    console.log(token);
  }

  onSubmit() {
    this.submitted = true;
    if (this.aadharForm.invalid) {
      return;
    }
    this.completeAdhar = this.aadharfirst.concat(
      this.aadharsecond,
      this.aadharForm.controls.thirdnum1.value
    );

    if (this.aadharservice.isValidUidaiNumber(this.completeAdhar)) {
      console.log(this.completeAdhar);
    } else {
      console.log(this.completeAdhar);
      this.msg = 'Aadhar Number is Invalid';
    }
  }
}
