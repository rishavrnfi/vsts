import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AadharNonOtpComponent } from './aadhar-non-otp.component';

const routes: Routes = [{
  path: '',
  component: AadharNonOtpComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AadharNonOtpRoutingModule { }
