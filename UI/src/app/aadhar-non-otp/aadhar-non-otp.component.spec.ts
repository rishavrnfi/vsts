import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AadharNonOtpComponent } from './aadhar-non-otp.component';

describe('AadharNonOtpComponent', () => {
  let component: AadharNonOtpComponent;
  let fixture: ComponentFixture<AadharNonOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AadharNonOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AadharNonOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
