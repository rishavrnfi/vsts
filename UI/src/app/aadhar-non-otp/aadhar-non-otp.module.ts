import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AadharNonOtpRoutingModule } from './aadhar-non-otp-routing.module';
import { AadharNonOtpComponent } from './aadhar-non-otp.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
// import { MaskDirective } from '../directive/mask.directive';
@NgModule({
  declarations: [AadharNonOtpComponent],
  imports: [
    CommonModule,
    AadharNonOtpRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AadharNonOtpModule { }
