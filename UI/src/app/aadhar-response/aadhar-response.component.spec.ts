import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AadharResponseComponent } from './aadhar-response.component';

describe('AadharResponseComponent', () => {
  let component: AadharResponseComponent;
  let fixture: ComponentFixture<AadharResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AadharResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AadharResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
