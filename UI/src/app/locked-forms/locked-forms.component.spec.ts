import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LockedFormsComponent } from './locked-forms.component';

describe('LockedFormsComponent', () => {
  let component: LockedFormsComponent;
  let fixture: ComponentFixture<LockedFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LockedFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockedFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
