import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LockedFormsRoutingModule } from './locked-forms-routing.module';
import { LockedFormsComponent } from './locked-forms.component';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio'
import { MatCheckboxModule } from '@angular/material/checkbox';



@NgModule({
  declarations: [LockedFormsComponent],
  imports: [
    CommonModule,
    LockedFormsRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule
  ]
})
export class LockedFormsModule { }