import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LockedFormsComponent } from './locked-forms.component';

const routes: Routes = [{ path: '', component: LockedFormsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LockedFormsRoutingModule { }
