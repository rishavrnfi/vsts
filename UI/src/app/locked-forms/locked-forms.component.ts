import { Component, OnInit } from '@angular/core';
import { EkycService } from '../services/ekyc.service';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { PageEvent } from '@angular/material/paginator';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-locked-forms',
  templateUrl: './locked-forms.component.html',
  styleUrls: ['./locked-forms.component.css']
})
export class LockedFormsComponent implements OnInit {

  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  SearchFormData: FormGroup;
  loader:Boolean = false;
  result = [];
  length = 100;
  pageSize = 100;
  pageEvent: PageEvent;
  page:number=1;

  
  constructor(
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,   
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
  ) { 
    this.user = this.jwtauth.isAuthenticatedjwttoken();
  }  

  ngOnInit(): void {

    this.SearchFormData = new FormGroup({
      uservalue:new FormControl("",Validators.compose([
        // Validators.required,
        // Validators.max(10),
        // Validators.min(10)
      ])),
      page:new FormControl("",Validators.compose([
      ]))
    });

  }

  getLockedData(){
    
    this.loader = true;
    const formData = new FormData();
    formData.append('mobileNumber', this.SearchFormData.value.uservalue);
    formData.append('page', this.SearchFormData.value.page);

    this.ekycService.getLockedData(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.loader = false;
          this.result = data['result']['data'];
          this.length = data['result']['total'];
          // this.openSnackBar(data['message'],'Done!!');
        }else{
          this.loader = false;
          // this.openSnackBar(data['message'],'Error!!');
        }
        
      },
      err => {        
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );

  }

  unlockData(event,id){
    if(event.checked){

      Swal.fire({
        title: 'Are you sure?',
        text: "Want to unlock!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          const formData = new FormData();
          formData.append('mobileNumber', this.SearchFormData.value.uservalue);
          this.ekycService.getLockedData(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader = false;
                this.result = data['result']['data'];
                this.length = data['result']['total'];
                // this.openSnackBar(data['message'],'Done!!');
              }else{
                this.loader = false;
                // this.openSnackBar(data['message'],'Error!!');
              }
              
            },
            err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            },() =>{
              console.log('request completed!');
            }
          );
        }
      })
      
    }
    console.log(id)
  }

  pageevent(event){
    this.SearchFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.getLockedData();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}
