import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewDataRoutingModule } from './view-data-routing.module';
import { ViewDataComponent } from './view-data.component';
import { ShowDataComponent } from './show-data/show-data.component';
import {NgxUiLoaderModule } from 'ngx-ui-loader';
@NgModule({
  declarations: [ViewDataComponent, ShowDataComponent],
  imports: [
    CommonModule,
    ViewDataRoutingModule,
    NgxUiLoaderModule
  ]
})
export class ViewDataModule { }
