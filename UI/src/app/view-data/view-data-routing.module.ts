import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowDataComponent } from './show-data/show-data.component';

import { ViewDataComponent } from './view-data.component';

const routes: Routes = [{ path: '', component: ViewDataComponent }, ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewDataRoutingModule { }
