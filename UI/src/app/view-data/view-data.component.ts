import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EkycService } from '../services/ekyc.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { FormGroup } from '@angular/forms';
import { AuthjwtService } from '../services/authjwt.service';
import { NgStyle } from '@angular/common';
import { UserService } from '../services/user.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-view-data',
  templateUrl: './view-data.component.html',
  styleUrls: ['./view-data.component.css']
})
export class ViewDataComponent implements OnInit {
  mobile: any;
  loader : boolean=false;
  kycList: any;
  data: any;


  MobileFormGroup: FormGroup;
  PanFormGroup: FormGroup;
  BasicDetailsFormGroup: FormGroup;
  ResidentialAddressFormGroup: FormGroup;
  BusinessinformationFormGroup: FormGroup;
  BankVerificationFormGroup: FormGroup;
  ResidentialProofFormGroup: FormGroup;
  KycModeFormGroup: FormGroup;

  apiUrlVar:string='';
  token:string='';

  mobileError = '';
  kycFormId:string='';
  mobileNumber:string='';
 
  /* Pan details  */
  pan:string='';
  email:string='';
  applicant:string;
  panRemarks:any;
  panLoader:Boolean =false;
  panFile:string='';
  panThumbFile:string='';
  selectedPanFile:File = null;
  selectedPPFile:File = null;
  panRemarksArray = [];
  /* End of pan details  */

  /* Basic details  */
  minDate: Date;
  maxDate: Date;
  name:string='';
  dob: any;
  parentName: any;
  gender: any;
  qualification: any;
  basicDetailsRemarks:any;
  isBankVerification = true;
  basicLoader: boolean=false;
  qualificationList:any =[];
  genders = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
    {value: 'Other', viewValue: 'Other'}
  ];

  BasicDetailsRemarkArray:Array<Remark>=[];

  /* End of Basic details  */

  /* Resendial Address */
  address: any;
  landmark: any;
  pincode: any;
  district: any;
  city: any;
  state: any;
  addressRemarks:any;
  resiLoader: boolean=false;
  ResidentialDetailsRemarkArray:Array<Remark>=[];
  /* End of Resendial Address */

  /* Business Information */
  firmName: any;
  workExp: any;
  firm_address: any;
  firm_landmark: any;
  firm_pincode: any;
  firm_district: any;
  firm_city: any;
  firm_state: any;
  works: any=[];
  businessRemarks:any;
  BusinessLoader: boolean=false;
  isResAddr = false;
  BusinesssRemarkArray:Array<Remark>=[];
   /* End of Business Information */

  /*Bank Details */
  accountType: any;
  bankName: any;
  ifsc: any;
  account: any;
  bankVerificationRemarks:any;
  accountTypes: any=[];
  bankList: any = [];
  BankLoader: boolean=false;
  bankFile:string='';
  selectedBankFile:File = null;
  BankRemarkArray:Array<Remark>=[];
  /* End of Bank Details*/ 

  VideoRemarkArray:Array<Remark>=[];
  secAddressProofRemarkArray:Array<Remark>=[];

  /* Address proof */

  proof_type: string='';
  addressProofRemarks:any;
  ProofLoader: boolean=false;
  AddressProofRemarkArray:Array<Remark>=[];

  proofPhotoFile:string='';
  proofThumbPhotoFile:string='';
  proofPhotoBackFile:string='';
  proofThumbPhotoBackFile:string='';

  addressProofList = [
    {value: 'Voter ID', viewValue: 'Voter ID'},
    {value: 'Driving Licence', viewValue: 'Driving Licence'},
    {value: 'Passport', viewValue: 'Passport'},
    {value: 'Aadhar', viewValue: 'Aadhar'},
    
  ];
  /* End Address proof */
 
  /* KYC mode */
  kyc_mode: string;
  kycModeRemarks:any;
  kyCRemarksArray:any;
  kycModeLoader: boolean=false;
  kycModeList = [
    {value: 'Aadhar OTP', viewValue: 'Aadhar OTP(Free)'},
    {value: 'Biometric', viewValue: 'Biometric(Free)'},
    {value: 'Non Aadhar', viewValue: 'Non Aadhar(Rs 100)'},
  ];
  /* end of KYC mode */


  events: Array<string> = [];
  panExtractionResponseData: Array<string> = [];
  panFetchResponseData: any = {};
  addressProofResponseData: Array<string> = [];
  BankVerificationResponseData: Array<string> = [];
  VideoVerificationResponseData: any;

  // panalbum: Array<Album> = [];

  remarksDataArray:any;
  panRemarksDataArray:any;
  bankRemarksDataArray:any
  aadharRemarksDataArray:any
  addressRemarksDataArray:any
  videoRemarksDataArray:any;

  VideoVerificationFormGroup: FormGroup;
  ForgeryResponseData: any;
  ImageQualityResponseData: any;
  NameMatchResponseData: any;
  category: any;
  physicallyHandicapped: any;
  alternateOccupationType: any;
  provider: any;
  entityType: any;
  alternateNumber: any;
  BiometricResponseData: any;
  kycStatus: any;
  aadhar_otp_response:any;
  pic:any;
  categoryList = [
    {value: 'General', viewValue: 'General'},
    {value: 'OBC', viewValue: 'OBC'},
    {value: 'SC', viewValue: 'SC'},
    {value: 'ST', viewValue: 'ST'}
  ];

  physicallyhandicappedList = [
    {value: 'Handicaped', viewValue: 'Handicaped'},
    {value: 'Not Handicapped', viewValue: 'Not Handicapped'}
  ];
  
  alternateoccupationtypeList = [
    {value: 'Public Sector', viewValue: 'Public Sector'},
    {value: 'Self Employed', viewValue: 'Self Employed'},
    {value: 'Private', viewValue: 'Private'},
    {value: 'Others', viewValue: 'Others'},
    {value: 'None', viewValue: 'None'}
  ];

  providerList = [
    {value: 'Airtel', viewValue: 'Airtel'},
    {value: 'IDEA', viewValue: 'IDEA'},
    {value: 'VODA', viewValue: 'VODA'},
    {value: 'JIO', viewValue: 'JIO'}
  ];

  entitytypeList = [
    {value: 'Individual', viewValue: 'Individual'},
    {value: 'Sole Proprietorship', viewValue: 'Sole Proprietorship'},
    {value: 'Partnership Firm', viewValue: 'Partnership Firm'}
  ];
  secondaryProff: any;
  readonly maxSize = 104857600;
  bussinessRemarksDataArray: any;
  user:any;
 

  constructor(
    private route: ActivatedRoute,
    private ekycService: EkycService,
    public jwtauth: AuthjwtService,
    private userService:UserService,
    private router: Router,
    private ngxloader:NgxUiLoaderService
  ) {
    this.user = this.jwtauth.isAuthenticatedjwttoken();
  }

  ngOnInit(): void {

    this.apiUrlVar = environment.apiBaseUrl;
    // this.token = this.userService.getToken();
    this.route.queryParams.subscribe((params) => {
      this.kycFormId = params['data'];
    });
    this.SearchKycData();
  //  console.log(this.kycFormId);
    console.log( this.user);
    
  }
  
  
  SearchKycData() {
    this.ngxloader.start();
this.loader=true;
    this.ekycService.getKycDataView(this.kycFormId).subscribe(
      response => {
        this.ngxloader.stop();
        this.loader=false;
        this.data = response['result'];
        this.setData()
        console.log(this.data)
      },
      err => {
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      }, () => {
        console.log('request completed!');
      }
    );
  }
  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  setData(){
    if(this.data.aadhar_otp_response!=null){
      this.aadhar_otp_response = this.data.aadhar_otp_response.response.result;
      console.log(this.aadhar_otp_response);
      
    }
     /*Pan Details */
     if(this.data.pan_details!=null){
      this.pan = this.data.pan_details.panNumber;
      this.applicant = this.data.pan_details.applicantName;
      this.panFile   = this.data.pan_details.panPhoto;
      this.panThumbFile   = this.data.pan_details.panThumbPhoto;
      this.mobileNumber = this.data.mobileNumber;
    } 

    if(this.data.basic_details !=null){
      this.dob = this.data.basic_details.dob;
      this.name = this.data.basic_details.name;
      this.email = this.data.basic_details.email;
      //this.distributor = this.data.kyc.distributorId;
      this.parentName = this.data.basic_details.fatherName;
      this.gender = this.data.basic_details.gender;
      this.qualification = this.data.basic_details.qualification;
      this.category = this.data.basic_details.category;
      this.physicallyHandicapped = this.data.basic_details.physicallyHandicapped;
      this.alternateOccupationType = this.data.basic_details.alternateOccupationType;
      this.provider = this.data.basic_details.provider;
      this.entityType = this.data.basic_details.entityType;
      this.alternateNumber = this.data.basic_details.alternateNumber;
    }
    /*End of Basic Details*/

    /*Residentail Address */
    if(this.data.residential_details!=null){
      this.address  = this.data.residential_details.address;
      this.landmark = this.data.residential_details.landmark;
      this.pincode  = this.data.residential_details.pincode;
      this.district = this.data.residential_details.district;
      this.city     = this.data.residential_details.city;
      this.state    = this.data.residential_details.state;
    }

    /*End of Residentail Address*/

    /*Business Information */
  if(this.data.business_details!=null){
    this.firmName       = this.data.business_details.firmName;
    this.workExp        = this.data.business_details.workExperiance;
    this.firm_address   = this.data.business_details.businessAddress;
    this.firm_landmark  = this.data.business_details.businessLandmark;
    this.firm_pincode   = this.data.business_details.businessPincode;
    this.firm_district  = this.data.business_details.businessDistrict;
    this.firm_city      = this.data.business_details.businessCity;
    this.firm_state     = this.data.business_details.businessState;
  }

    /*End of Business Information*/

    /*Bank Details */

    if(this.data.bank_details!=null){
      this.accountType = this.data.bank_details.bankAccountType;
      this.bankName    = this.data.bank_details.bankName;
      this.ifsc        = this.data.bank_details.ifscCode;
      this.account     = this.data.bank_details.accountNumber;
      this.bankFile    = this.data.bank_details.passbookPhoto;
    }

    /*End of Bank Details*/
    console.log(this.data.kyc);
 
 
   
    
    /* Address Proof */

    if(this.data.residential_proof!=null){
      this.proof_type   = this.data.residential_proof.kyc_type;
      this.proofPhotoFile=this.data.residential_proof.proofPhoto;
      this.proofThumbPhotoFile=this.data.residential_proof.proofThumbPhoto;
      this.proofPhotoBackFile=this.data.residential_proof.proofPhotoBack;
      this.proofThumbPhotoBackFile = this.data.residential_proof.proofThumbPhotoBack;
    }

    /*End of Address Proof*/

     /* Final Confirmation */
    if(this.data.kyc_mode){
      this.kyc_mode   = this.data.kyc_mode;
      if(this.kyc_mode=='Non Aadhar'){
        this.addressProofList=[
          {value: 'Voter ID', viewValue: 'Voter ID'},
          {value: 'Driving Licence', viewValue: 'Driving Licence'},
          {value: 'Passport', viewValue: 'Passport'},
          
        ];
      }else if(this.kyc_mode=='Aadhar OTP'){
      }
    }


    if(this.data.kyc_remarks!=null){   //for each form
      this.kyCRemarksArray = this.data.kyc_remarks;
      this.kyCRemarksArray.forEach(function (value) {
        switch(value.type){
          case 4:
            const remark4 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.panRemarksArray.push(remark4);
          break;

          case 5:
            const remark5 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.AddressProofRemarkArray.push(remark5);
          break;

          case 6:
            const remark6 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.BasicDetailsRemarkArray.push(remark6);
          break;

          case 7:
            const remark7 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.ResidentialDetailsRemarkArray.push(remark7);
          break;

          case 8:
            const remark8 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.BusinesssRemarkArray.push(remark8);
          break;

          case 9:
            const remark9 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.BankRemarkArray.push(remark9);
          break;

          case 10:
            const remark10 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.VideoRemarkArray.push(remark10);
          break;
          case 13:
            const remark13 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name,
              created_at:value.created_at
            };
            this.secAddressProofRemarkArray.push(remark13);
          break;
        }
    }.bind(this));
    }

    // if(this.data.kyc.kycStatus !=null && this.data.kyc.kycStatus !=''){
    //   this.kycStatus = this.data.kyc.kycStatus;
    //   console.log(this.kycStatus);
    // }
    this.addressProofResponse();
    this.panFetchResponse();
    this.panFetchResponse();
    this.panExtractionResponse();
    this.getBankVerificationResponse();
    this.videoVerificationResponse();
    this.forgeryResponse();
    this.getBiometricResponse();
    this.getNameMatchResponse();
    this.getImageQualityResponse();
    this.getSecondaryProof();
  }

  addressProofResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('address_proof_type',this.data.residential_proof.kyc_type);
    this.ekycService.addressProofResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.addressProofResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  panFetchResponse(){

    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.panFetchResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.panFetchResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  panExtractionResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.panExtractionResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.panExtractionResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }


  getBankVerificationResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getBankVerificationResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.BankVerificationResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  videoVerificationResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.getVideoVerificationResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          if(data['result'] != null){ 
            this.VideoVerificationResponseData = data['result'];
            console.log(this.VideoVerificationResponseData['finalMatchImage2']);
          }
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  forgeryResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getForgeryResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ForgeryResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getImageQualityResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getImageQualityResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ImageQualityResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getNameMatchResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getNameMatchResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.NameMatchResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getBiometricResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getBiometricResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.BiometricResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }


  getSecondaryProof(){

    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.getSecondaryProof(formData).subscribe(
      data => {
        if(data['status']) {
          this.secondaryProff = data['result'];
          console.log(this.secondaryProff);
        }else{
          //do something
        }
      },
      err => {        
         console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
 

  
}
  
interface Remark {
  type: string;
  remark: string;
}