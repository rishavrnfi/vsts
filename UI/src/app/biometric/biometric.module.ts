import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';

import { BiometricRoutingModule } from './biometric-routing.module';
import { BiometricComponent } from './biometric.component';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [BiometricComponent],
  imports: [
    CommonModule,
    BiometricRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule
  ]
})
export class BiometricModule { }
