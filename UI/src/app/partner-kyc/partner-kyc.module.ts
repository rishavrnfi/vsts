import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnerKycRoutingModule } from './partner-kyc-routing.module';
import { PartnerKycComponent,DialogDataKycDialog } from './partner-kyc.component';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatRadioModule } from '@angular/material/radio';
import { NgxEditorModule } from 'ngx-editor';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { LightboxModule } from 'ngx-lightbox';
import {NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  declarations: [PartnerKycComponent,DialogDataKycDialog],
  imports: [
    NgxUiLoaderModule,
    CommonModule,
    PartnerKycRoutingModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule,
    MatPaginatorModule,
    MatDialogModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MaterialFileInputModule,
    MatRadioModule,
    NgxEditorModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    LightboxModule
  ]
})
export class PartnerKycModule { }
