import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnerKycComponent } from './partner-kyc.component';

const routes: Routes = [{ path: '', component: PartnerKycComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerKycRoutingModule { }
