import { Component, OnInit, Inject, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';

import { AuthjwtService } from '../services/authjwt.service';
import { UserService } from '../services/user.service';
import { PartnerEkycService } from '../services/partner.ekyc.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';
import { Lightbox } from 'ngx-lightbox';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-partner-kyc',
  templateUrl: './partner-kyc.component.html',
  styleUrls: ['./partner-kyc.component.css'],
  providers: [
    PartnerEkycService
  ],
})
export class PartnerKycComponent implements OnInit {

  SearchFormData: FormGroup;
  showlist:Boolean=false;
  kycList: any = [];
  loader:Boolean=false;

  length   = 100;
  pageSize = 10;
  pageEvent: PageEvent;
  page:number;
  kycData:any;
  dialogRef:any;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  user: any;
  
  constructor(
    private ekycService: PartnerEkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,
    public jwtauth: AuthjwtService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private ngxloader:NgxUiLoaderService
    ) { 
      this.user = this.jwtauth.isAuthenticatedjwttoken();
     }

  ngOnInit(): void {
    //this.userService.validatePage();
    this.SearchFormData = this.formBuilder.group({
      uservalue: [''],
      status:[''],
      page:['']
    });
  }

  SearchKycData() {
    this.ngxloader.start();
    this.SearchFormData.value.token = this.userService.getToken();
    this.ekycService.SearchKycData(this.SearchFormData.value).subscribe(
      response => {
        this.ngxloader.stop();
        this.showlist = true;
        this.kycList = response['result'].data;
        this.length  = response['result'].total;
      },
      err => {
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      }, () => {
        console.log('request completed!');
      }
    );
  }

  pageevent(event){
    this.SearchFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.SearchKycData();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openDialog(kycId) {
    this.getKycData(kycId);
  }

  getKycData(kycFormId){
    this.ekycService.getKycData(kycFormId).subscribe(
      data => {
        if(data['code'] == 200) {
            this.kycData = data['result'];
        }else{
          // do something
        }
      },
      err => {        
         console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        if(this.kycData.islocked == "Yes"){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Form is locked by some user.'
          })
          
        }else {
          this.dialogRef = this.dialog.open(DialogDataKycDialog, {
            disableClose:true,
            maxWidth: '100vw',
            width: '90%',
            data: {
              kyc:this.kycData
            }
          });

          this.dialogRef.afterClosed().subscribe(result => {
            console.log('afterClosed event fired');
            this.unlockForm(this.kycData.id)
          });
        }
        
        console.log('request completed!');
      }
    );
  }

  unlockForm(kycFormId){
    const formData = new FormData();
    formData.append('kycFormId', kycFormId);
    formData.append('isLocked', "No");
    
    this.ekycService.unlockForm(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          //this.loader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          //this.loader=false;
          // this.works = data['result'];
        }
      },
      err => {        
       	console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }

}


@Component({
  selector: 'dialog-data-kyc-dialog',
  templateUrl: 'dialog-data-kyc-dialog.html',
  styleUrls: ['./dialog-data-kyc-dialog.css'],
})
export class DialogDataKycDialog {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  MobileFormGroup: FormGroup;
  PanFormGroup: FormGroup;
  ResidentialProofFormGroup: FormGroup;

  apiUrlVar:string='';
  token:string='';

  loader:Boolean=false;
  mobileError = '';
  kycFormId:string='';
 
  /* Pan details  */
  pan:string='';
  email:string='';
  applicant:string;
  panRemarks:any;
  panLoader:Boolean =false;
  panFile:string='';
  ppFile:string='';
  selectedPanFile:File = null;
  selectedPPFile:File = null;
  panRemarksArray:Array<Remark> = [];
  /* End of pan details  */


  VideoRemarkArray:Array<Remark>=[];

  /* Address proof */

  proof_type: string='';
  addressProofRemarks:any;
  ProofLoader: boolean=false;
  AddressProofRemarkArray:Array<Remark>=[];

  proofPhotoFile:string='';
  proofThumbPhotoFile:string='';
  proofPhotoBackFile:string='';
  proofThumbPhotoBackFile:string='';

  addressProofList: AddressProof[] = [
    {value: 'Voter ID', viewValue: 'Voter ID'},
    {value: 'Driving Licence', viewValue: 'Driving Licence'},
    {value: 'Passport', viewValue: 'Passport'},
    {value: 'Aadhar', viewValue: 'Aadhar'},
    
  ];
  /* End Address proof */
 
  kyCRemarksArray:any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  events: Array<string> = [];
  panExtractionResponseData: Array<string> = [];
  panFetchResponseData: any = {};
  addressProofResponseData: Array<string> = [];
  VideoVerificationResponseData: any;

  panalbum: Array<Album> = [];
  remarksDataArray:any;
  VideoVerificationFormGroup: FormGroup;
  ForgeryResponseData: any;
  ImageQualityResponseData: any;
  NameMatchResponseData: any;
 
  constructor(
    public dialogRef: MatDialogRef<DialogDataKycDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ekycService: PartnerEkycService,
    private userService:UserService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private _lightbox: Lightbox
    ) {}
   
  ngOnInit(): void {
    this.getRemarks('rejection');
    this.apiUrlVar = environment.apiBaseUrl;
    this.token = this.userService.getToken();

    if(this.data.kyc.id){
      this.kycFormId = this.data.kyc.id;
    }

    if(this.data.kyc.kyc_remarks!=null){   //for each form
      this.kyCRemarksArray = this.data.kyc.kyc_remarks;
      this.kyCRemarksArray.forEach(function (value) {
        switch(value.type){
          case 4:
            const remark4 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.panRemarksArray.push(remark4);
          break;

          case 5:
            const remark5 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.AddressProofRemarkArray.push(remark5);
          break;

          case 6:
            const remark6 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.BasicDetailsRemarkArray.push(remark6);
          break;

          case 7:
            const remark7 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.ResidentialDetailsRemarkArray.push(remark7);
          break;

          case 8:
            const remark8 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.BusinesssRemarkArray.push(remark8);
          break;

          case 9:
            const remark9 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.BankRemarkArray.push(remark9);
          break;

          case 10:
            const remark10 = {
              type: value.type,
              remark: value.remarks,
              name:value.first_name
            };
            this.VideoRemarkArray.push(remark10);
          break;
        }
    }.bind(this));
    }
    /*Pan Details */
    if(this.data.kyc.partner_pan_details!=null){
      this.pan = this.data.kyc.partner_pan_details.panNumber;
      this.panFile   = this.data.kyc.partner_pan_details.panPhoto;
      for (let i = 1; i <= 1; i++) {
        const src = this.apiUrlVar+'/getPhoto?filename='+this.panFile+'&type=pandoc&token='+this.userService.getToken();
        const caption = 'Pan Details';
        const thumb = this.apiUrlVar+'/getPhoto?filename='+this.panFile+'&type=pandocThumb&token='+this.userService.getToken();
        const album = {
            src: src,
            caption: caption,
            thumb: thumb
        };
        this.panalbum.push(album);
      } 

      
  }

    /* Address Proof */

    if(this.data.kyc.partner_residential_proof!=null){
      this.proof_type   = this.data.kyc.partner_residential_proof.kyc_type;
      this.proofPhotoFile=this.data.kyc.partner_residential_proof.proofPhoto;
      this.proofThumbPhotoFile=this.data.kyc.partner_residential_proof.proofThumbPhoto;
      this.proofPhotoBackFile=this.data.kyc.partner_residential_proof.proofPhotoBack;
      this.proofThumbPhotoBackFile = this.data.kyc.partner_residential_proof.proofThumbPhotoBack;
    }

    /*End of Address Proof*/

  this.PanFormGroup = new FormGroup({
    panRemarks:new FormControl("",Validators.compose([])) 
  });

  this.ResidentialProofFormGroup = new FormGroup({
    addressProofRemarks: new FormControl("",Validators.compose([])), 
  });
  
  this.VideoVerificationFormGroup = new FormGroup({
    VideoVerificationRemarks: new FormControl("",Validators.compose([])),
  });

  this.panExtractionResponse();
  this.videoVerificationResponse();
  this.forgeryResponse();
  this.addressProofResponse();
  this.getImageQualityResponse();
  
  
}

panopen(index: number): void {
  // open lightbox
  this._lightbox.open(this.panalbum, index, {fitImageInViewPort:true,wrapAround: true,
    positionFromTop:50,showZoom:true,
    showRotate:true,centerVertically:true});
}

close(): void {
  // close lightbox programmatically
  this._lightbox.close();
}

updatePanDetails() {
  this.panLoader = true;
  const formData = new FormData();
  formData.append('kycFormId', this.kycFormId);
  formData.append('panRemarks', this.PanFormGroup.value.panRemarks);

  this.ekycService.updatePanDetails(formData).subscribe(
    data => {
      if(data['code'] == 200) {
        this.panLoader=false;
        this.openSnackBar(data['message'],'Done!!');
        
      }else{
        this.panLoader=false;
        this.openSnackBar(data['message'],'Error!!');
      }
      
    },
    err => {        
      console.log(err);
      if(err['status'] == 401){
        window.location.replace(err['error']['returnURL']);
      }
      
    },() =>{
      console.log('pan request completed!');
    }
  );
}

  updateResidentialProof() {
    this.ProofLoader = true;
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('addressProofRemarks', this.ResidentialProofFormGroup.value.addressProofRemarks);

    this.ekycService.updateResidentialProof(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ProofLoader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          this.ProofLoader=false;
          
          this.openSnackBar(data['message'],'Error!!');
        }
      },
      err => {        
       	console.log(err);
         if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
  }

 

  onFilePanSelected(event){
    this.selectedPanFile = <File>event.target.files[0];
  }
  
  onFilePPSelected(event){
    this.selectedPPFile = <File>event.target.files[0];
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['mat-snack-bar-container'],
    });
  }

  unlockForm(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.unlockForm(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          //this.loader=false;
          this.openSnackBar(data['message'],'Done!!');
        }else{
          //this.loader=false;
          
        }
      },
      err => {        
       	console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
    
      }
    );
  }

  rejectForm(){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.rejectForm(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
              this.dialogRef.close();
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
 
  }

  approvePanDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve pan details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approvePanDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          },() =>{
            console.log('request completed!');
          }
        );
      }
    });
  }

  rejectPanDetails(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject this pan details.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.remarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.panRemarks = remarksField
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('panRemarks', this.panRemarks);
          this.ekycService.rejectPanDetails(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
              }
            },
            err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
              
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  approveAddressProof(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve address proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveAddressProof(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
            }else{
              //this.loader=false;
            }
          },
          err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
          },() =>{
            console.log('request completed!');
        
          }
        );
      }
    });
  }

  rejectAddressProof(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject address proof details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {

        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.remarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.addressProofRemarks = remarksField;
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('addressProofRemarks', this.addressProofRemarks);
          this.ekycService.rejectAddressProof(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
              }
            },
            err => {        
                console.log(err);
                if(err['status'] == 401){
                  window.location.replace(err['error']['returnURL']);
                }
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }
  

  onNoClick(): void {
    this.dialogRef.close();
  }

  panExtractionResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.panExtractionResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.panExtractionResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  addressProofResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    formData.append('address_proof_type',this.proof_type);
    this.ekycService.addressProofResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.addressProofResponseData = data['result'];
          console.log(this.addressProofResponseData['original_kyc_info'].name);
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

 

  videoVerificationResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);
    this.ekycService.getVideoVerificationResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          if(data['result'] != null){ 
            this.VideoVerificationResponseData = data['result'];
            console.log(this.VideoVerificationResponseData['finalMatchImage2']);
          }
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  approveVideoVerification(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to approve video details.",
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then((result) => {
      if (result.value) {
        const formData = new FormData();
        formData.append('kycFormId', this.kycFormId);
        this.ekycService.approveVideoDetails(formData).subscribe(
          data => {
            if(data['code'] == 200) {
              this.loader=false;
              this.openSnackBar(data['message'],'Done!!');
            }else{
              //this.loader=false;
            }
          },
          err => {        
             console.log(err);
             if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          },() =>{
            console.log('request completed!');
          }
        );
      }
    });
  }

  rejectVideoVerification(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You are going to reject this pan details.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(async (result) => {
      if (result.value) {
        const { value: remarksField } = await Swal.fire({
          title: 'Select Reamrk',
          input: 'select',
          inputOptions: this.remarksDataArray,
          inputPlaceholder: 'Select a remarks',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          inputValidator: (value) => {
            return new Promise((resolve) => {
              resolve()
            })
          }
        });
        if(remarksField){
          this.panRemarks = remarksField
          const formData = new FormData();
          formData.append('kycFormId', this.kycFormId);
          formData.append('videoRemarks', this.panRemarks);
          this.ekycService.rejectVideoDetails(formData).subscribe(
            data => {
              if(data['code'] == 200) {
                this.loader=false;
                this.openSnackBar(data['message'],'Done!!');
              }else{
                //this.loader=false;
              }
            },
            err => {        
              console.log(err);
              if(err['status'] == 401){
                window.location.replace(err['error']['returnURL']);
              }
            },() =>{
              console.log('request completed!');
          
            }
          );
        }
      }
    });
  }

  forgeryResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getForgeryResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ForgeryResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  getImageQualityResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getImageQualityResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ImageQualityResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getNameMatchResponse(){
    const formData = new FormData();
    formData.append('kycFormId', this.kycFormId);

    this.ekycService.getNameMatchResponse(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.NameMatchResponseData = data['result'];
        }else{
         // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getRemarks(type){
    // const formData = new FormData();
    // formData.append('type', type);
    this.ekycService.getRemarksByType(type).subscribe(
      data => {
        if(data['code'] == 200) {
          this.remarksDataArray = data['result']; 
        }else{
          // do something
        }
      },
      err => {        
          console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
  

}

interface Gender {
  value: string;
  viewValue: string;
}

interface AddressProof {
  value: string;
  viewValue: string;
}

interface KycMode {
  value: string;
  viewValue: string;
}

interface Album {
  src: string;
  caption: string;
  thumb:string
}

interface Remarks {
  value: string;
  viewValue: string;
}

interface Remark {
  type: string;
  remark: string;
}