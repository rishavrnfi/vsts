import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerKycComponent } from './partner-kyc.component';

describe('PartnerKycComponent', () => {
  let component: PartnerKycComponent;
  let fixture: ComponentFixture<PartnerKycComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerKycComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerKycComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
