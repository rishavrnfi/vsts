import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AdminService } from '../services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  dashboardStats : any = [];
  loader: Boolean = false;
  user:any;
  userStatus: any;

  constructor(
    
    private userService:UserService,
    private adminService:AdminService,
    private router: Router,
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService,
    ) {
      this.user = this.jwtauth.isAuthenticatedjwttoken();
     }

     ngOnInit(): void {
      // this.loginvalidate();
      this.ngxloader.start();
      const formData = new FormData();
      formData.append('staff_login', 'true');
      this.adminService.getAdminDashboard(formData).subscribe(
        data => {
          this.ngxloader.stop();
          this.dashboardStats = data['result'];			
          this.userStatus = data['user_status'];			
        }, err =>{
          console.log(err);
        }, () =>{
          console.log('Request Completed!');
        }
      )
    }

  async loginvalidate(){
    await this.userService.validatePage();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }
}
