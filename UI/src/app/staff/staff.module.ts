import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffRoutingModule } from './staff-routing.module';
import { StaffComponent } from './staff.component';

import {NgxUiLoaderModule } from 'ngx-ui-loader';
@NgModule({
  declarations: [StaffComponent],
  imports: [
    CommonModule,
    StaffRoutingModule,
    NgxUiLoaderModule
  ]
})
export class StaffModule { }
