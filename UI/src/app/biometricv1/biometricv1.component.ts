import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { parseString } from 'xml2js';
import Swal from 'sweetalert2';
import { environment } from '../../environments/environment';
import { EkycService } from '../services/ekyc.service';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { threadId } from 'worker_threads';
import { MatRadioChange } from '@angular/material/radio';

declare global {
  interface Window { MyNamespace: any; deviceport:any}
}

@Component({
  selector: 'app-biometricv1',
  templateUrl: './biometricv1.component.html',
  styleUrls: ['./biometricv1.component.css'],
  providers:[EkycService]
})

export class Biometricv1Component implements OnInit {

  port:String='';
  rdsUrl:String='http://127.0.0.1';
  BiometricCatureForm: FormGroup;
  biometricLoding:Boolean=false;
  biometricArrayData: any = [];
  cpuniquerefno:string='';
  mobile:string='';
  device_name:String='';
  device_name_list: string[] = ['morpho', 'others'];

  constructor(
    private formBuilder: FormBuilder,
    private ekycService:EkycService,
    private route: ActivatedRoute,
    ) { }

	ngOnInit(): void {
		this.route.queryParams.subscribe(params => {
			this.mobile = params['mobile'];
			this.cpuniquerefno = params['cpuniquerefno'];
			sessionStorage.setItem("mobile", this.mobile);
		});
		this.BiometricCatureForm = this.formBuilder.group({
				device_name: new FormControl("",Validators.compose([
				Validators.required
			])) 
		});
		this.getRdsService();
	}

	capture() {
		console.log(this.port);
		if(this.device_name == 'morpho'){
			var urlStr = this.rdsUrl+':'+this.port+'/capture';	
		}else{
			var urlStr =  this.rdsUrl+':'+this.port+'/rd/capture';
		}
		console.log(urlStr);

		this.getJSONCapture(urlStr, function (err, data, currentobj) {
			if (err != null) {
				console.log('Error Response: ' + err);
			} 
			else 
			{
				let xml = String(data);
				let biometricData = new Object;
				parseString(xml, function (err, result) {						
					if(result.PidData.Resp[0].$.errCode == "0"){
						biometricData = {
							deviceInfo:result.PidData.DeviceInfo[0].$,
							skey:result.PidData.Skey[0],
							hmac:result.PidData.Hmac[0],
							data:result.PidData.Data[0]
						}						  
						window.MyNamespace = data;
						
						currentobj.doBiometric();
					}else{      
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: result.PidData.Resp[0].$.errInfo
						});
					}
				});
			}
		});
	}
  
  	getJSONCapture(url, callback) {
		var xhr;
		if ((<any>window).XMLHttpRequest) {
			try{
				xhr = new XMLHttpRequest();
				xhr.open('CAPTURE', url, true);
				xhr.responseType = 'text';
			}catch(e){
				xhr = new XMLHttpRequest();
				xhr.open('CAPTURE', url, true);
			}
		} else {
			xhr = new XMLHttpRequest();
			xhr.open('CAPTURE', url, true);
		}

		if(environment.production){
			if(this.device_name== 'morpho'){
				var InputXml = '<PidOptions ver="1.0">'+'<Opts fCount="1" fType="0" iCount="0" pCount="0" format="0" pidVer="2.0" timeout="10000" posh="UNKNOWN" env="P" />'+'<CustOpts><Param name="mantrakey" value="" /></CustOpts>'+'</PidOptions>';
			}
			else{
				var InputXml = '<PidOptions ver=\"1.0\">'+'<Opts fCount=\"1\" fType=\"0\" iCount=\"0\" pCount=\"0\" format=\"0\" pidVer=\"2.0\" timeout=\"20000\" otp=\"\"  env=\"P\" wadh=\"E0jzJ/P8UopUHAieZn8CKqS4WPMi5ZSYXgfnlfkWjrc=\" />'+'</PidOptions>'; 	
			}
		}
		else{
			if(this.device_name == 'morpho'){
				var InputXml = '<PidOptions ver=\"1.0\">'+'<Opts fCount=\"1\" fType=\"0\" iCount=\"\" iType=\"\" pCount=\"\" pType=\"\" format=\"0\" pidVer=\"2.0\" timeout=\"10000\" otp=\"\" wadh=\"E0jzJ/P8UopUHAieZn8CKqS4WPMi5ZSYXgfnlfkWjrc=\" posh=\"\"/>'+'</PidOptions>';
			}else{
				var InputXml = '<PidOptions ver=\"1.0\">'+'<Opts fCount=\"1\" fType=\"0\" iCount=\"0\" pCount=\"0\" format=\"0\" pidVer=\"2.0\" timeout=\"20000\" otp=\"\"  env=\"P\" wadh=\"E0jzJ/P8UopUHAieZn8CKqS4WPMi5ZSYXgfnlfkWjrc=\" />'+'</PidOptions>'; 
			}
		}

		xhr.currentObj = this; 	
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				if (xhr.status == 200) {
					callback(null, xhr.responseText,xhr.currentObj);
				} else {
					callback(status,'',xhr.currentObj);
				}
			}
		};
	
		xhr.send(InputXml);
  	};

	async getRdsService() {
		let tempVar = false;
		for (let i = 11100; i <= 11120; i++) {
			await this.checkRdService(i).then(response=>{
				console.log(response);
				tempVar = true;
				this.port = response.toString();
			}).catch(err=>{
				console.log(err);
			});

			if (tempVar) { 
				console.log('RDSERVICE is running on port:'+this.port);
				break; 
			}
		}
		if(!tempVar){
			Swal.fire({
				icon: 'error',
				title: 'RDSERVICE STATUS',
				html: 'RDSERVICE is not running on any port.'
			});
		}
	}

	checkRdService(i){
		return new Promise((resolve,reject)=>{
			try{
				var urlStr = this.rdsUrl+':'+i.toString()+'/';
				this.getJSON_rd(urlStr, function (err, data) {
					if (err != null) {
						console.log('Biometric Device Response: ' + err);
						reject('RD service is not running on port '+i);
					} else { 
						window.deviceport = i;
						resolve(i);
					}
				});	
				
			}catch(err){
				reject(err);
			}
		});
	}
  
	getJSON_rd(url, callback){
		var xhr;
		if ((<any>window).XMLHttpRequest) {
			try{
				xhr = new XMLHttpRequest();
				xhr.open('RDSERVICE', url, true);
				xhr.responseType = 'text';
			}catch(e){
				xhr = new XMLHttpRequest();
				xhr.open('RDSERVICE', url, true);
			}
		} else {
			xhr = new XMLHttpRequest();
			xhr.open('RDSERVICE', url, true);
		}

		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				if (xhr.status == 200) {
					callback(null, xhr.responseText);
				} else {
					callback(status);
				}
			}
		};
		xhr.send();
	};

	async getDeviceInformation() {
		await this.finDeviceInfo().then(response=>{
		console.log(response);
		}).catch(err=>{
			console.log(err);
		})
		
	}
  
	finDeviceInfo(){
		return new Promise((resolve,reject)=>{
			var urlStr = '';
			if(this.device_name=='morpho'){
				urlStr = this.rdsUrl+':'+this.port+'/getDeviceInfo';
			}else{
				urlStr = this.rdsUrl+':'+this.port+'/rd/info';
			}
			
			this.getJSON_info(urlStr,function (err, data) {
				if (err != null) {
					console.log('Error Response: ' + err);
					reject(err);
				} else {
					console.log('Success Response' + data);
					parseString(data, function (err, result) {
						console.log(result);
						resolve(result.DeviceInfo.dpId);
					})
				}
			});
		});
	}
  
	getJSON_info(url, callback){
		var xhr;
		if ((<any>window).XMLHttpRequest) {
			try{
				xhr = new XMLHttpRequest();
				xhr.open('DEVICEINFO', url, true);
				xhr.responseType = 'text';
			}catch(e){
				xhr = new XMLHttpRequest();
				xhr.open('DEVICEINFO', url, true);
			}
		} else {
			xhr = new XMLHttpRequest();
			xhr.open('DEVICEINFO', url, true);
		}

		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				if (xhr.status == 200) {
					callback(null, xhr.responseText);
				} else {
					callback(status);
				}
			}
		};
		xhr.send();
	};
  
  doBiometric() {
 	if (this.BiometricCatureForm.valid) {

		if(window.MyNamespace != null){		
			const formData = new FormData();
			formData.append('biometricData', window.MyNamespace);
			formData.append('cpuniquerefno', this.cpuniquerefno);
			formData.append('mobile',this.mobile);
			this.ekycService.doBiometricv1(formData).subscribe(
				data =>{
					if(data['status']){
						window.MyNamespace = null;
						this.biometricArrayData = data['result'];
						Swal.fire({
							icon: 'success',
							title: 'Biometric KYC',
							text: data['message']
						});
					}else{
						window.MyNamespace   = null;
						this.biometricLoding = false;
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: data['message']
						});
					}
					this.BiometricCatureForm.reset();
				},err =>{
					this.biometricLoding = false;	
				},() =>{
					this.biometricLoding = false;	
					console.log('Request Completed!');
				}) 
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Please capture fingerprint.'
				});
			}
	 	}	
 	}
}
