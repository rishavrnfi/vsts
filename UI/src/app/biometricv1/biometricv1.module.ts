import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';

import { Biometricv1RoutingModule } from './biometricv1-routing.module';
import { Biometricv1Component } from './biometricv1.component';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [Biometricv1Component],
  imports: [
    CommonModule,
    Biometricv1RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule
  ]
})
export class Biometricv1Module { }
