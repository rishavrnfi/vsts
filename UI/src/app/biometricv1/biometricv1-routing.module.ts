import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Biometricv1Component } from './biometricv1.component';

const routes: Routes = [{ path: '', component: Biometricv1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Biometricv1RoutingModule { }
