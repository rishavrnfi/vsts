import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Biometricv1Component } from './biometricv1.component';

describe('BiometricComponent', () => {
  let component: Biometricv1Component;
  let fixture: ComponentFixture<Biometricv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Biometricv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Biometricv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
