import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AadharOtpComponent } from './aadhar-otp.component';

describe('AadharOtpComponent', () => {
  let component: AadharOtpComponent;
  let fixture: ComponentFixture<AadharOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AadharOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AadharOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
