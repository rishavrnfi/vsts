import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { EkycService } from '../services/ekyc.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
declare var $: any;

@Component({
  selector: 'app-aadhar-otp',
  templateUrl: './aadhar-otp.component.html',
  styleUrls: ['./aadhar-otp.component.css'],
  // providers: [EkycService],
})
export class AadharOtpComponent implements OnInit {
  @Input() labelPosOneResult: any;
  agent_token: string = '';
  AadharOtpCredentials: any = [];
  client_code: string = '';
  api_key: string = '';
  redirect_url: string = '';
  request_id: string = '';
  stan: string = '';
  hash: string = '';
  mobile: string = '';
  otp_required: String = '';
  aadharotploader: Boolean = false;
  labelPosition: any;
  aadharotpform: Boolean = true;
  initWebVIKYCURL = '';
  loader: Boolean = false;
  AadharOtpFormGroup: FormGroup;
  AadharchooseCredentials: any;
  aadharotpdiv: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private ekycService: EkycService
  ) {}

  ngOnInit(): void {
    this.aadharotploader=true;
    this.labelPosition = this.labelPosOneResult;

    this.client_code = this.labelPosOneResult['client_code'];
    this.api_key = this.labelPosOneResult['api_key'];
    this.request_id = this.labelPosOneResult['request_id'];
    this.stan = this.labelPosOneResult['stan'];
    this.hash = this.labelPosOneResult['hash'];
    this.mobile = this.labelPosOneResult['mobile'];
    this.otp_required = this.labelPosOneResult['otp_required'];
    this.initWebVIKYCURL = this.labelPosOneResult['initWebVIKYC'];

    this.redirect_url = environment.appBaseUrl + '/aadhar-response';
    
    setTimeout(function () {
      this.aadharotploader=false;
      $('#click').trigger('click');
    }, 4000);

    this.AadharOtpFormGroup = new FormGroup({
      client_code: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      api_key: new FormControl('', Validators.compose([Validators.required])),
      redirect_url: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      request_id: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
      stan: new FormControl('', Validators.compose([Validators.required])),
      hash: new FormControl('', Validators.compose([Validators.required])),
      mobile: new FormControl('', Validators.compose([Validators.required])),
      otp_required: new FormControl(
        '',
        Validators.compose([Validators.required])
      ),
    });
  }

  aadharOtpSubmit(e) {
    e.target.submit();
  }

  // getAadharOtpCredentials(agent_token) {
  //   this.ekycService.getAadharOtpCredentials(agent_token).subscribe(
  //     (data) => {
  //       if (data['status']) {
  //         this.AadharOtpCredentials = data['result'];
  //         this.client_code = this.AadharOtpCredentials['client_code'];
  //         this.api_key = this.AadharOtpCredentials['api_key'];
  //         this.redirect_url = environment.appBaseUrl + '/aadhar-response';
  //         this.request_id = this.AadharOtpCredentials['request_id'];
  //         this.stan = this.AadharOtpCredentials['stan'];
  //         this.hash = this.AadharOtpCredentials['hash'];
  //         this.mobile = this.AadharOtpCredentials['mobile'];
  //         this.otp_required = this.AadharOtpCredentials['otp_required'];
  //         this.initWebVIKYCURL = this.AadharOtpCredentials['initWebVIKYC'];
  //       } else {
  //         //do something
  //       }
  //     },
  //     (err) => {
  //       console.log(err);
  //     },
  //     () => {
  //       // this.aadharotploader=false;
  //       console.log('request completed!');
  //       setTimeout(function () {
  //         $('#click').trigger('click');
  //       }, 4000);
  //     }
  //   );
  // }
}
