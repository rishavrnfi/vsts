import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgxUiLoaderModule } from 'ngx-ui-loader';

import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AadharOtpComponent } from './aadhar-otp/aadhar-otp.component';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { VideoKycComponent } from './video-kyc/video-kyc.component';
import { VideoResponseComponent } from './video-response/video-response.component';
import { AadharResponseComponent } from './aadhar-response/aadhar-response.component';
import { GoodpipePipe } from './hello/goodpipe.pipe';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgSelect2Module } from 'ng-select2';
import { SharedModule } from './shared/shared.module';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
  declarations: [
    AppComponent,
    // AadharOtpComponent,
    VideoKycComponent,
    VideoResponseComponent,
    AadharResponseComponent,
    GoodpipePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    NgSelect2Module,
    SharedModule,
    NgxUiLoaderModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
