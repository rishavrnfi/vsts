import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoVerificationRoutingModule } from './video-verification-routing.module';
import { VideoVerificationComponent } from './video-verification.component';


@NgModule({
  declarations: [VideoVerificationComponent],
  imports: [
    CommonModule,
    VideoVerificationRoutingModule
  ]
})
export class VideoVerificationModule { }
