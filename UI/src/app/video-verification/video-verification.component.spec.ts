import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoVerificationComponent } from './video-verification.component';

describe('VideoVerificationComponent', () => {
  let component: VideoVerificationComponent;
  let fixture: ComponentFixture<VideoVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
