import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoVerificationComponent } from './video-verification.component';

const routes: Routes = [{ path: '', component: VideoVerificationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoVerificationRoutingModule { }
