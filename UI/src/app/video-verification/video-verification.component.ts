import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef
} from '@angular/core';

import videojs from 'video.js';
import * as adapter from 'webrtc-adapter/out/adapter_no_global.js';
import * as RecordRTC from 'recordrtc';

import * as Record from 'videojs-record/dist/videojs.record.js';

import { Router, ActivatedRoute, CanActivate } from '@angular/router'; 
import { EkycService } from '../services/ekyc.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-video-verification',
  templateUrl: './video-verification.component.html',
  styleUrls: ['./video-verification.component.css'],
  providers:[EkycService]
})
export class VideoVerificationComponent implements OnInit {

  // reference to the element itself: used to access events and methods
  private _elementRef: ElementRef

  // index to create unique ID for component
  idx = 'clip1';

  private config: any;
  private player: any; 
  private plugin: any;
  saveVideo: Boolean = false;
  mobileNumber: any;
  loader: Boolean = false;
  status: Boolean = false;
  showTimer: Boolean = false;
  showReady: Boolean = false;
  msg: string = '';
  random: string = '';


  timeLeft: number = 5;
  interval;

 
  // constructor initializes our declared vars
  constructor(
    elementRef: ElementRef,
    private route: ActivatedRoute,
    private ekycService:EkycService,
    ) {
    
    this.player = false;

    // save reference to plugin (so it initializes)
    this.plugin = Record;

    // video.js configuration
    this.config = {
      controls: false,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 320,
      height: 240,
      bigPlayButton: false,
      controlBar: {
        volumePanel: false
      },
      plugins: {
        /*
        // wavesurfer section is only needed when recording audio-only
        wavesurfer: {
            backend: 'WebAudio',
            waveColor: '#36393b',
            progressColor: 'black',
            debug: true,
            cursorWidth: 1,
            displayMilliseconds: true,
            hideScrollbar: true,
            plugins: [
                // enable microphone plugin
                WaveSurfer.microphone.create({
                    bufferSize: 4096,
                    numberOfInputChannels: 1,
                    numberOfOutputChannels: 1,
                    constraints: {
                        video: false,
                        audio: true
                    }
                })
            ]
        },
        */
        // configure videojs-record plugin
        record: {
          audio: true,
          video: true,
          debug: true,
          maxLength: 5,
        }
      }
    };
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.mobileNumber = params['mobile'];
      this.status = params['status'];
      this.msg = (typeof params['msg'] != 'undefined' && params['msg'] != 'undefined') ? params['msg']: '';
    });
    console.log(this.msg)
  }

  // use ngAfterViewInit to make sure we initialize the videojs element
  // after the component template itself has been rendered
  ngAfterViewInit() {
    // ID with which to access the template's video element
    let el = 'video_' + this.idx;

    // setup the player via the unique element ID
    this.player = videojs(document.getElementById(el), this.config, () => {
      console.log('player ready! id:', el);

      // print version information at startup
      var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
      videojs.log(msg);
    });

    this.player.record().getDevice();


    // device is ready
    this.player.on('deviceReady', () => {
      console.log('device is ready!');
      this.showReady = true;
    });

    // user clicked the record button and started recording
    this.player.on('startRecord', () => {
      console.log('started recording!');
      this.saveVideo = false;
      });

    // user completed recording and stream is available
    this.player.on('finishRecord', () => {
      // recordedData is a blob object containing the recorded data that
      // can be downloaded by the user, stored on server etc.
      console.log('finished recording: ', this.player.recordedData);
      this.uploadVideo()
      this.saveVideo = true;
    });

    // error handling
    this.player.on('error', (element, error) => {
      console.warn(error);
    });

    this.player.on('deviceError', () => {
      console.error('device error:', this.player.deviceErrorCode);
    });
  }
  
  uploadVideo(){
    
    // var myFile = this.blobToFile(this.player.recordedData, "my-image.mp4");
    console.log(this.player)

    this.loader = true;
    var fileName = this.getFileName('mp4');

    // we need to upload "File" --- not "Blob"
    var fileObject = new File([this.player.recordedData], fileName, {
        type: 'video/mp4'
    });

    // URL.createObjectURL(this.player.recordedData);

    const formData = new FormData();
    // formData.append('video', (myFile));

    // var formData = new FormData();

    // recorded data
    formData.append('video-blob', fileObject);
    formData.append('video-filename', fileObject.name);
    formData.append('mobile',this.mobileNumber);

    this.ekycService.uploadVideo(formData).subscribe(
      data => {
        if(data['status']){
          this.loader = false;
          window.location.replace(environment.appBaseUrl+'/video-verification?status=success&msg='+data['message'])
        }else{
          // do something
          this.loader = false;
          window.location.replace(environment.appBaseUrl+'/video-verification?status=false&msg='+data['message'])
        }
      },
      err => {        
         console.log(err);
         window.location.replace(environment.appBaseUrl+'/video-verification?status=false&msg=Something went wrong, try again')
      },() =>{
        // this.aadharotploader=false;
        console.log('request completed!');
      }
    );
  }

  
  // this function is used to generate random file name
  getFileName(fileExtension) {
    var d = new Date();
    var year = d.getUTCFullYear();
    var month = d.getUTCMonth();
    var date = d.getUTCDate();
    return 'rnfi-'+Math.random() + '.' + fileExtension;
}

  blobToFile(theBlob, fileName){
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return <File>theBlob;
}

startRecording(){
  this.player.record().start();
  // setTimeout(function(){ 
  //   this.player.record().stop();
  // }, 3000);
  this.random = this.randomString();
  this.showTimer = true
  this.showReady = false
  this.startTimer()
}


randomString() {
  let chars = '0123456789';
  let length = 4;
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

startTimer() {
  this.interval = setInterval(() => {
    if(this.timeLeft > 0) {
      this.timeLeft--;
    } else {
      // this.showTimer = false
      this.timeLeft = 0;
    }
  },1000)
}

  // use ngOnDestroy to detach event handlers and remove the player
  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
      this.player = false;
    }
  }

}
