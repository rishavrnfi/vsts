import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseAadharComponent } from './choose-aadhar.component';

describe('ChooseAadharComponent', () => {
  let component: ChooseAadharComponent;
  let fixture: ComponentFixture<ChooseAadharComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseAadharComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseAadharComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
