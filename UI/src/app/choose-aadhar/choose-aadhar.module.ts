import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChooseAadharRoutingModule } from './choose-aadhar-routing.module';
import { ChooseAadharComponent } from './choose-aadhar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AadharOtpComponent } from '../aadhar-otp/aadhar-otp.component';
import { ChooseAadharOtpComponent } from './choose-aadhar-otp/choose-aadhar-otp.component';

@NgModule({
  declarations: [
    ChooseAadharComponent,
    AadharOtpComponent,
    ChooseAadharOtpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ChooseAadharRoutingModule,
  ],
})
export class ChooseAadharModule {}
