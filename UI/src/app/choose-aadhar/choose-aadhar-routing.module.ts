import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChooseAadharComponent } from './choose-aadhar.component';

const routes: Routes = [
  {
    path: '',
    component: ChooseAadharComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseAadharRoutingModule {}
