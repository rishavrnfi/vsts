import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseAadharOtpComponent } from './choose-aadhar-otp.component';

describe('ChooseAadharOtpComponent', () => {
  let component: ChooseAadharOtpComponent;
  let fixture: ComponentFixture<ChooseAadharOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseAadharOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseAadharOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
