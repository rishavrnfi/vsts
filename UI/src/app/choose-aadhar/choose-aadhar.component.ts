import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { EkycService } from '../services/ekyc.service';
@Component({
  selector: 'app-choose-aadhar',
  templateUrl: './choose-aadhar.component.html',
  styleUrls: ['./choose-aadhar.component.css'],
})
export class ChooseAadharComponent implements OnInit {
  clickable:boolean= false;
  hiddentab:any;
  hiddentab1:any;
  loader: Boolean = false;
  viewAadharOtp: boolean = true;
  messageText: any;
  redirect_url: string = '';
  AadharchooseCredentials: any = [];
  chooseForm: FormGroup;
  aadharotploader: Boolean = false;
  labelPosition: any;
  agent_token: any;
  msg: string = '';
  constructor(
    private ekycservice: EkycService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.agent_token = params['agent_token'];
      sessionStorage.setItem('agent_token',this.agent_token );
    });
    this.loader = true;
    this.ekycservice.checkoptionforAadhar(this.agent_token).subscribe((res:any)=>{
      this.loader = false;
      if (res['status']) {
        this.hiddentab =res['data'].is_nonaadhaar;
        this.hiddentab1 =res['data'].is_aadhaar;
      }
    },(err)=>{
      this.loader = false;
    })
  }

  onSubmit() {
    this.loader = true;
    this.clickable=true;
    this.ekycservice
    .chooseaddhartype(
      sessionStorage.getItem('agent_token'),
      this.labelPosition
      )
      .subscribe((res: any) => {
        // this.loader = false;
        this.clickable=false;
        if (this.labelPosition == 1) {
          if (res['status']) {
            this.AadharchooseCredentials = res['result'];
            this.viewAadharOtp = false;
          }
        } else {
          if (res['status']) {
            this.checkMatchers();
          }
        }
      },(err)=>{
        this.loader = false;
        this.clickable=false;
      });
  }


  checkMatchers() {
    localStorage.clear();
    this.loader = true;
    this.ekycservice.checkMatchersFornOnOtp(sessionStorage.getItem('agent_token')).subscribe(
      (data) => {
        this.loader = false;
        this.messageText = data['message'] ? data['message'] : '';
        if (data['status']) {
          localStorage.setItem('msg', data['message']);
          window.location.replace(
            environment.appBaseUrl + '/aadhar-response/?status=success'
          );
        } else {
          if(typeof data['result']['api_status'] != 'undefined' ){
            localStorage.setItem('msg', data['message']);
            window.location.replace(
              environment.appBaseUrl + '/aadhar-response/?status=failed'
            );
          }
          else if(typeof data['partialMatch'] != 'undefined'){
            localStorage.setItem('msg', data['message']);
            window.location.replace(
              environment.appBaseUrl + '/aadhar-response/?status=mismatch'
            );
          }
          
        }
      },
      (err) => {

        this.loader = false;
      },
      () => {
        // this.loader = false;
        console.log('request completed!');
      }
    );
  }

}
