import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminReportsRoutingModule } from './admin-reports-routing.module';
import { AdminReportsComponent,DialogDataKycDialog } from './admin-reports.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatRadioModule } from '@angular/material/radio';
import { NgxEditorModule } from 'ngx-editor';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { LightboxModule } from 'ngx-lightbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {NgxUiLoaderModule } from 'ngx-ui-loader';
// import {MaskDirective } from '../directive/mask.directive';
import { SharedModule } from '../shared/shared.module'

MatDatepickerModule
@NgModule({
  declarations: [AdminReportsComponent,DialogDataKycDialog],
  imports: [
    NgxUiLoaderModule,
    CommonModule,
    AdminReportsRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MaterialFileInputModule,
    MatRadioModule,
    NgxEditorModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    LightboxModule,
    MatDialogModule,
    MatAutocompleteModule,
    SharedModule
  ]
})
export class AdminReportsModule { }
