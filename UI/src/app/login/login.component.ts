import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthjwtService } from '../services/authjwt.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService, AuthjwtService],
})
export class LoginComponent implements OnInit {
  // isModal: any = 1;
  loginForm: FormGroup;
  isError: Boolean = false;
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService
  ) {}

  ngOnInit(): void {
    if (sessionStorage.getItem('token')) {
      this.userService.validatePage();
    }
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: [null, Validators.required],
    });
  }

  login() {
    this.isError = false;
    this.ngxloader.start();
    this.loginForm.value.loginMode = 'web';
    this.userService.login(this.loginForm.value).subscribe(
      (data) => {
        this.ngxloader.stop();
        if (data['status'] && data['is_model'] == 0) {
          sessionStorage.setItem('token', data['token']);
          this.user = this.jwtauth.isAuthenticatedjwttoken();
          if (this.user.type == 1 || this.user.type == 3) {
            this.router.navigate(['admin-dashboard']);
          } else {
            this.router.navigate(['staff-dashboard']);
          }
        } else if (data['status'] && data['is_model'] == 1) {
          Swal.fire({
            title: 'Regret',
            text: "Sorry For Inconvenience",
            icon: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Use this Url'
          }).then((result) => {
            if (result.value) {
              window.open(data['redirect_url']);
            }
          })
        } else {
          this.isError = true;
          this.loginForm.reset();
        }
      },
      (err) => {
        this.isError = true;
        console.log(err);
        this.ngxloader.stop();
      },
      () => {
        console.log('request completed!');
      }
    );
  }

}
