import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EkycService } from '../services/ekyc.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'app-partner-aadhar-otp',
  templateUrl: './partner-aadhar-otp.component.html',
  styleUrls: ['./partner-aadhar-otp.component.css']
})
export class PartnerAadharOtpComponent implements OnInit {

  agent_token:string='';
  AadharOtpCredentials:any=[];
  client_code:string='';
  api_key:string='';
  redirect_url:string='';
  request_id:string='';
  stan:string='';
  hash:string='';
  mobile:string='';
  otp_required:string='';
  aadharotploader:Boolean=true;
  aadharotpform:Boolean=true;
  initWebVIKYCURL='';
  loader:Boolean=false;
  AadharOtpFormGroup: FormGroup;
  requestIdResponse: any;
  userIdResponse: any;
  statusResponse: any;
  hashResponse: any;
  msg: string ='';
  uuidResponse: any;
  aadhar_status: string = '';
  constructor(
    private route: ActivatedRoute,
    private ekycService:EkycService,
  ) { }


  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.mobile = params['mobile'];

      this.requestIdResponse = params['requestId'];
      this.userIdResponse    = params['userId'];
      this.uuidResponse      = params['uuid'];
      this.hashResponse      = params['hash'];
      this.statusResponse    = params['status'];
      this.aadhar_status     = params['aadhar_status'];
  });

  // if(typeof this.aadhar_status !=='undefined' &&  this.aadhar_status !=null &&  this.aadhar_status !=''){
  //   this.aadharotploader = false;
  //   this.aadharotpform   = false;
  //   this.msg              = typeof localStorage.getItem("msg") !=='undefined' ? localStorage.getItem("msg") : '';
  // }
  
  this.AadharOtpFormGroup = new FormGroup({
    client_code: new FormControl("",Validators.compose([
      Validators.required
    ])),
    api_key: new FormControl("",Validators.compose([
      Validators.required
    ])),
    redirect_url: new FormControl("",Validators.compose([
      Validators.required
    ])),
    request_id: new FormControl("",Validators.compose([
      Validators.required
    ])),
    stan: new FormControl("",Validators.compose([
      Validators.required
    ])),
    hash: new FormControl("",Validators.compose([
      Validators.required
    ])),
    mobile: new FormControl("",Validators.compose([
      Validators.required
    ])),
    otp_required: new FormControl("",Validators.compose([
      Validators.required
    ])), 
  });

  if(typeof this.statusResponse !=='undefined' && this.statusResponse=='FAIL'){   
    localStorage.setItem("msg", 'Aadhar Otp failed');
    window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=failed');
  }
  else if(typeof this.requestIdResponse !=='undefined' && this.requestIdResponse != null && typeof this.userIdResponse !=='undefined'
   && typeof this.uuidResponse !=='undefined' && typeof this.hashResponse !=='undefined' && typeof this.statusResponse !=='undefined')
  {   
      localStorage.removeItem("msg");
      this.msg = '';
      this.aadharotploader = false;
      this.aadharotpform = false;
      this.loader = true;
      this.ekycService.savePartnerAadharOtpResponse({
        requestId:this.requestIdResponse,
        userId:this.userIdResponse,
        uuid:this.uuidResponse,
        hash:this.hashResponse,
        status:this.statusResponse
      }).subscribe(
        data => {
          if(data['status']) {
            console.log(data['message'])
            localStorage.setItem("msg", data['message']);
            window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=success');
          }else{
            if(typeof data['result']['partial_match'] !=='undefined' && data['result']['partial_match']){

              Swal.fire({
                title: 'Name does not match with pan',
                text: data['message'],
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, continue with this name!',
                allowOutsideClick: false
              }).then((result) => {
                if (result.value) {

                  this.ekycService.continueWithPartnerName({mobile:data['mobile']}).subscribe(
                    data => {
                      
                      if(data['status']) {
                        this.loader=false;
                        localStorage.setItem("msg", data['message']);
                        console.log(data['message'])
                        window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=success');
                        
                      }else{
                        this.loader=false;
                        localStorage.setItem("msg", data['message']);
                        console.log(data['message'])
                        window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=failed');
            
                      }
                      
                    },
                    err => {        
                       console.log(err);
                      
                    },() =>{
                      console.log('request completed!');
                    }
                  );
                }
                else {
                  localStorage.setItem("msg", 'Your Stage is at pending state');
                  window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=failed');
            
                }
              })
            }
            else if( typeof data['message'] !=='undefined' ){
              this.loader=false;
              localStorage.setItem("msg", data['message']);
              window.location.replace(environment.appBaseUrl+'/partner-aadhar-otp/?aadhar_status=failed');
            
            }
         }
        },
        err => {        
           console.log(err);      
        },() =>{
          console.log('request completed!');
        }
      );

      // localStorage.setItem("msg", this.messageText);
  }
  else if(typeof this.aadhar_status !=='undefined' &&  this.aadhar_status !=null &&  this.aadhar_status !=''){
    this.aadharotploader = false;
    this.aadharotpform   = false;
    this.msg              = typeof localStorage.getItem("msg") !=='undefined' ? localStorage.getItem("msg") : '';
  }
  else {
    this.getAadharOtpCredentials(this.mobile);
  }
  
  }

  aadharOtpSubmit(e){
    e.target.submit();
  }

  getAadharOtpCredentials(mobile){
    
    this.ekycService.getPartnerAadharOtpCredentials(mobile).subscribe(
      data => {
        if(data['status']){
          this.AadharOtpCredentials = data['result'];
          this.client_code     = this.AadharOtpCredentials['client_code'];
          this.api_key         = this.AadharOtpCredentials['api_key'];
          this.redirect_url    = environment.appBaseUrl+'/partner-aadhar-otp';
          this.request_id      = this.AadharOtpCredentials['request_id'];
          this.stan            = this.AadharOtpCredentials['stan'];
          this.hash            = this.AadharOtpCredentials['hash'];
          this.mobile          = this.AadharOtpCredentials['mobile'];
          this.otp_required    = this.AadharOtpCredentials['otp_required'];
          this.initWebVIKYCURL = this.AadharOtpCredentials['initWebVIKYC']
        }else{
          //do something
        }
      },
      err => {        
         console.log(err);
      },() =>{
        // this.aadharotploader=false;
        console.log('request completed!');
        setTimeout(function(){ $("#click").trigger("click"); }, 4000);
      }
    );
  }
  
}
