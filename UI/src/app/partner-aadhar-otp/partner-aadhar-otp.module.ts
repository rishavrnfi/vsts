import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnerAadharOtpRoutingModule } from './partner-aadhar-otp-routing.module';
import { PartnerAadharOtpComponent } from './partner-aadhar-otp.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [PartnerAadharOtpComponent],
  imports: [
    CommonModule,
    PartnerAadharOtpRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class PartnerAadharOtpModule { }
