import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerAadharOtpComponent } from './partner-aadhar-otp.component';

describe('PartnerAadharOtpComponent', () => {
  let component: PartnerAadharOtpComponent;
  let fixture: ComponentFixture<PartnerAadharOtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerAadharOtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerAadharOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
