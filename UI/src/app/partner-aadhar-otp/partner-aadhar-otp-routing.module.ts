import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnerAadharOtpComponent } from './partner-aadhar-otp.component';

const routes: Routes = [{ path: '', component: PartnerAadharOtpComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerAadharOtpRoutingModule { }
