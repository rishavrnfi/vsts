import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BiometricKycRoutingModule } from './biometric-kyc-routing.module';
import { BiometricKycComponent } from './biometric-kyc.component';


@NgModule({
  declarations: [BiometricKycComponent],
  imports: [
    CommonModule,
    BiometricKycRoutingModule
  ]
})
export class BiometricKycModule { }
