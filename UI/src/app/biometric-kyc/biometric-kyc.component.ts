import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EkycService } from '../services/ekyc.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-biometric-kyc',
  templateUrl: './biometric-kyc.component.html',
  styleUrls: ['./biometric-kyc.component.css']
})
export class BiometricKycComponent implements OnInit {
  private agent_token: string = '';
  private mobileNumber: string = '';
  private method:string = '';
  status: any;
  rbl_status: any;

  constructor(
    private route: ActivatedRoute,
    private ekycService:EkycService,
  ) { }

  loader:Boolean=false;
  msgTxt : string ='';

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.agent_token = params['agent_token'];
      this.mobileNumber = params['mobile'];
      this.method =   params['type'];
      if(typeof params['status'] !== 'undefined' && params['status'] != '' ){
        if(params['status'] == 'rbl_failed'){
          this.msgTxt = localStorage.getItem('msg') !== 'undefined'
          ? localStorage.getItem('msg')
          : '';
        }
        this.status = params['status'];
      }
      else {
        this.status = ''
      }
    });
    if(typeof this.agent_token != 'undefined' && this.agent_token !='' && typeof this.mobileNumber != 'undefined' && this.mobileNumber != '') {
      localStorage.removeItem('msg');
      if(typeof this.method !== 'undefined' && this.method !==''){
        this.bioOldMetricSubmit();
      }else{
        this.bioMetricSubmit();
      }
    }
  }

  bioMetricSubmit() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    formData.append('mode', 'mobile');

    this.ekycService.bioMetricSubmit(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          window.location.assign(data['result']);
        }else{
          localStorage.setItem('msg', data['message']);
          window.location.replace(
            environment.appBaseUrl + '/biometric-kyc?status=rbl_failed'
          );
        }
      },
      err => {
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }

  bioOldMetricSubmit() {
    this.loader = true;
    const formData = new FormData();
    formData.append('agent_token', this.agent_token);
    formData.append('mobile', this.mobileNumber);
    formData.append('mode', 'mobile');

    this.ekycService.bioMetricOldSubmit(formData).subscribe(
      data => {
        if(data['status']) {
          this.loader=false;
          window.location.assign(data['result']);
        }else{
          localStorage.setItem('msg', data['message']);
          window.location.replace(
            environment.appBaseUrl + '/biometric-kyc?status=rbl_failed'
          );
        }
      },
      err => {
       	console.log(err);
      },() =>{
        console.log('request completed!');
      }
    );
  }


}