import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BiometricKycComponent } from './biometric-kyc.component';

const routes: Routes = [{ path: '', component: BiometricKycComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BiometricKycRoutingModule { }
