import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiometricKycComponent } from './biometric-kyc.component';

describe('BiometricKycComponent', () => {
  let component: BiometricKycComponent;
  let fixture: ComponentFixture<BiometricKycComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiometricKycComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiometricKycComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
