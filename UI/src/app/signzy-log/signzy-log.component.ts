import { Component, OnInit } from '@angular/core';
import { EkycService } from '../services/ekyc.service';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { PageEvent } from '@angular/material/paginator';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-signzy-log',
  templateUrl: './signzy-log.component.html',
  styleUrls: ['./signzy-log.component.css'],
  providers:[EkycService]
})
export class SignzyLogComponent implements OnInit {

  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  SignzyLogFormData: FormGroup;
  loader:Boolean = false;
  result = [];
  length = 100;
  pageSize = 100;
  pageEvent: PageEvent;
  page:number=1;
  api_names: [];
  logs: any;
  fromdate: String = '';
  todate: String = '';

  minFromDate = new Date(2020, 0, 1);
  maxFromDate = new Date(2021, 0, 1);

  minToDate   = new Date(2020, 0, 1);
  maxToDate   = new Date(2021, 0, 1);

  constructor(
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    private userService:UserService,
    private router: Router,   
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private ngxloader:NgxUiLoaderService
  ) { 
    this.user = this.jwtauth.isAuthenticatedjwttoken();
  }
  ngOnInit(): void {
    this.SignzyLogFormData = new FormGroup({
      uservalue:new FormControl("",Validators.compose([
        // Validators.required,
        // Validators.max(10),
        // Validators.min(10)
      ])),
      mobile:new FormControl("",Validators.compose([
      ])),
      fromdate: new FormControl("",Validators.compose([
      ])),
      todate: new FormControl("",Validators.compose([
      ])),
      page:new FormControl("",Validators.compose([
      ]))
    });

    this.getSignzyApi();
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  getLogs(){

    this.ngxloader.start();
    const formData = new FormData();
    formData.append('api_name', this.SignzyLogFormData.value.uservalue);
    formData.append('page', this.SignzyLogFormData.value.page);
    formData.append('fromdate', this.SignzyLogFormData.value.fromdate);
    formData.append('todate', this.SignzyLogFormData.value.todate);
    formData.append('mobile', this.SignzyLogFormData.value.mobile);

    this.ekycService.getSignzyLog(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.ngxloader.stop();
          this.result = data['result']['data'];
          this.length = data['result']['total'];
          // this.openSnackBar(data['message'],'Done!!');
        }else{
          this.ngxloader.stop();
          // this.openSnackBar(data['message'],'Error!!');
        }
        
      },
      err => {        
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );

  }

  getSignzyApi(){
    this.ngxloader.start();
    // const formData = new FormData();
    // formData.append('mobileNumber', this.SearchFormData.value.uservalue);
    // formData.append('page', this.SearchFormData.value.page);

    this.ekycService.getSignzyApi().subscribe(
      data => {
        if(data['code'] == 200) {
          this.ngxloader.stop();
          this.api_names = data['result'];
          this.getLogs();
          // this.length = data['result']['total'];
          // this.openSnackBar(data['message'],'Done!!');
        }else{
          this.ngxloader.stop();
          // this.openSnackBar(data['message'],'Error!!');
        }
        
      },
      err => {        
        console.log(err);
        if(err['status'] == 401){
          window.location.replace(err['error']['returnURL']);
        }
      },() =>{
        console.log('request completed!');
      }
    );
  }

  pageevent(event){
    this.SignzyLogFormData.value.page = event.pageIndex+1;
    this.page = event.pageIndex+1;
    this.getLogs();
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.minToDate = event.value;
  }

}
