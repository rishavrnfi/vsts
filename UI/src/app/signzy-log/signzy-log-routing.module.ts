import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignzyLogComponent } from './signzy-log.component';

const routes: Routes = [{ path: '', component: SignzyLogComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignzyLogRoutingModule { }
