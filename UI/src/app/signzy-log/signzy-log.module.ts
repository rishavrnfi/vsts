import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignzyLogRoutingModule } from './signzy-log-routing.module';
import { SignzyLogComponent } from './signzy-log.component';

import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule }  from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  declarations: [SignzyLogComponent],
  imports: [
    NgxUiLoaderModule,
    CommonModule,
    SignzyLogRoutingModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class SignzyLogModule { }
