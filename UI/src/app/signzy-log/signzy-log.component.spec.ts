import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignzyLogComponent } from './signzy-log.component';

describe('SignzyLogComponent', () => {
  let component: SignzyLogComponent;
  let fixture: ComponentFixture<SignzyLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignzyLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignzyLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
