import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { environment } from '../../environments/environment';
import { AdminService } from '../services/admin.service';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { EkycService } from '../services/ekyc.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import Swal from 'sweetalert2';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
  selector: 'app-delete-forms',
  templateUrl: './delete-forms.component.html',
  styleUrls: ['./delete-forms.component.css']
})
export class DeleteFormsComponent implements OnInit {

  SearchFormData: FormGroup;
  AddUserFormGroup: FormGroup;
  user: any;
  apiBaseUrl: string;
  tokenvar: string;
  loader:Boolean=false;
  reportsList:  any = [];
  length: any;

  constructor(
    private userService:UserService,
    private router: Router,
    private route:ActivatedRoute,
    public jwtauth: AuthjwtService,
    private formBuilder: FormBuilder,
    private ekycService: EkycService,
    private _snackBar: MatSnackBar,
    private adminService:AdminService,
    public dialog: MatDialog,
    private ngxloader:NgxUiLoaderService
    ) {
      this.user       = this.jwtauth.isAuthenticatedjwttoken();
      this.apiBaseUrl = environment.apiBaseUrl;
      this.tokenvar   = this.userService.getToken();
     }

  ngOnInit(): void {

    this.SearchFormData = new FormGroup({
      mobile:new FormControl("",Validators.compose([
        Validators.required
      ]))
    });

  }

  SearchData(){
    const formData = new FormData();

    if(this.SearchFormData.value.mobile != ''){
      
      formData.append('uservalue', this.SearchFormData.value.mobile);

      this.ngxloader.start();
      this.ekycService.SearchKyc(formData).subscribe(
        response => {
          this.reportsList = response['result'];
          this.length  = this.reportsList.length;
          this.ngxloader.stop();
        },
        err => {
          console.log(err);
          if(err['status'] == 401){
            window.location.replace(err['error']['returnURL']);
          }
        }, () => {
          console.log('request completed!');
        }
      );
    }
    

  }

  openDialog(kycId){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        const formData = new FormData();
        formData.append('kycId', kycId);
        this.adminService.deleteForm(formData).subscribe(
          response => {

            if(response['status']){
              Swal.fire(
                'Deleted!',
                'Form has been deleted.',
                'success'
              )
            }
            else {
              Swal.fire(
                'oops!',
                response['message'] && response['message'] !='' ? response['message'] : 'somwthing went wrong',
                'warning'
              )
            }
          },
          err => {
            console.log(err);
            if(err['status'] == 401){
              window.location.replace(err['error']['returnURL']);
            }
          }, () => {
            console.log('request completed!');
          }
        );
      }
    })
    
  }

  logMeOut(){
    this.userService.logMeOut().subscribe(
      data => {
        if(data['code'] == 200) {
          sessionStorage.removeItem('token');
          this.router.navigateByUrl('login');
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

}
