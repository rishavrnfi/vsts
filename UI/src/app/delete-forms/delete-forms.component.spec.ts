import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFormsComponent } from './delete-forms.component';

describe('DeleteFormsComponent', () => {
  let component: DeleteFormsComponent;
  let fixture: ComponentFixture<DeleteFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
