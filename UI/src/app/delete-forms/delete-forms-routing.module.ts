import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeleteFormsComponent } from './delete-forms.component';

const routes: Routes = [{ path: '', component: DeleteFormsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeleteFormsRoutingModule { }
