import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import { EkycService } from '../services/ekyc.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
 


@Component({
  selector: 'app-kyc-forms',
  templateUrl: './kyc-forms.component.html',
  styleUrls: ['./kyc-forms.component.css']
})
export class KycFormsComponent implements OnInit {
  kyc_mode: any;
  SearchFormData: FormGroup;
  loader:Boolean = false;
  length   = 100;
  pageSize = 10;
  pageEvent: PageEvent;
  page:number=1; 
  showlist:Boolean=false;
 
  kycModeList:any = [
    {value: 'Aadhar OTP', viewValue: 'Aadhar OTP(Free)'},
    {value: 'Biometric', viewValue: 'Biometric(Free)'},
    {value: 'Non Aadhar', viewValue: 'Non Aadhar(Rs 100)'},
  ];
  kycStatus:String='';
  Status: any;
  statusList:any = [
    {key: '', value: 'All'},
    {key: '2', value: 'Approved'},
    {key: '1', value: 'Pre Approved'},
    {key: '5', value: 'Pending'},
    {key: '0', value: 'Incomplete'},
    {key: '3', value: 'Rejected'},
    {key: 'emp-processed', value: 'Employee Processed'},
    {key: 'emp-processed-pending', value: 'Employee Processed Pending'},
    // {key: 'rbl-manual', value: 'Rbl pending'},
  ];
  minFromDate = new Date(2020, 0, 1);
  maxFromDate = new Date(2021, 0, 1);

  minToDate   = new Date(2020, 0, 1);
  maxToDate   = new Date(2021, 0, 1);
  kycFormsList: any = [];
  createrType: any;


  fromdate: String = '';
  todate: String = '';
  showIncomplete: boolean = false;
  kycData:any;
  dialogRef:any;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  kycDataMsg: any;
  
  permission: boolean;
  constructor(
    private router: Router,
    private ekycService: EkycService,
    private formBuilder: FormBuilder,
    ) { }

  ngOnInit(): void {
    this.SearchFormData = new FormGroup({
      fromdate: new FormControl("",Validators.compose([
      ])), 
      page:new FormControl("",Validators.compose([
      ])),
      kycStatus:new FormControl("",Validators.compose([
      ])),
      kycMode:new FormControl("",Validators.compose([
      ])),
      uservalue:new FormControl("",Validators.compose([
      ]))
    });
 
    this.allkycforms();
  }
  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.minToDate = event.value;
  }
  pageevent(event){
    if(event.pageIndex<2){
      this.SearchFormData.value.page = 2;
      this.page = 2;
    }
    else{
      this.SearchFormData.value.page = event.pageIndex+1;
      this.page = event.pageIndex+1;
    }
    this.allkycforms();
  }
  allkycforms(){
    const formData = new FormData();
    formData.append('fromdate', this.SearchFormData.value.fromdate); 
    formData.append('kycStatus', this.SearchFormData.value.kycStatus);
    formData.append('kycMode', this.SearchFormData.value.kycMode); 
    formData.append('uservalue',this.SearchFormData.value.uservalue);
    formData.append('page',this.SearchFormData.value.page);
    
    this.ekycService.allkycforms(formData).subscribe(
      data => {
        if(data['code'] == 200) {
          this.showlist = true;
          this.kycFormsList = data['result'].data;
          this.length      = data['result'].total;
          this.createrType = data['creater_type'];
        }else{
          // do something
        }
      },
      err => {        
       	console.log(err);
        
      },() =>{
        console.log('request completed!');
      }
    );
  }

  logMeOut(){
    sessionStorage.removeItem('session_token');
    this.router.navigateByUrl('login');
  }

  

}
