import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycFormsComponent } from './kyc-forms.component';

describe('KycFormsComponent', () => {
  let component: KycFormsComponent;
  let fixture: ComponentFixture<KycFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
