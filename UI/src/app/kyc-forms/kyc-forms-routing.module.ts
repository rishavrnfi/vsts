import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KycFormsComponent } from './kyc-forms.component';

const routes: Routes = [{ path: '', component: KycFormsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KycFormsRoutingModule { }
