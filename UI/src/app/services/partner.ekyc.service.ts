import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class PartnerEkycService {

  
  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private http: HttpClient,private userService : UserService) { }

  getUserProfile() {
    return this.http.get(environment.apiBaseUrl + '/userProfile');
  }

  SearchKycData(SearchFormData) {
    return this.http.post(environment.apiBaseUrl + '/searchPartnerKyc', SearchFormData);
  }

  mobileVerify(formData){
    formData.append('session_token', this.userService.getSessionToken());
    return this.http.post(environment.apiBaseUrl + '/mobileVerify', formData,this.noAuthHeader);
  }

  agentMobileVerify(formData){
    return this.http.post(environment.apiBaseUrl + '/agentMobileVerify', formData,this.noAuthHeader);
  }
  
  
  confirmForm(formData){
    return this.http.post(environment.apiBaseUrl + '/confirmForm', formData,this.noAuthHeader);
  }

  addPanDetails(formData){
    return this.http.post(environment.apiBaseUrl + '/addPanDetails', formData,this.noAuthHeader);
  }

  uploadPanPhoto(formData){
    return this.http.post(environment.apiBaseUrl + '/uploadPanPhoto', formData,this.noAuthHeader);
  }
  
  updatePanDetails(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/updatePanDetails', formData);
  }

  addDistributorDetails(formData){
    return this.http.post(environment.apiBaseUrl + '/addDistributorDetails', formData,this.noAuthHeader);
  }

  addAddressProof(formData){
    return this.http.post(environment.apiBaseUrl + '/addAddressProof', formData);
  }

  getKycData(kycFormId){
    let token = this.userService.getToken();
    return this.http.get(environment.apiBaseUrl + '/getPartnerKycData?kycFormId='+kycFormId+'&token='+token);
  }

  unlockForm(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/unlockPartnerForm', formData);
  }

  rejectForm(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/rejectPartnerForm', formData);
  }

  approvePanDetails(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/approvePartnerPanDetails', formData);
  }

  rejectPanDetails(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/rejectPartnerPanDetails', formData);
  }
  
  approveAddressProof(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/approvePartnerAddressProof', formData);
  }

  rejectAddressProof(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/rejectPartnerAddressProof', formData);
  }
  
  panFetchResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/partnerPanFetchResponse', formData);
  }

  panExtractionResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/partnerPanExtractionResponse', formData);
  }

  addressProofResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/partnerAddressProofResponse', formData);
  }
  
  getVideoVerificationResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/getPartnerVideoVerificationResponse', formData);
  }
  
  approveVideoDetails(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/partnerApproveVideoDetails', formData);
  }
  
  rejectVideoDetails(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/partnerRejectVideoDetails', formData);
  }

  getForgeryResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/getPartnerForgeryResponse', formData);
  }

  getRemarksByType(type){
    let token = this.userService.getToken();
    return this.http.get(environment.apiBaseUrl + '/getRemarksByType?type='+type+'&token='+token);
  }

  updateResidentialProof(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/updatePartnerResidentialProof', formData);
  }

  getImageQualityResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/getPartnerImageQualityResponse', formData);
  }

  getNameMatchResponse(formData){
    formData.append('token', this.userService.getToken());
    return this.http.post(environment.apiBaseUrl + '/getPartnerNameMatchResponse', formData);
  }

}
