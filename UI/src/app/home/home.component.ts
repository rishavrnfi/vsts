import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, CanActivate } from '@angular/router';
import { AuthjwtService } from '../services/authjwt.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  jwtPayload: any;
	session_token : string = '';
 
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public jwtauth: AuthjwtService,
    ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.session_token = params['session_token'];
    });
  
    if(typeof this.session_token !=='undefined')
    {
      sessionStorage.setItem("session_token", this.session_token);
      sessionStorage.removeItem('token');
      this.router.navigate(['dashboard']);
    }
    else{
      if(sessionStorage.getItem("token")){
         if(this.jwtauth.isAuthenticated()){
          this.jwtPayload =  this.jwtauth.isAuthenticatedjwttoken();
          this.router.navigate(['admin-dashboard']); 
        }else{
          sessionStorage.removeItem("token");
          this.router.navigate(['login']);
        }
        
      }else if(sessionStorage.getItem("session_token")){
        this.router.navigate(['dashboard']);
      }
      else{
        this.router.navigate(['login']);
      }
    }
  }

}
