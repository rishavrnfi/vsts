export const environment = {
  production: true,
  // apiBaseUrl: 'https://kyc.vsts.net.in/server/public/index.php/api',
  // appBaseUrl: 'https://kyc.vsts.net.in/client'
  apiBaseUrl: 'https://portal.vsts.net.in/servertest/public/index.php/api',
  appBaseUrl: 'https://portal.vsts.net.in/client'
};
